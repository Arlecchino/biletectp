package com.gb.ctptickets.ui.preference;


import android.content.Context;
import android.preference.ListPreference;
import android.util.AttributeSet;

import com.gb.ctptickets.R;
import com.gb.ctptickets.ui.preference.helper.PreferenceHelper;
import com.gb.ctptickets.ui.preference.helper.PreferenceHelperUtil;


/**
 * Created by GABOR on 2015-maj.-31.
 */
public class AutoSetupListPreference extends ListPreference {

	public PreferenceHelper<ListPreference>	helper;


	public AutoSetupListPreference(Context context, AttributeSet attrs) {
		super(context, attrs);
		helper = PreferenceHelperUtil.getHelper(this, attrs, R.styleable.AutoSetupListPreference,
				R.styleable.AutoSetupListPreference_helper);
	}


	@Override
	protected void onAttachedToActivity() {
		super.onAttachedToActivity();

		if (helper != null) {
			helper.setup();
		}
	}
}
