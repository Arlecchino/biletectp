package com.gb.ctptickets.ui.preference;


import android.content.Context;
import android.preference.CheckBoxPreference;
import android.util.AttributeSet;

import com.gb.ctptickets.R;
import com.gb.ctptickets.ui.preference.helper.PreferenceHelper;
import com.gb.ctptickets.ui.preference.helper.PreferenceHelperUtil;


/**
 * CheckBoxPreference with PreferenceHelper
 *
 * Created by GABOR on 2015-maj.-31.
 */
public class AutoSetupCheckboxPreference extends CheckBoxPreference {

	private PreferenceHelper<CheckBoxPreference>	helper;


	public AutoSetupCheckboxPreference(Context context, AttributeSet attrs) {
		super(context, attrs);
		helper = PreferenceHelperUtil.getHelper(this, attrs, R.styleable.AutoSetupCheckboxPreference,
				R.styleable.AutoSetupCheckboxPreference_helper);
	}


	@Override
	protected void onAttachedToActivity() {
		super.onAttachedToActivity();

		if (helper != null) {
			helper.setup();
		}
	}
}
