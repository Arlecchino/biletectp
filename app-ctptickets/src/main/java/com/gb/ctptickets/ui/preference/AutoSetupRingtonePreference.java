package com.gb.ctptickets.ui.preference;


import android.content.Context;
import android.preference.RingtonePreference;
import android.util.AttributeSet;

import com.gb.ctptickets.R;
import com.gb.ctptickets.ui.preference.helper.PreferenceHelper;
import com.gb.ctptickets.ui.preference.helper.PreferenceHelperUtil;


/**
 * Created by GABOR on 2015-maj.-31.
 */
public class AutoSetupRingtonePreference extends RingtonePreference {

	private PreferenceHelper<RingtonePreference>	helper;


	@SuppressWarnings("WeakerAccess")
	public AutoSetupRingtonePreference(Context context, AttributeSet attrs) {
		super(context, attrs);
		helper = PreferenceHelperUtil.getHelper(this, attrs,
				R.styleable.AutoSetupRingtonePreference, R.styleable.AutoSetupRingtonePreference_helper);
	}


	@Override
	protected void onAttachedToActivity() {
		super.onAttachedToActivity();

		if (helper != null) {
			helper.setup();
		}
	}
}
