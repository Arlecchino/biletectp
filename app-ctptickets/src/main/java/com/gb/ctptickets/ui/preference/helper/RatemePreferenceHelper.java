package com.gb.ctptickets.ui.preference.helper;


import android.preference.Preference;

import com.gb.ctptickets.ui.preference.helper.PreferenceHelper;
import com.gb.ctptickets.util.IntentUtils;


/**
 * Created by GABOR on 2015-maj.-30.
 */
public class RatemePreferenceHelper extends PreferenceHelper<Preference> {

	public RatemePreferenceHelper(Preference preference) {
		super(preference);
	}


	@Override
	public void setup() {
		preference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

			@Override
			public boolean onPreferenceClick(Preference preference) {
                IntentUtils.launchGooglePlay(preference.getContext());
				return false;
			}
		});
	}
}
