package com.gb.ctptickets.ui.activity;


import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;

import com.gb.ctptickets.R;


public abstract class ActionBarPreferenceActivity extends PreferenceActivity {

	@SuppressWarnings("FieldCanBeLocal")
	private Toolbar	toolbar;

	@SuppressWarnings("FieldCanBeLocal")
	private View	shadowView;


	@SuppressWarnings("SameReturnValue")
	protected abstract int getPreferencesXmlId();


	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_preference_with_actionbar);
		toolbar = (Toolbar) findViewById(R.id.toolbar);

		if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
			shadowView = findViewById(R.id.shadow_view);
			final ViewGroup parent = (ViewGroup) shadowView.getParent();
			parent.removeView(shadowView);
			shadowView = null;
		}
		addPreferencesFromResource(getPreferencesXmlId());
		toolbar.setClickable(true);
		toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
	}
}
