package com.gb.ctptickets.ui.preference.helper;


import android.preference.ListPreference;
import android.preference.Preference;

import com.gb.ctptickets.R;
import com.gb.ctptickets.ui.preference.helper.PreferenceHelper;
import com.gb.ctptickets.util.GoogleAnalyticsHelper;


/**
 * Because PreferenceActivity is not backward-compatible, I needed to create two Activities (one with PreferenceActivity
 * and another with ActionBarActivity and PreferenceFragment) and I put in this class the common operations related to
 * the timetable file expiry preference
 *
 * Created by GABOR on 12/15/2014.
 */
public class TimetableExpiryPreferenceHelper extends PreferenceHelper<ListPreference> {

	public TimetableExpiryPreferenceHelper(ListPreference preference) {
		super(preference);
	}


	public void setup() {
		BindTimeTablePreferenceSummaryToValueListener listener = new BindTimeTablePreferenceSummaryToValueListener();
		preference.setOnPreferenceChangeListener(listener);
		String value = preference.getValue();
		listener.onPreferenceChange(preference, value);
	}

	private static class BindTimeTablePreferenceSummaryToValueListener implements Preference.OnPreferenceChangeListener {

		@Override
		public boolean onPreferenceChange(Preference preference, Object value) {
			String stringValue = value.toString();
			ListPreference listPreference = (ListPreference) preference;
			int index = listPreference.findIndexOfValue(stringValue);
			preference.setSummary(index >= 0 ? listPreference.getContext().getResources()
					.getStringArray(R.array.timetable_refresh_description)[index] : null);
			GoogleAnalyticsHelper.onSettingChanged(preference.getKey(), stringValue);
			return true;
		}
	}
}
