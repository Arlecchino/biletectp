package com.gb.ctptickets.ui.dialog;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.gb.ctptickets.R;
import com.gb.ctptickets.util.GoogleAnalyticsHelper;


/**
 * Allows the user to enter the name of the new bus line
 * <p/>
 * Created by gbiro on 7/7/2014.
 */
public class AddCustomBusLineDialog extends DialogFragment {

    public interface OnCustomBusLineAddedCallback {

        void onCustomBusLineEntered(String line);
    }

    public static void show(FragmentActivity activity, AddCustomBusLineDialog.OnCustomBusLineAddedCallback callback) {
        AddCustomBusLineDialog customBusLineDialog = new AddCustomBusLineDialog();
        customBusLineDialog.setCallback(callback);
        customBusLineDialog.show(activity.getSupportFragmentManager(), null);
    }

    private OnCustomBusLineAddedCallback callback;
    private AlertDialog dialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance()) {
            getDialog().setDismissMessage(null);
        }
        super.onDestroyView();
    }

    private void setCallback(OnCustomBusLineAddedCallback callback) {
        this.callback = callback;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
            alertDialogBuilder.setTitle(R.string.specify_new_line_name);
            final EditText inputET = new EditText(getActivity());
            RelativeLayout container = new RelativeLayout(getActivity());
            container.addView(inputET, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            int padding = getResources().getDimensionPixelSize(R.dimen.spacing);
            container.setPadding(padding, padding, padding, padding);
            alertDialogBuilder.setView(container);

            alertDialogBuilder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String line = inputET.getText().toString().trim();
                    GoogleAnalyticsHelper.onCustomLineAdded(line);

                    if (callback != null) {
                        callback.onCustomBusLineEntered(line);
                    }
                }
            });
            alertDialogBuilder.setNegativeButton(android.R.string.cancel, null);
            dialog = alertDialogBuilder.create();
        }
        return dialog;
    }
}
