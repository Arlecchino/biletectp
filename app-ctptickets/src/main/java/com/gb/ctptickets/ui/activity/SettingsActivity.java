package com.gb.ctptickets.ui.activity;


import android.os.Bundle;
import android.preference.PreferenceScreen;

import com.gb.ctptickets.BuildConfig;
import com.gb.ctptickets.R;
import com.gb.ctptickets.ui.preference.AutoSetupListPreference;
import com.google.android.gms.analytics.GoogleAnalytics;

import de.greenrobot.event.EventBus;


/**
 * Setting activity for pre HC devices. Based on old {@link android.preference.PreferenceActivity}
 * <p/>
 * Created by GABOR on 2014-szept.-28.
 */
@SuppressWarnings("deprecation")
public class SettingsActivity extends ActionBarPreferenceActivity {

	private AutoSetupListPreference	currencyPreference;


	@Override
	protected int getPreferencesXmlId() {
		return R.xml.pref_general;
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initPreferences();

		//noinspection PointlessBooleanExpression
		if (!BuildConfig.DEBUG) {
			PreferenceScreen screen = (PreferenceScreen) findPreference(getString(R.string.pref_container));
			screen.removePreference(findPreference(getString(R.string.pref_debug_category)));
		}
	}


	@Override
	protected void onStart() {
		super.onStart();
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
	}


	@Override
	protected void onResume() {
		super.onResume();

		if (currencyPreference.helper != null) {
			EventBus.getDefault().register(currencyPreference.helper);
		}
	}


	@Override
	protected void onPause() {
		super.onPause();

		if (currencyPreference.helper != null) {
			EventBus.getDefault().unregister(currencyPreference.helper);
		}
	}


	@Override
	protected void onStop() {
		super.onStop();
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}


	private void initPreferences() {
		String currencyPreferenceKey = getString(R.string.pref_currency);
		currencyPreference = (AutoSetupListPreference) findPreference(currencyPreferenceKey);
	}
}
