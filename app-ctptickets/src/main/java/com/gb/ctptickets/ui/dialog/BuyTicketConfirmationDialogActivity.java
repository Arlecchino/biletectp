package com.gb.ctptickets.ui.dialog;


import android.content.Intent;
import android.os.Bundle;

import com.gb.ctptickets.App;
import com.gb.ctptickets.ui.activity.DialogActivity;


/**
 * Asks the user whether he/she really wants to buy that ticket.
 * 
 * Created by GABOR on 2014-jún.-22.
 */
public class BuyTicketConfirmationDialogActivity extends DialogActivity {

	private static final String ACTION_BUY = "com.gb.ctptickets.BUY";

	public static void show(String ticket) {
		App.getContext().startActivity(prepareIntent(ticket));
	}

	public static Intent prepareIntent(String ticket) {
		Intent i = new Intent(App.getContext(), BuyTicketConfirmationDialogActivity.class);
		i.setAction(ACTION_BUY);
		i.putExtra(BuyTicketConfirmationDialog.EXTRA_TICKET, ticket);
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		return i;
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		BuyTicketConfirmationDialog dialog = new BuyTicketConfirmationDialog();
		dialog.setArguments(getIntent().getExtras());
		dialog.show(getSupportFragmentManager(), null);
	}
}
