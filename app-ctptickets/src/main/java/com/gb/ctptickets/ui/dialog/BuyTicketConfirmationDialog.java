package com.gb.ctptickets.ui.dialog;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.gb.ctptickets.App;
import com.gb.ctptickets.Constants;
import com.gb.ctptickets.R;
import com.gb.ctptickets.UserSettings;
import com.gb.ctptickets.provider.calendar.HolidayProvider;
import com.gb.ctptickets.provider.ticket.TicketBuyer;
import com.gb.ctptickets.provider.ticket.TicketHandlerFactory;
import com.gb.ctptickets.provider.ticket.TicketRequestStatusRegistry;
import com.gb.ctptickets.provider.ticket.TicketUIProvider;
import com.gb.ctptickets.provider.ticket.AvailableBusLineRegistry;
import com.gb.ctptickets.provider.ticket.model.TicketRate;
import com.gb.ctptickets.provider.timetable.TimeTableHandler;
import com.gb.ctptickets.provider.timetable.TimeTableUnavailableException;
import com.gb.ctptickets.ui.eventbus.TimeTableDownloadStateChangedEvent;
import com.gb.ctptickets.util.CalendarUtils;
import com.gb.ctptickets.util.IntentUtils;

import org.apache.commons.lang3.StringUtils;

import de.greenrobot.event.EventBus;


/**
 * Prompted to the user before actually sending the ticket buying sms
 * <p/>
 * Created by gbiro on 12/17/2014.
 */
public class BuyTicketConfirmationDialog extends DialogFragment {

    static final String EXTRA_TICKET = "EXTRA_TICKET";

    private TicketBuyer ticketBuyer;
    private TimeTableHandler timeTableHandler;

    private ImageView timeTableButton;
    private ProgressBar progressIndicator;

    private LinearLayout confirmationDialogBody;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final String ticket = getArguments().getString(EXTRA_TICKET);
        ticketBuyer = TicketHandlerFactory.getBuyer(ticket);
        timeTableHandler = new TimeTableHandler(ticket);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

        if (AvailableBusLineRegistry.isSeasonalTicketValue(ticket)) {
            alertDialogBuilder.setTitle(R.string.buy_seasonal_ticket_confirmation_dialog);
        } else {
            alertDialogBuilder.setTitle(R.string.buy_ticket_confirmation_dialog);
        }
        TicketRate ticketRate = UserSettings.getCurrentTicketRate();

        TicketUIProvider uiProvider = TicketHandlerFactory.getUIProvider(ticket);

        TicketRequestStatusRegistry registry = TicketHandlerFactory.getRequestStatusRegistry(ticket);

        String ticketDescription = "";

        long id = registry.getIdForTicketWithClosestExpiration();

        if (id >= 0) {
            if (registry.isValid(id)) {
                ticketDescription = getString(R.string.ticket_already_bought) + "\n\n";
            } else if (registry.isRequestSent(id) || registry.isRequestDelivered(id)) {
                ticketDescription = getString(R.string.ticket_request_already_sent) + "\n\n";
            }
        }
        ticketDescription += uiProvider.getTitle() + "\n" + uiProvider.getPrice(ticketRate);

        confirmationDialogBody = (LinearLayout) LayoutInflater.from(getActivity()).inflate(
                R.layout.confirmation_dialog_body, null);
        TextView message = (TextView) confirmationDialogBody.findViewById(R.id.text);
        message.setText(ticketDescription);
        progressIndicator = (ProgressBar) confirmationDialogBody.findViewById(R.id.progress_indicator);
        timeTableButton = (ImageView) confirmationDialogBody.findViewById(R.id.timetable_button);

        timeTableButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (timeTableHandler.isDownloaded() && !timeTableHandler.isExpired()) {
                    openPdf();
                    dismiss();
                } else {
                    timeTableButton.setVisibility(View.GONE);
                    progressIndicator.setVisibility(View.VISIBLE);
                    try {
                        timeTableHandler.download();
                    } catch (TimeTableUnavailableException e) {
                        EventBus.getDefault().post(
                                new TimeTableDownloadStateChangedEvent(ticket,
                                        TimeTableDownloadStateChangedEvent.STATE_ERROR, App.getContext().getString(
                                        R.string.timetable_unavailable)));
                    }
                }
            }
        });
        alertDialogBuilder.setView(confirmationDialogBody);
        alertDialogBuilder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                ticketBuyer.buy();
            }
        });
        alertDialogBuilder.setNegativeButton(R.string.no, null);

        if (id >= 0 && registry.isValid(id)) {
            alertDialogBuilder.setNeutralButton(getString(R.string.view_sms), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    IntentUtils.startSmsApplication(Constants.TICKET_BUY_PHONE_NUMBER);
                }
            });
        }
        return alertDialogBuilder.create();
    }


    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this); // TimeTableDownloadStateChangedEvent
        progressIndicator.setVisibility(View.GONE);
        updateTimeTableButtonVisibility();
    }


    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this); // TimeTableDownloadStateChangedEvent
    }


    @Override
    public void onDetach() {
        super.onDetach();
        getActivity().finish();
    }


    /**
     * Open the pdf for the bus line displayed in this dialog
     */
    private void openPdf() {
        try {
            timeTableHandler.open();
        } catch (ActivityNotFoundException e) {
            NoMatchingActivityDialogActivity.show(timeTableHandler.getFilename());
        } catch (TimeTableUnavailableException e) {
            // Timetable button shouldn't be shown when file is not downloaded
            e.printStackTrace();
        }
        if (UserSettings.isWeekendWarningEnabled()) {
            if (CalendarUtils.isWeekend()) {
                Toast.makeText(getActivity(), R.string.weekend_warning, Toast.LENGTH_LONG).show();
            } else {
                String[] holidays = HolidayProvider.getHolidays();

                if (holidays != null && holidays.length > 0) {
                    Toast.makeText(getActivity(), getString(R.string.holiday_warning, StringUtils.join(holidays, ", ")), Toast.LENGTH_LONG).show();
                }
            }
        }
    }


    @SuppressWarnings("unused")
    public void onEventMainThread(TimeTableDownloadStateChangedEvent event) {
        switch (event.state) {
            case TimeTableDownloadStateChangedEvent.STATE_FINISHED:
                openPdf();
                dismiss();
                break;
            case TimeTableDownloadStateChangedEvent.STATE_ERROR:
                Toast.makeText(getActivity(), event.reasonStr, Toast.LENGTH_LONG).show();
                progressIndicator.setVisibility(View.GONE);
                updateTimeTableButtonVisibility();
                break;
        }
    }


    private void updateTimeTableButtonVisibility() {
        final String ticket = getArguments().getString(EXTRA_TICKET);

        if (AvailableBusLineRegistry.isSeasonalTicketValue(ticket)) {
            timeTableButton.setVisibility(View.GONE);

            if (Build.VERSION.SDK_INT >= Constants.HONEYCOMB) {
                confirmationDialogBody.setShowDividers(LinearLayout.SHOW_DIVIDER_NONE);
            }
        } else {
            timeTableButton.setVisibility(View.VISIBLE);

            if (Build.VERSION.SDK_INT >= Constants.HONEYCOMB) {
                confirmationDialogBody.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
            }
        }
    }
}