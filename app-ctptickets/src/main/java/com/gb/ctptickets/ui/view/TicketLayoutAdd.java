package com.gb.ctptickets.ui.view;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.gb.ctptickets.R;


/**
 * Created by gbiro on 2014-Jun-19.
 */
public class TicketLayoutAdd extends TicketLayout {

	public TicketLayoutAdd(Context context, String title) {
		super(context, null, title, null);
	}


	@Override
	void initUI(Context context, String title, String subtitle, String value) {
		super.initUI(context, title, subtitle, value);
		RelativeLayout extraControl = (RelativeLayout) findViewById(R.id.extra_control);
		View ticketAddIcon = LayoutInflater.from(getContext()).inflate(R.layout.ticket_add, null);
		extraControl.addView(ticketAddIcon);
		extraControl.setVisibility(View.VISIBLE);
	}
}
