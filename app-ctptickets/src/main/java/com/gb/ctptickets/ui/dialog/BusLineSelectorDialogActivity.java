package com.gb.ctptickets.ui.dialog;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.gb.ctptickets.App;
import com.gb.ctptickets.R;
import com.gb.ctptickets.ui.activity.DialogActivity;


/**
 * Floating activity that houses {@link BusLineSelectorDialog}. Used from MainActivity or to set up widget and shortcut.
 *
 * Created by gbiro on 10/16/2014.
 */
public class BusLineSelectorDialogActivity extends DialogActivity implements BusLineSelectorDialog.Callback {

    public static final int REQUEST_SELECT_CUSTOM_TICKET = 1;
    public static final int REQUEST_SELECT_QUICK_TICKET = 2;
    public static final int REQUEST_BUY_TICKET = 3;

    public static final String EXTRA_BUS_LINE = "EXTRA_BUS_LINE";


    /**
     * This method show a dialog from which the user can select a bus line.<br>
     * (A custom ticket is a green ticket with some bus line. Buys a ticket for one travel)<br>
     * If the user chooses to add a new line, this dialog disappears and a new input dialog appears. After the user
     * enters the new line, it will be added to the quick ticket selector button and it will also be selected.
     */
    public static void showCustomTicketSelectorDialog(Activity parent) {
        parent.startActivityForResult(prepareIntent(true, true), REQUEST_SELECT_CUSTOM_TICKET);
    }


    /**
     * This method show a dialog from which the user can select a bus line.<br>
     * (A custom ticket is a green ticket with some bus line. Buys a ticket for one travel)<br>
     * If the user chooses to add a new line, this dialog disappears and a new input dialog appears. After the user
     * enters the new line, it will be added to the quick ticket selector button and it will also be selected.
     */
    public static void showBusLineSelectorDialog(Activity parent) {
        parent.startActivityForResult(prepareIntent(false, true), REQUEST_SELECT_QUICK_TICKET);
    }

    /**
     * This method show a dialog from which the user can select a bus line.<br>
     * (A custom ticket is a green ticket with some bus line. Buys a ticket for one travel)<br>
     * If the user chooses to add a new line, this dialog disappears and a new input dialog appears. After the user
     * enters the new line, it will be added to the quick ticket selector button and it will also be selected.
     * After a bus line has been selected, the buy confirmation dialog appears so that the user can immediately buy the ticket.
     */
    public static void showBusLineSelectorAndBuyDialog(Activity parent) {
        parent.startActivityForResult(prepareIntent(false, true), REQUEST_BUY_TICKET);
    }

    @SuppressWarnings("SameParameterValue")
    private static Intent prepareIntent(boolean unusedOnly, boolean showAddNewButton) {
        Intent i = new Intent(App.getContext(), BusLineSelectorDialogActivity.class);
        i.putExtra(BusLineSelectorDialog.EXTRA_SHOW_UNUSED_ONLY, unusedOnly);
        i.putExtra(BusLineSelectorDialog.EXTRA_SHOW_ADD_NEW_BUTTON, showAddNewButton);
        return i;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setResult(RESULT_CANCELED);
        initUI();
    }


    protected void initUI() {
        BusLineSelectorDialog dialog = new BusLineSelectorDialog();

        // Note: if started directly by intent, both EXTRA_SHOW_UNUSED_ONLY and EXTRA_SHOW_ADD_NEW_BUTTON are set to false. Which is the intended way at the moment.

        dialog.setArguments(getIntent().getExtras());
        dialog.show(getSupportFragmentManager(), null);
    }


    @Override
    public void onBusLineSelected(String line) {
        if (getIntent().getAction() != null && getIntent().getAction().equals(Intent.ACTION_CREATE_SHORTCUT)) {
            Intent.ShortcutIconResource icon = Intent.ShortcutIconResource.fromContext(this, R.drawable.ic_launcher);
            Intent intent = new Intent();
            intent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, BuyTicketConfirmationDialogActivity.prepareIntent(line));
            intent.putExtra(Intent.EXTRA_SHORTCUT_NAME, getString(R.string.line_name, line));
            intent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, icon);
            setResult(RESULT_OK, intent);
        } else {
            Intent result = new Intent();
            result.putExtra(EXTRA_BUS_LINE, line);
            setResult(RESULT_OK, result);
        }
    }
}
