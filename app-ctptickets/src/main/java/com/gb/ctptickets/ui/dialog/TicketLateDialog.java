package com.gb.ctptickets.ui.dialog;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.gb.ctptickets.App;
import com.gb.ctptickets.R;
import com.gb.ctptickets.UserSettings;
import com.gb.ctptickets.provider.ticket.TicketHandlerFactory;
import com.gb.ctptickets.provider.ticket.TicketUIProvider;
import com.gb.ctptickets.ui.activity.DialogActivity;
import com.gb.ctptickets.util.NotificationBuilder;


/**
 * Created by GABOR on 2014-okt.-12.
 */
public class TicketLateDialog extends DialogActivity {

	private static final String	EXTRA_TICKET	= "EXTRA_TICKET";


	public static void show(String ticket) {
		Intent i = new Intent(App.getContext(), TicketLateDialog.class);
		i.putExtra(TicketLateDialog.EXTRA_TICKET, ticket);
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		App.getContext().startActivity(i);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		TicketLateDialog_ dialog = new TicketLateDialog_();
		dialog.setArguments(getIntent().getExtras());
		dialog.show(getSupportFragmentManager(), null);
	}

	public static class TicketLateDialog_ extends DialogFragment implements CompoundButton.OnCheckedChangeListener {

		private TicketUIProvider	uiProvider;


		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			final String ticket = getArguments().getString(EXTRA_TICKET);
			uiProvider = TicketHandlerFactory.getUIProvider(ticket);
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
			alertDialogBuilder.setTitle(R.string.app_name);
			String message = uiProvider.getTitle();
			View body = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_ticket_late, null);
			TextView messageView = (TextView) body.findViewById(R.id.message);
			messageView.setText(getString(R.string.ticket_late_notif_body, message));
			CheckBox newerShowAgainCB = (CheckBox) body.findViewById(R.id.newer_show_again);
			newerShowAgainCB.setChecked(UserSettings.getTicketLateWarningNewerShowAgain());
			newerShowAgainCB.setOnCheckedChangeListener(this);
			alertDialogBuilder.setView(body);
			alertDialogBuilder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					new NotificationBuilder(ticket).hide();
				}
			});
			alertDialogBuilder.setIcon(R.drawable.ic_action_error);
			return alertDialogBuilder.create();
		}


		@Override
		public void onDetach() {
			super.onDetach();
			getActivity().finish();
		}


		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			UserSettings.setTicketLateWarningDialogNewerShowAgain(isChecked);
		}
	}
}
