package com.gb.ctptickets.ui.view;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gb.ctptickets.AppSettings;
import com.gb.ctptickets.R;
import com.gb.ctptickets.UserSettings;
import com.gb.ctptickets.provider.ticket.AvailableBusLineRegistry;
import com.gb.ctptickets.util.PrefsUtil;


/**
 * Created by gbiro on 2014-Jun-20.
 */
public class TicketLayoutQuick extends TicketLayout implements SharedPreferences.OnSharedPreferenceChangeListener {

	private static final int	ANDROID_VERSION_CODE_HONEYCOMB	= 11;

	private TextView			busSpinner;


	public TicketLayoutQuick(Context context, AttributeSet attrs) {
		super(context, attrs);

		if (!isInEditMode()) {
			PrefsUtil.registerOnSharedPreferenceChangeListener(UserSettings.PREF_QUICK_BUS_LINE_SELECTION, this);
			PrefsUtil.registerOnSharedPreferenceChangeListener(UserSettings.PREF_CUSTOM_BUS_LINES, this);
			PrefsUtil.registerOnSharedPreferenceChangeListener(AppSettings.PREF_NEW_BUS_LINES, this);
			PrefsUtil.registerOnSharedPreferenceChangeListener(AppSettings.PREF_HIDDEN_BUS_LINES, this);
		}
	}


	public TicketLayoutQuick(Context context, String title) {
		super(context, null, title, null);

		if (!isInEditMode()) {
			PrefsUtil.registerOnSharedPreferenceChangeListener(UserSettings.PREF_QUICK_BUS_LINE_SELECTION, this);
			PrefsUtil.registerOnSharedPreferenceChangeListener(UserSettings.PREF_CUSTOM_BUS_LINES, this);
			PrefsUtil.registerOnSharedPreferenceChangeListener(AppSettings.PREF_NEW_BUS_LINES, this);
			PrefsUtil.registerOnSharedPreferenceChangeListener(AppSettings.PREF_HIDDEN_BUS_LINES, this);
		}
	}


	@Override
	void initUI(final Context context, String title, String subtitle, String value) {
		super.initUI(context, title, subtitle, value);
		RelativeLayout extraControl = (RelativeLayout) findViewById(R.id.extra_control);
		busSpinner = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.bus_spinner, null);
		RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(getResources()
				.getDimensionPixelSize(R.dimen.spinner_width), ViewGroup.LayoutParams.WRAP_CONTENT);
		extraControl.addView(busSpinner, layoutParams);

		if (Build.VERSION.SDK_INT >= ANDROID_VERSION_CODE_HONEYCOMB) {
			extraControl.setBackgroundResource(R.drawable.ticket_background_yellow);
		}
		extraControl.setVisibility(View.VISIBLE);
		updateSelector();
	}


	/**
	 * Use this method if you want
	 */
	public void setSpinnerOnClickListener(OnClickListener listener) {
		busSpinner.setOnClickListener(listener);
	}


	private void updateSelector() {
		String[] busLines = AvailableBusLineRegistry.getAllBusLines(true, false);

		if (!TextUtils.isEmpty(UserSettings.getQuickBusLineSelection())) {
			busSpinner.setText(UserSettings.getQuickBusLineSelection());
		} else {
			busSpinner.setText(busLines[0]);
		}
	}


	@Override
	public String getValue() {
		return busSpinner.getText().toString();
	}


	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		updateSelector();
		updateTicketRequestState();
	}
}
