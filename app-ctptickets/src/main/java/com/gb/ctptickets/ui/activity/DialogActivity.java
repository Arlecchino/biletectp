package com.gb.ctptickets.ui.activity;


import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.gb.ctptickets.R;


/**
 * An activity that looks like a dialog. Use it with android:theme="@style/DialogActivityTheme" in the manifest.<br>
 * Created by GABOR on 2014-szept.-21.
 */
@SuppressLint("Registered")
public class DialogActivity extends FragmentActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().getDecorView().setBackgroundColor(Color.TRANSPARENT);
	}


	@Override
	public void finish() {
		super.finish();
		overridePendingTransition(R.anim.abc_fade_in, 0);
	}
}
