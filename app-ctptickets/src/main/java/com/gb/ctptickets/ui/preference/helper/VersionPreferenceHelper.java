package com.gb.ctptickets.ui.preference.helper;


import android.preference.Preference;

import com.gb.ctptickets.App;
import com.gb.ctptickets.BuildConfig;


/**
 * Used by {@link com.gb.ctptickets.ui.activity.SettingsActivity} to set up the version number row in the settings
 * <p/>
 * Created by gbiro on 5/22/2015.
 */
public class VersionPreferenceHelper extends PreferenceHelper<Preference> {

	public VersionPreferenceHelper(Preference preference) {
		super(preference);
	}


	public void setup() {
		preference.setSummary(App.getVersionName() + " (" + App.getVersion() + ") - "
				+ (BuildConfig.DEBUG ? "DEBUG" : "PROD"));
	}
}
