package com.gb.ctptickets.ui.preference.helper;


import android.preference.Preference;


/**
 * Base class for preference helpers
 *
 * Created by GABOR on 2015-maj.-30.
 */
public abstract class PreferenceHelper<T extends Preference> {

	final T	preference;


	PreferenceHelper(T preference) {
		this.preference = preference;
	}


	public abstract void setup();
}
