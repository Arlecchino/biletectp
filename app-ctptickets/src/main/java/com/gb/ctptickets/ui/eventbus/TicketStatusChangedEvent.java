package com.gb.ctptickets.ui.eventbus;


/**
 * Created by gbiro on 11/12/2014.
 */
public class TicketStatusChangedEvent {

	public final String	ticket;


	public TicketStatusChangedEvent(String ticket) {
		this.ticket = ticket;
	}
}
