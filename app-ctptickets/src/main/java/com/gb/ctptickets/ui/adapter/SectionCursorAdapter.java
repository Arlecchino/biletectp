package com.gb.ctptickets.ui.adapter;


import android.content.Context;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.support.annotation.LayoutRes;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.LinkedHashMap;


public abstract class SectionCursorAdapter extends CursorAdapter {

    private static final int TYPE_NORMAL = 1;
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_COUNT = 2;

    private final int headerLayoutResId;
    private final int groupColumn;
    private final LayoutInflater layoutInflater;

    private final LinkedHashMap<Integer, String> sectionsIndexer;

    public static class ViewHolder {
        public TextView textView;
    }


    public SectionCursorAdapter(Context context, Cursor cursor, @LayoutRes int headerLayoutResId, int groupColumn) {
        super(context, cursor, 0);
        this.sectionsIndexer = new LinkedHashMap<>();
        this.headerLayoutResId = headerLayoutResId;
        this.groupColumn = groupColumn;
        this.layoutInflater = LayoutInflater.from(context);

        if (cursor != null) {
            calculateSectionHeaders();
            cursor.registerDataSetObserver(dataSetObserver);
        }
    }

    private final DataSetObserver dataSetObserver = new DataSetObserver() {
        public void onChanged() {
            calculateSectionHeaders();
        }

        public void onInvalidated() {
            sectionsIndexer.clear();
        }
    };

    /**
     * <p>
     * This method serve as an intercepter before the sections are calculated so you can transform some computer data
     * into human readable, e.g. format a unix timestamp, or a status.
     * </p>
     * <p>
     * <p>
     * By default this method returns the original data for the group column.
     * </p>
     */
    protected String getCustomGroup(String groupData) {
        return groupData;
    }

    private void calculateSectionHeaders() {
        int i = 0;
        String previous = "";
        int count = 0;

        final Cursor cursor = getCursor();

        sectionsIndexer.clear();

        if (cursor == null) {
            return;
        }
        cursor.moveToPosition(-1);

        while (cursor.moveToNext()) {
            final String group = getCustomGroup(cursor.getString(groupColumn));

            if (!previous.equals(group)) {
                sectionsIndexer.put(i + count, group);
                previous = group;
                count++;
            }
            i++;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int viewType = getItemViewType(position);
        if (viewType == TYPE_NORMAL) {
            Cursor cursor = (Cursor) getItem(position);
            if (cursor == null) {
                return layoutInflater.inflate(headerLayoutResId, parent, false);
            }
            final int mapCursorPos = getSectionForPosition(position);
            cursor.moveToPosition(mapCursorPos);
            return super.getView(mapCursorPos, convertView, parent);
        } else {
            ViewHolder holder;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = layoutInflater.inflate(headerLayoutResId, parent, false);
                holder.textView = (TextView) convertView;
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            TextView sectionText = holder.textView;
            final String group = sectionsIndexer.get(position);
            sectionText.setText(group);
            return sectionText;
        }
    }

    @Override
    public int getViewTypeCount() {
        return TYPE_COUNT;
    }

    @Override
    public int getCount() {
        return super.getCount() + sectionsIndexer.size();
    }

    @Override
    public boolean isEnabled(int position) {
        return getItemViewType(position) == TYPE_NORMAL;
    }

    private int getPositionForSection(int section) {
        if (sectionsIndexer.containsKey(section)) {
            return section + 1;
        }
        return section;
    }

    private int getSectionForPosition(int position) {
        int offset = 0;
        for (Integer key : sectionsIndexer.keySet()) {
            if (position > key) {
                offset++;
            } else {
                break;
            }
        }
        return position - offset;
    }

    @Override
    public Object getItem(int position) {
        if (getItemViewType(position) == TYPE_NORMAL) {
            return super.getItem(getSectionForPosition(position));
        }
        return super.getItem(position);
    }

    @Override
    public long getItemId(int position) {
        if (getItemViewType(position) == TYPE_NORMAL) {
            return super.getItemId(getSectionForPosition(position));
        }
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == getPositionForSection(position)) {
            return TYPE_NORMAL;
        }
        return TYPE_HEADER;
    }

    @Override
    public void changeCursor(Cursor cursor) {
        final Cursor old = swapCursor(cursor);
        if (old != null) {
            old.close();
        }
    }

    @Override
    public Cursor swapCursor(Cursor newCursor) {
        if (getCursor() != null) {
            getCursor().unregisterDataSetObserver(dataSetObserver);
        }
        final Cursor oldCursor = super.swapCursor(newCursor);
        calculateSectionHeaders();
        if (newCursor != null) {
            newCursor.registerDataSetObserver(dataSetObserver);
        }
        return oldCursor;
    }
}
