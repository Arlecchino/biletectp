package com.gb.ctptickets.ui.preference.helper;


import android.preference.ListPreference;
import android.preference.Preference;

import com.gb.ctptickets.ui.preference.helper.PreferenceHelper;
import com.gb.ctptickets.util.GoogleAnalyticsHelper;


/**
 * Because PreferenceActivity is not backward-compatible, I needed to create two Activities (one with PreferenceActivity
 * and another with ActionBarActivity and PreferenceFragment) and I put in this class the common operations related to
 * list preferences
 *
 * Created by GABOR on 12/15/2014.
 */
public class SimpleListPreferenceHelper extends PreferenceHelper<ListPreference> implements
		Preference.OnPreferenceChangeListener {

	@SuppressWarnings("WeakerAccess")
	public SimpleListPreferenceHelper(ListPreference preference) {
		super(preference);
	}


	public void setup() {
		preference.setOnPreferenceChangeListener(this);
		String value = preference.getValue();
		onPreferenceChange(preference, value);
	}


	@Override
	public boolean onPreferenceChange(Preference preference, Object value) {
		String stringValue = value.toString();

		if (preference instanceof ListPreference) {
			ListPreference listPreference = (ListPreference) preference;
			int index = listPreference.findIndexOfValue(stringValue);
			preference.setSummary(index >= 0 ? listPreference.getEntries()[index] : null);
		} else {
			preference.setSummary(stringValue);
		}
		GoogleAnalyticsHelper.onSettingChanged(preference.getKey(), stringValue);
		return true;
	}
}
