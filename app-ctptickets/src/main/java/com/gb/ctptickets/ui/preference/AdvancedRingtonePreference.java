package com.gb.ctptickets.ui.preference;


import android.content.Context;
import android.content.Intent;
import android.preference.RingtonePreference;
import android.util.AttributeSet;

import com.gb.ctptickets.ui.dialog.RingtonePickerDialogActivity;
import com.gb.ctptickets.util.AndroidRUtils;


/**
 * Allows the user to pick a ringtone. Uses {@link com.gb.ctptickets.ui.dialog.RingtonePickerDialogActivity} which accepts
 * custom ringtones as well.<br>
 * 
 * @see com.gb.ctptickets.ui.dialog.RingtonePickerDialogActivity#EXTRA_RINGTONE_CUSTOM_RINGTONES
 * 
 *      Created by gbiro on 9/24/2014.
 */
public class AdvancedRingtonePreference extends AutoSetupRingtonePreference {

	public AdvancedRingtonePreference(Context context, AttributeSet attrs) {
		super(context, attrs);
	}


	protected void onPrepareRingtonePickerIntent(Intent ringtonePickerIntent) {
		super.onPrepareRingtonePickerIntent(ringtonePickerIntent);
		ringtonePickerIntent.putExtra(RingtonePickerDialogActivity.EXTRA_RINGTONE_CUSTOM_RINGTONES,
				AndroidRUtils.getResourceUrisFromRSubClass(AndroidRUtils.RAW));
		// don't bother with the activity selector, just launch RingtonePickerDialog
		ringtonePickerIntent.setClass(getContext(), RingtonePickerDialogActivity.class);
	}
}
