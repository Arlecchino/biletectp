package com.gb.ctptickets.ui.dialog;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.gb.ctptickets.R;
import com.gb.ctptickets.provider.ticket.AvailableBusLineRegistry;
import com.gb.ctptickets.ui.view.GridViewCompat;


/**
 * Dialog to select a bus line
 *
 * Created by GABOR on 2014-nov.-23.
 *
 * @see BusLineSelectorDialogActivity
 */
public class BusLineSelectorDialog extends DialogFragment {

	static final String	EXTRA_SHOW_UNUSED_ONLY		= "EXTRA_SHOW_UNUSED_ONLY";
	static final String	EXTRA_SHOW_ADD_NEW_BUTTON	= "EXTRA_SHOW_ADD_NEW_BUTTON";

	public interface Callback {

		void onBusLineSelected(String line);
	}

	private Callback	callback;


	@SuppressWarnings("SameParameterValue")
	public static DialogFragment prepare(boolean unusedOnly, boolean showAddNewButton) {
		BusLineSelectorDialog dialog = new BusLineSelectorDialog();
		Bundle arguments = new Bundle();
		arguments.putBoolean(EXTRA_SHOW_UNUSED_ONLY, unusedOnly);
		arguments.putBoolean(EXTRA_SHOW_ADD_NEW_BUTTON, showAddNewButton);
		dialog.setArguments(arguments);
		return dialog;
	}


	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		this.callback = (Callback) activity;
	}


	@Override
	public void onDetach() {
		super.onDetach();
		this.callback = null;
		getActivity().finish();
	}


	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		boolean showUnusedBusLinesOnly = false;
		boolean showAddNewButton = false;

		if (getArguments() != null) {
			showUnusedBusLinesOnly = getArguments().getBoolean(EXTRA_SHOW_UNUSED_ONLY, false);
			showAddNewButton = getArguments().getBoolean(EXTRA_SHOW_ADD_NEW_BUTTON, true);
		}

		AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
		alert.setTitle(R.string.custom_bus_line_dialog_title);
		final GridViewCompat grid = (GridViewCompat) LayoutInflater.from(getActivity()).inflate(
				R.layout.line_selector_grid, null);
		String[] lines = AvailableBusLineRegistry.getAllBusLines(showAddNewButton, showUnusedBusLinesOnly);
		final ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.line_selector_item,
				android.R.id.text1, lines);
		grid.setAdapter(adapter);
		grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				String line = adapter.getItem(position);

				if (callback != null) {
					callback.onBusLineSelected(line);
				}
				dismiss();
			}
		});
		alert.setView(grid);
		alert.setNegativeButton(android.R.string.cancel, null);
		return alert.create();
	}
}