package com.gb.ctptickets.ui.preference.helper;


import android.content.Context;
import android.preference.Preference;

import com.gb.ctptickets.R;
import com.gb.ctptickets.ui.preference.helper.PreferenceHelper;
import com.gb.ctptickets.util.IntentUtils;


/**
 * Created by gbiro on 5/22/2015.
 */
public class DeveloperPreferenceHelper extends PreferenceHelper<Preference> {

	public DeveloperPreferenceHelper(Preference preference) {
		super(preference);
	}


	public void setup() {
		preference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

			@Override
			public boolean onPreferenceClick(Preference preference) {
				Context context = preference.getContext();
				context.startActivity(IntentUtils.getSendMailIntent(context.getString(R.string.feedback_mail),
						context.getString(R.string.app_name), context.getString(R.string.developer)));
				return false;
			}
		});
	}
}
