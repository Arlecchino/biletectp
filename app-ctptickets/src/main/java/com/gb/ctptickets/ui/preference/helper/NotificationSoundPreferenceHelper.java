package com.gb.ctptickets.ui.preference.helper;


import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.gb.ctptickets.App;
import com.gb.ctptickets.BuildConfig;
import com.gb.ctptickets.R;
import com.gb.ctptickets.ui.dialog.RingtonePickerDialogActivity;
import com.gb.ctptickets.util.GoogleAnalyticsHelper;


/**
 * Because PreferenceActivity is not backward-compatible, I needed to create two Activities (one with PreferenceActivity
 * and another with ActionBarActivity and PreferenceFragment) and I put in this class the common operations related to
 * the notification sound preference
 * <p/>
 * Created by GABOR on 2014-szept.-28.
 */
public class NotificationSoundPreferenceHelper extends PreferenceHelper<Preference> {

	public NotificationSoundPreferenceHelper(Preference preference) {
		super(preference);
	}


	public void setup() {
		final Context context = App.getContext();
		final String notificationSoundPreferenceKey = context.getString(R.string.pref_notification_sound);

		String value = PreferenceManager.getDefaultSharedPreferences(context).getString(notificationSoundPreferenceKey,
				"android.resource://" + BuildConfig.APPLICATION_ID + "/raw/woop_woop");

		if (!TextUtils.isEmpty(value)) {
			preference.setSummary(RingtonePickerDialogActivity.getRingtoneTitle(context, Uri.parse(value)));
			PreferenceManager.getDefaultSharedPreferences(context).edit()
					.putString(notificationSoundPreferenceKey, value).commit();
		} else {
			preference.setSummary(Resources.getSystem().getIdentifier("ringtone_silent", "string", "android"));
		}

		preference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {

			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				if (!TextUtils.isEmpty(newValue.toString())) {
					String title = RingtonePickerDialogActivity.getRingtoneTitle(context,
							Uri.parse(newValue.toString()));
					preference.setSummary(title);
				} else {
					preference.setSummary(Resources.getSystem().getIdentifier("ringtone_silent", "string", "android"));
				}
				GoogleAnalyticsHelper.onSettingChanged(preference.getKey(), newValue.toString());
				return true;
			}
		});
	}
}
