package com.gb.ctptickets.ui.view;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gb.ctptickets.App;
import com.gb.ctptickets.R;
import com.gb.ctptickets.UserSettings;
import com.gb.ctptickets.provider.ticket.TicketHandlerFactory;
import com.gb.ctptickets.provider.ticket.TicketRequestStatusRegistry;
import com.gb.ctptickets.provider.ticket.TicketUIProvider;
import com.gb.ctptickets.ui.eventbus.CurrencyRateChangedEvent;
import com.gb.ctptickets.ui.eventbus.TicketStatusChangedEvent;
import com.gb.ctptickets.util.HumanTime;

import java.util.TimerTask;

import de.greenrobot.event.EventBus;


/**
 * Every TicketLayout has a value which is the bus line or abonament it is associated with.<br>
 * Special TicketLayouts are an exception. {@link com.gb.ctptickets.ui.view.TicketLayoutQuick}s' value is the current
 * selection,<br>
 * {@link com.gb.ctptickets.ui.view.TicketLayoutAdd}s' value is null<br>
 * <br>
 * Created by gbiro on 2014-Jun-19.
 *
 * @see #getValue()
 */
public class TicketLayout extends RelativeLayout {

	private View	        cardViewRoot;

	private TextView		titleTV;
	private TextView		subtitleTV;
	private ImageView		ticketRequestStatusIcon;
	private TextView		ticketCountdownTV;
	private View			shadowOverlay;

	private String			title;
	private String			subtitle;
	private String			value;

	private String			comment;

	private Handler			timer;
	private CountdownTask	countdownTask;


	public TicketLayout(Context context, String value) {
		this(context, value, null, null);
	}


	@SuppressWarnings("WeakerAccess")
	public TicketLayout(Context context, String value, String title, @SuppressWarnings("SameParameterValue") String subtitle) {
		super(context);
		this.value = value;
		View.inflate(context, R.layout.ticket, this);
		initUI(context, title, subtitle, value);
		timer = new Handler();
		updateTicketRequestState();
	}


	@SuppressWarnings("WeakerAccess")
	public TicketLayout(Context context, AttributeSet attrs) {
		super(context, attrs);

		if (attrs != null) {
			TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.TicketLayout, 0, 0);

			try {
				value = a.getString(R.styleable.TicketLayout_value);
				title = a.getString(R.styleable.TicketLayout_title_);
				subtitle = a.getString(R.styleable.TicketLayout_subtitle_);
			} finally {
				a.recycle();
			}
		}
		View.inflate(context, R.layout.ticket, this);
		initUI(context, title, subtitle, value);
		updateTicketRequestState();
	}


	public static UIState getStateForShortestExpiryTicket(TicketRequestStatusRegistry provider) {
		long id = provider.getIdForTicketWithClosestExpiration();

		if (id >= 0) {
			if (provider.isValid(id)) {
				return TicketLayout.UIState.RECEIVED;
			} else if (provider.isRequestFailed(id)) {
				return TicketLayout.UIState.FAILED;
			} else if (provider.isRequestSent(id)) {
				if (provider.isRequestDelivered(id)) {
					return TicketLayout.UIState.DELIVERED;
				} else {
					return TicketLayout.UIState.SENT;
				}
			}
		}
		return TicketLayout.UIState.UNKNOWN;
	}


	/**
	 * Inflate the layout and fill it with content
	 *
	 * @param context
	 *            RadioButton in the ticket layout to be visible or not
	 * @param title
	 *            description that appears in the ticket layout
	 */
	void initUI(Context context, String title, String subtitle, String value) {
        cardViewRoot = findViewById(R.id.card_view);
		titleTV = (TextView) findViewById(R.id.title);
		subtitleTV = (TextView) findViewById(R.id.subtitle);
		ticketRequestStatusIcon = (ImageView) findViewById(R.id.ticket_request_status);
		ticketCountdownTV = (TextView) findViewById(R.id.ticket_countdown);
		shadowOverlay = findViewById(R.id.shadow_overlay);
		shadowOverlay.setVisibility(View.GONE);
		this.title = title;
		this.subtitle = subtitle;

		updateTitle(this.title);
		updateSubtitle(this.subtitle, value);
		setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
	}


	public void setStatusIconClickListener(OnClickListener listener) {
		if (ticketRequestStatusIcon != null) {
			ticketRequestStatusIcon.setOnClickListener(listener);
		}
	}


	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
		EventBus.getDefault().register(this);
	}


	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		EventBus.getDefault().unregister(this);
	}


	private void updateTitle(String title) {
		String text = null;

		if (!TextUtils.isEmpty(title)) {
			text = title;
		} else if (!TextUtils.isEmpty(value)) {
			TicketUIProvider provider = TicketHandlerFactory.getUIProvider(value);
			text = provider.getShortTitle();
		}
		titleTV.setText(text);
	}


	private void updateSubtitle(String subtitle, String value) {
		String text = null;

		if (!TextUtils.isEmpty(subtitle)) {
			text = subtitle;
		} else if (!TextUtils.isEmpty(value)) {
			TicketUIProvider provider = TicketHandlerFactory.getUIProvider(value);
			text = provider.getPrice(UserSettings.getCurrentTicketRate());
		} else {
			subtitleTV.setVisibility(View.GONE);
		}
		subtitleTV.setText(text);
	}


	public String getValue() {
		return value;
	}


	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		boolean result = super.dispatchTouchEvent(event);
		int action = event.getActionMasked();

		if (cardViewRoot instanceof CardView) {
			if (action == MotionEvent.ACTION_DOWN) {
				((CardView)cardViewRoot).setCardBackgroundColor(getResources().getColor(R.color.ticket_green_pressed));
			} else if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_CANCEL) {
				((CardView)cardViewRoot).setCardBackgroundColor(getResources().getColor(R.color.ticket_green));
			}
		}
		return result;
	}


	@Override
	public String toString() {
		return "TicketLayout(" + getValue() + ")";
	}


	void updateTicketRequestState() {
		if (!TextUtils.isEmpty(getValue())) {
			TicketRequestStatusRegistry registry = TicketHandlerFactory.getRequestStatusRegistry(getValue());

			long id = registry.getIdForTicketWithClosestExpiration();

			if (id >= 0) {
				updateTicketRequestState(TicketLayout.getStateForShortestExpiryTicket(registry),
						registry.getStatusDescription(id));
			} else {
				ticketRequestStatusIcon.setVisibility(View.GONE);
				ticketCountdownTV.setVisibility(View.GONE);
			}
		} else {
			ticketRequestStatusIcon.setVisibility(View.GONE);
			ticketCountdownTV.setVisibility(View.GONE);
		}
	}


	private void updateTicketRequestState(UIState state, String comment) {
		this.comment = comment;

		if (UIState.RECEIVED == state) {
			ticketRequestStatusIcon.setVisibility(View.GONE);
			ticketCountdownTV.setVisibility(View.VISIBLE);

			if (countdownTask == null) {
				countdownTask = new CountdownTask();
				timer.post(countdownTask);
			}
		} else {
			Drawable drawable = state.getDrawable(getContext());

			if (drawable != null) {
				ticketRequestStatusIcon.setImageDrawable(drawable);
				ticketRequestStatusIcon.setVisibility(View.VISIBLE);
			} else {
				ticketRequestStatusIcon.setVisibility(View.GONE);
			}
			ticketCountdownTV.setVisibility(View.GONE);

			if (countdownTask != null) {
				countdownTask.cancel();
				countdownTask = null;
			}
		}
	}


	private void updateTicketTimeoutCountdown() {
		TicketRequestStatusRegistry registry = TicketHandlerFactory.getRequestStatusRegistry(getValue());
		long id = registry.getIdForTicketWithClosestExpiration();

		if (id >= 0) {
			long delta = registry.getExpiryDate(id) - System.currentTimeMillis();

			if (delta <= HumanTime.Time.SECOND.valueInMilisecs) {
				delta = 0;
			}

			HumanTime ht = new HumanTime(delta);

			if (delta > 60000) {
				ticketCountdownTV.setText(ht.setCompact(true).getApproximately(2));
			} else if (delta > 0) {
				ticketCountdownTV.setText(ht.setCompact(true).getApproximately(1));
			} else {
				updateTicketRequestState(UIState.UNKNOWN, null);
				EventBus.getDefault().post(new TicketStatusChangedEvent(getValue()));
			}
		} else {
			updateTicketRequestState(UIState.UNKNOWN, null);
			EventBus.getDefault().post(new TicketStatusChangedEvent(getValue()));
		}
	}


	public String getComment() {
		return comment;
	}


	@SuppressWarnings("unused")
	public void onEventMainThread(@SuppressWarnings("UnusedParameters") CurrencyRateChangedEvent event) {
		updateSubtitle(this.subtitle, this.value);
	}


	@SuppressWarnings("unused")
	public void onEventMainThread(TicketStatusChangedEvent event) {
		if (event.ticket.equals(getValue())) {
			updateTicketRequestState();
		}
	}

	/**
	 * Depending on the UI state set this TicketLayout will sport various visual indicators
	 */
	public enum UIState {
		/**
		 * We have no information about any request for this ticket. Active if<br>
		 * - there is no ongoing request for this ticket (no sms was yet sent)<br>
		 * - a request sms was sent but no response sms and an hour passed<br>
		 * - the ticket was successfully bought but then expired
		 */
		UNKNOWN(0),

		/**
		 * The request sms was successfully sent to the mobile operator. Followed by the DELIVERED state, otherwise it
		 * switches back to UNKNOWN after some time.
		 */
		SENT(R.drawable.ic_action_tick),

		/**
		 * The mobile operator confirmed the arrival of the sms to the destination. Followed by the RECEIVED state,
		 * otherwise is switches to FAILED after 25 seconds and to UNKNOWN after some time.
		 */
		DELIVERED(R.drawable.ic_action_tick),

		/**
		 * A response sms arrived and it is not yet expired. After the ticket expires the state changes back to UNKNOWN.
		 */
		RECEIVED(0),

		/**
		 * Either the request sms couldn't be sent, delivered or a response didn't arrive after 25 seconds. Switches
		 * back to UNKNOWN after some time.
		 */
		FAILED(R.drawable.ic_action_error);

		private final int	imageRes;


		UIState(int imageRes) {
			this.imageRes = imageRes;
		}


		public Drawable getDrawable(Context context) {
			if (imageRes > 0) {
				//noinspection deprecation
				return context.getResources().getDrawable(imageRes);
			} else {
				return null;
			}
		}


		public int getImageRes() {
			return imageRes;
		}
	}

	private class CountdownTask extends TimerTask {

		@Override
		public void run() {
			updateTicketTimeoutCountdown();
			timer.postDelayed(this, 1000);
		}
	}


	@Override
	public void setSelected(boolean selected) {
		super.setSelected(selected);
		shadowOverlay.setVisibility(selected ? View.VISIBLE : View.GONE);
	}
}
