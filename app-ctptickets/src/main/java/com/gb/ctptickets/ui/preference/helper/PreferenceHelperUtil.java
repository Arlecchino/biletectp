package com.gb.ctptickets.ui.preference.helper;


import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import android.content.res.TypedArray;
import android.preference.Preference;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;

import com.gb.ctptickets.App;


/**
 * Created by GABOR on 2015-maj.-31.
 */
public class PreferenceHelperUtil {

	private static final String	TAG	= PreferenceHelperUtil.class.getSimpleName();


	public static <T extends Preference> PreferenceHelper<T> getHelper(Preference preference, AttributeSet set,
			int[] attrs, int helperAttrId) {
		PreferenceHelper<T> helper = null;

		if (set != null) {
			TypedArray a = App.getContext().getTheme().obtainStyledAttributes(set, attrs, 0, 0);
			String helperClass = a.getString(helperAttrId);

			if (!TextUtils.isEmpty(helperClass))
				try {
					//noinspection unchecked
					Constructor[] constructors = Class.forName(helperClass).getDeclaredConstructors();

					for (Constructor constructor : constructors) {
						if (constructor.getParameterTypes().length == 1
								&& Preference.class.isAssignableFrom(constructor.getParameterTypes()[0])) {
							helper = (PreferenceHelper<T>) constructor.newInstance(preference);
							break;
						}
					}
					if (helper == null) {
						Log.e(TAG, "Class " + helperClass + " has no constructor with "
								+ preference.getClass().getSuperclass().getName());
					}
				} catch (ClassNotFoundException e) {
					Log.e(TAG, "Class " + helperClass + " not found");
				} catch (InstantiationException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				} finally {
					a.recycle();
				}
		}
		return helper;
	}
}
