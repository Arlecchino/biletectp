package com.gb.ctptickets.ui.dialog;


import java.util.Arrays;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.MergeCursor;
import android.media.MediaMetadataRetriever;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.CheckedTextView;
import android.widget.ListView;
import android.widget.TextView;

import com.gb.ctptickets.R;
import com.gb.ctptickets.ui.activity.DialogActivity;
import com.gb.ctptickets.ui.adapter.SectionCursorAdapter;
import com.gb.ctptickets.util.ArrayUtils;


/**
 * Modified com.android.internal.app.RingtonePickerActivity. It's actually an Activity that looks like a dialog. If you
 * pass an array of {@link android.net.Uri}s with the {@link #EXTRA_RINGTONE_CUSTOM_RINGTONES} they will be also
 * displayed.
 *
 * @see com.gb.ctptickets.ui.activity.DialogActivity
 * @see #EXTRA_RINGTONE_CUSTOM_RINGTONES
 * <p/>
 */
public class RingtonePickerDialogActivity extends DialogActivity {

    /**
     * Array of strings (uri's)
     */
    public static final String EXTRA_RINGTONE_CUSTOM_RINGTONES = "EXTRA_RINGTONE_CUSTOM_RINGTONES";

    private static final String[] MEDIA_COLUMNS = new String[]{
            MediaStore.Audio.Media._ID, MediaStore.Audio.Media.TITLE,
            "\"" + MediaStore.Audio.Media.EXTERNAL_CONTENT_URI + "\"", MediaStore.Audio.Media.TITLE_KEY
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RingtonePickerDialog dialog = new RingtonePickerDialog();
        dialog.setArguments(getIntent().getExtras());
        dialog.show(getSupportFragmentManager(), null);
    }

    public static class RingtonePickerDialog extends DialogFragment implements DialogInterface.OnClickListener,
            Runnable, AdapterView.OnItemSelectedListener {

        private static final int DELAY_MS_SELECTION_PLAYED = 300;

        private static final String SAVE_CLICKED_POS = "clicked_pos";

        private RingtoneManager mRingtoneManager;

        private Cursor mCursor;
        private Handler mHandler;

        /**
         * The position in the list of the 'Silent' item.
         */
        private int mSilentPos = -1;

        /**
         * The position in the list of the 'Default' item.
         */
        private int mDefaultRingtonePos = -1;

        /**
         * The position in the list of the last clicked item.
         */
        private int mClickedPos = -1;

        /**
         * The position in the list of the ringtone to sample.
         */
        private int mSampleRingtonePos = -1;

        /**
         * Whether this list has the 'Silent' item.
         */
        private boolean mHasSilentItem;

        /**
         * The Uri to place a checkmark next to.
         */
        private Uri mExistingUri;

        /**
         * The number of static items in the list.
         */
        private int mStaticItemCount;

        /**
         * Whether this list has the 'Default' item.
         */
        private boolean mHasDefaultItem;

        /**
         * The Uri to play when the 'Default' item is clicked.
         */
        private Uri mUriForDefaultItem;

        /**
         * A Ringtone for the default ringtone. In most cases, the RingtoneManager will stop the previous ringtone.
         * However, the RingtoneManager doesn't manage the default ringtone for us, so we should stop this one manually.
         */
        private Ringtone mDefaultRingtone;
        private Ringtone mCurrentRingtone;

        private Uri[] customRingtones;

        private ListView listView;
        private SectionCursorAdapter adapter;

        public RingtonePickerDialog() {
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            mHandler = new Handler();
            Bundle args = getArguments();
            /*
             * Get whether to show the 'Default' item, and the URI to play when the default is clicked
			 */
            mHasDefaultItem = args.getBoolean(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, true);
            mUriForDefaultItem = args.getParcelable(RingtoneManager.EXTRA_RINGTONE_DEFAULT_URI);

            if (mUriForDefaultItem == null) {
                mUriForDefaultItem = Settings.System.DEFAULT_RINGTONE_URI;
            }

            if (savedInstanceState != null) {
                mClickedPos = savedInstanceState.getInt(SAVE_CLICKED_POS, -1);
            }
            // Get whether to show the 'Silent' item
            mHasSilentItem = args.getBoolean(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, true);

            // Give the Activity so it can do managed queries
            mRingtoneManager = new RingtoneManager(getActivity());

            // Get the types of ringtones to show
            int types = args.getInt(RingtoneManager.EXTRA_RINGTONE_TYPE, -1);
            if (types != -1) {
                mRingtoneManager.setType(types);
            }

            CharSequence title = args.getCharSequence(RingtoneManager.EXTRA_RINGTONE_TITLE);

            // The volume keys will control the stream that we are choosing a ringtone for
            getActivity().setVolumeControlStream(mRingtoneManager.inferStreamType());

            // Get the URI whose list item should have a checkmark
            mExistingUri = args.getParcelable(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI);

            Parcelable[] parcelables = args.getParcelableArray(EXTRA_RINGTONE_CUSTOM_RINGTONES);
            customRingtones = new Uri[parcelables.length];

            for (int i = 0; i < parcelables.length; i++) {
                customRingtones[i] = (Uri) parcelables[i];
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            listView = new ListView(getActivity());
            listView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);

            if (mHasDefaultItem) {
                mDefaultRingtonePos = addDefaultRingtoneItem(listView);

                if (RingtoneManager.isDefault(mExistingUri)) {
                    mClickedPos = mDefaultRingtonePos;
                }
            }

            if (mHasSilentItem) {
                mSilentPos = addSilentItem(listView);

                // The 'Silent' item should use a null Uri
                if (mExistingUri == null) {
                    mClickedPos = mSilentPos;
                }
            }

            MatrixCursor customCursor = new MatrixCursor(MEDIA_COLUMNS);

            for (int i = 0; i < customRingtones.length; i++) {
                String ringtoneTitle = getRingtoneTitle(getActivity(), customRingtones[i]);

                if (!TextUtils.isEmpty(ringtoneTitle)) { // ignoring invalid ringtones
                    customCursor.addRow(new Object[]{
                            0, ringtoneTitle, customRingtones[i], null
                    });
                } else {
                    customRingtones[i] = null;
                }
            }
            customRingtones = ArrayUtils.deleteNulls(Uri.class, customRingtones);

            if (mClickedPos == -1) {
                int ringtoneIndex = mRingtoneManager.getRingtonePosition(mExistingUri);

                if (ringtoneIndex < 0) {// not system ringtone
                    ringtoneIndex = getCustomRingtoneIndex(mExistingUri);
                    mClickedPos = getListPositionForCustomRingtoneIndex(ringtoneIndex);
                } else {
                    mClickedPos = getListPositionForSystemRingtoneIndex(ringtoneIndex);
                }
            }

            mCursor = mRingtoneManager.getCursor();
            mCursor = new MergeCursor(new Cursor[]{
                    customCursor, mCursor
            });

            builder.setView(listView);
            builder.setTitle(title);
            builder.setPositiveButton(android.R.string.ok, this);
            builder.setNegativeButton(android.R.string.cancel, this);

            adapter = new SectionCursorAdapter(getActivity(), mCursor, android.R.layout.preference_category, 2) {

                @Override
                public View newView(Context context, Cursor cursor, ViewGroup parent) {
                    return generateItem(context);
                }


                @Override
                public void bindView(View view, Context context, Cursor cursor) {
                    TextView text = (TextView) view;

                    if (!cursor.isClosed()) {
                        text.setText(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE)));
                    }
                }


                @Override
                protected String getCustomGroup(String groupData) {
                    if (groupData.startsWith("android.resource")) {
                        return getString(R.string.app_ringtones);
                    } else {
                        return getString(R.string.system_ringtones);
                    }
                }
            };
            listView.setAdapter(adapter);
            listView.setItemChecked(mClickedPos, true);
            listView.setSelection(mClickedPos);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                public void onItemClick(AdapterView parent, View v, int position, long id) {
                    // Save the position of most recently clicked item
                    mClickedPos = position;

                    // Play clip
                    playRingtone(position, 0);
                }
            });
            listView.setOnItemSelectedListener(this);
            return builder.create();
        }


        @Override
        public void onDetach() {
            super.onDetach();
            getActivity().finish();
        }


        @Override
        public void onSaveInstanceState(Bundle outState) {
            super.onSaveInstanceState(outState);
            outState.putInt(SAVE_CLICKED_POS, mClickedPos);
        }


        /*
         * On item selected via keys
         */
        public void onItemSelected(AdapterView parent, View view, int position, long id) {
            playRingtone(position, DELAY_MS_SELECTION_PLAYED);
        }


        public void onNothingSelected(AdapterView parent) {
            // nothing to do
        }


        @Override
        public void onClick(DialogInterface dialog, int which) {
            boolean positiveResult = which == DialogInterface.BUTTON_POSITIVE;

            // Stop playing the previous ringtone
            mRingtoneManager.stopPreviousRingtone();

            if (positiveResult) {
                Intent resultIntent = new Intent();
                Uri uri;

                if (mClickedPos == mDefaultRingtonePos) {
                    // Set it to the default Uri that they originally gave us
                    uri = mUriForDefaultItem;
                } else if (mClickedPos == mSilentPos) {
                    // A null Uri is for the 'Silent' item
                    uri = null;
                } else {
                    int position = getRingtoneManagerIndex(mClickedPos);

                    if (position < 0) {
                        uri = customRingtones[getCustomRingtoneIndex(mClickedPos)];
                    } else {
                        uri = mRingtoneManager.getRingtoneUri(position);
                    }
                }

                resultIntent.putExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI, uri);
                getActivity().setResult(RESULT_FIRST_USER, resultIntent);
            } else {
                getActivity().setResult(RESULT_CANCELED);
            }
        }


        private void playRingtone(int position, int delayMs) {
            mHandler.removeCallbacks(this);
            mSampleRingtonePos = position;
            mHandler.postDelayed(this, delayMs);
        }


        @Override
        public void run() {
            /*
             * Stop the default ringtone, if it's playing (other ringtones will be stopped by the RingtoneManager when
			 * we get another Ringtone from it.
			 */
            if (mDefaultRingtone != null && mDefaultRingtone.isPlaying()) {
                mDefaultRingtone.stop();
                mDefaultRingtone = null;
            }

			/*
             * Stop the custom, ringtone, if it's playing (other ringtones will be stopped by the RingtoneManager when
			 * we get another Ringtone from it.
			 */
            if (mCurrentRingtone != null && mCurrentRingtone.isPlaying()) {
                mCurrentRingtone.stop();
                mCurrentRingtone = null;
            }

            if (mSampleRingtonePos == mSilentPos) {
                mRingtoneManager.stopPreviousRingtone();
                return;
            }

            if (mSampleRingtonePos == mDefaultRingtonePos) {
                if (mDefaultRingtone == null) {
                    mDefaultRingtone = RingtoneManager.getRingtone(getActivity(), mUriForDefaultItem);
                }
                mCurrentRingtone = mDefaultRingtone;

				/*
                 * Normally the non-static RingtoneManager.getRingtone stops the previous ringtone, but we're getting
				 * the default ringtone outside of the RingtoneManager instance, so let's stop the previous ringtone
				 * manually.
				 */
                mRingtoneManager.stopPreviousRingtone();
            } else {
                int position = getRingtoneManagerIndex(mSampleRingtonePos);

                if (position < 0) {
                    position = getCustomRingtoneIndex(mSampleRingtonePos);
                    mCurrentRingtone = RingtoneManager.getRingtone(getActivity(), customRingtones[position]);
                } else {
                    mCurrentRingtone = mRingtoneManager.getRingtone(position);
                }
            }

            if (mCurrentRingtone != null) {
                mCurrentRingtone.play();
            }
        }


        @Override
        public void onStop() {
            super.onStop();
            stopAnyPlayingRingtone();
        }


        @Override
        public void onPause() {
            super.onPause();
            stopAnyPlayingRingtone();
        }


        private void stopAnyPlayingRingtone() {
            if (mDefaultRingtone != null && mDefaultRingtone.isPlaying()) {
                mDefaultRingtone.stop();
            }

            if (mRingtoneManager != null) {
                mRingtoneManager.stopPreviousRingtone();
            }

            if (mCurrentRingtone != null && mCurrentRingtone.isPlaying()) {
                mCurrentRingtone.stop();
            }
        }


        /**
         * Adds a static item to the top of the list. A static item is one that is not from the RingtoneManager.
         *
         * @param listView  The ListView to add to.
         * @param textResId The resource ID of the text for the item.
         * @return The position of the inserted item.
         */
        private int addStaticItem(ListView listView, int textResId) {
            CheckedTextView view = (CheckedTextView) generateItem(getActivity());
            view.setText(textResId);
            listView.addHeaderView(view);
            mStaticItemCount++;
            return listView.getHeaderViewsCount() - 1;
        }


        private TextView generateItem(Context context) {
            TextView view = (TextView) ((LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE)).inflate(
                    android.R.layout.select_dialog_singlechoice, listView, false);
            TypedValue outValue = new TypedValue();
            context.getTheme().resolveAttribute(android.R.attr.textColorAlertDialogListItem, outValue, true);

            if (outValue.resourceId == 0) {
                // for example on platform 10 this attribute doesn't exist
                view.setTextColor(getResources().getColor(android.R.color.white));
            } else {
                view.setTextColor(getResources().getColor(outValue.resourceId));
            }
            return view;
        }


        private int addDefaultRingtoneItem(ListView listView) {
            int id = Resources.getSystem().getIdentifier("ringtone_default", "string", "android");
            return addStaticItem(listView, id);
        }


        private int addSilentItem(ListView listView) {
            int id = Resources.getSystem().getIdentifier("ringtone_silent", "string", "android");
            return addStaticItem(listView, id);
        }


        private int getCustomRingtoneIndex(Uri customRingtone) {
            return Arrays.binarySearch(customRingtones, customRingtone);
        }


        private int getListPositionForCustomRingtoneIndex(int index) {
            return index + mStaticItemCount + (customRingtones.length > 0 ? 1 : 0);
        }


        /**
         * Get the actual position in the list of a system ringtone
         */
        private int getListPositionForSystemRingtoneIndex(int ringtoneManagerIndex) {
            // If the manager position is -1 (for not found), return that
            if (ringtoneManagerIndex < 0) {
                return ringtoneManagerIndex;
            }
            return ringtoneManagerIndex + mStaticItemCount
                    + (customRingtones.length > 0 ? customRingtones.length + 2 : 0);
        }


        /**
         * Get the index of the custom ringtone from the actual list position
         *
         * @param listPosition
         * @return
         */
        private int getCustomRingtoneIndex(int listPosition) {
            if (listPosition <= mStaticItemCount + (customRingtones.length > 0 ? customRingtones.length : 0)) {
                return listPosition - mStaticItemCount - (customRingtones.length > 0 ? 1 : 0);
            } else {
                return -1;
            }
        }


        /**
         * Get the index of the system ringtone from the actual list position
         *
         * @param listPosition
         * @return integer higher or equal to 0 if the specified listPosition points to a system ringtone, -1 otherwise
         */
        private int getRingtoneManagerIndex(int listPosition) {
            if (listPosition > mStaticItemCount + (customRingtones.length > 0 ? customRingtones.length : 0)) {
                return listPosition - mStaticItemCount - (customRingtones.length > 0 ? customRingtones.length + 2 : 0);
            } else {
                return -1;
            }
        }
    }


    public static String getRingtoneTitle(Context context, Uri uri) {
        String title = null;
        try {
            title = getRingtoneTitleCompat(context, uri);
        } catch (IllegalArgumentException e) {
            // probably not a ringtone
        }
        if (TextUtils.isEmpty(title)) {
            try {
                Ringtone r = RingtoneManager.getRingtone(context, uri);
                title = r.getTitle(context);
            } catch (IllegalArgumentException e) {
                // even less probable that it's a ringtone
            }
        }
        return !uri.getLastPathSegment().equals(title) ? title : null;
    }

    @TargetApi(Build.VERSION_CODES.GINGERBREAD_MR1)
    private static String getRingtoneTitleCompat(Context context, Uri uri) throws IllegalArgumentException {
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        mmr.setDataSource(context, uri);
        return mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
    }
}
