package com.gb.ctptickets.ui.activity;


import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.gb.ctptickets.App;
import com.gb.ctptickets.R;
import com.gb.ctptickets.UserSettings;
import com.gb.ctptickets.ui.view.TicketLayout;
import com.gb.ctptickets.util.Animator;
import com.nineoldandroids.animation.AnimatorListenerAdapter;


class MainActivityTicketManager {

    private final Map<String, TicketGroup> ticketGroups;

    private final LinearLayout container;

    private static class TicketGroup {
        TextView header;
        TableLayout container;
    }


    public MainActivityTicketManager(LinearLayout container) {
        ticketGroups = new HashMap<>();
        this.container = container;
    }


    public void setTicketGroupVisibility(String ticketGroup, boolean visible) {
        TicketGroup group = ticketGroups.get(ticketGroup);

        if (group != null) {
            group.header.setVisibility(visible ? View.VISIBLE : View.GONE);
            group.container.setVisibility(visible ? View.VISIBLE : View.GONE);
        }
    }


    public void addTicketLayout(String ticketGroup, final TicketLayout ticketLayout, Context context) {
        TicketGroup group = ticketGroups.get(ticketGroup);

        if (group == null) {
            group = new TicketGroup();
            LinearLayout view = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.ticket_group, null);
            container.addView(view);
            group.header = (TextView) view.findViewById(R.id.header);
            group.header.setText(ticketGroup);
            group.container = (TableLayout) view.findViewById(R.id.container);
            ticketGroups.put(ticketGroup, group);
        }

        addTicketLayoutToTable(group.container, ticketLayout, context);
    }


    /**
     * Updates the table so that it can receive one more {@link com.gb.ctptickets.ui.view.TicketLayout} and actually
     * adds the View
     *
     * @param table        one of the three tables: tableCustomTickets, tableDefaultMetroUrbanTickets, tableDefaultUrbanTickets
     * @param ticketLayout ticket layout to be added
     */
    private static void addTicketLayoutToTable(TableLayout table, final TicketLayout ticketLayout, Context context) {
        int columnSpacing = App.getContext().getResources().getDimensionPixelSize(R.dimen.half_spacing);
        int rowSpacing = App.getContext().getResources().getDimensionPixelSize(R.dimen.spacing);
        // fetch the top table row if there is space in it, create a new one otherwise

        TableRow lastTableRow = null;
        View child = table.getChildAt(0);

        if (child instanceof ViewGroup && ((ViewGroup) child).getChildCount() < 2) {
            lastTableRow = (TableRow) child;
        }

        if (lastTableRow == null) {
            // create a new table row separator
            TableLayout.LayoutParams layoutParams = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                    rowSpacing);
            View separator = new View(context);
            table.addView(separator, 0, layoutParams);

            // create a new table row
            lastTableRow = new TableRow(context);
            layoutParams = new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT,
                    TableLayout.LayoutParams.WRAP_CONTENT);
            table.addView(lastTableRow, 0, layoutParams);
        }

        // add the ticket to the top table row
        TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT);

        if (lastTableRow.getChildCount() == 0) {
            // left, top, right, bottom
            layoutParams.setMargins(0, 0, columnSpacing, 0);
        } else {
            layoutParams.setMargins(columnSpacing, 0, 0, 0);
        }
        lastTableRow.addView(ticketLayout, layoutParams);
    }


    public void clearTickets() {
        for (TicketGroup group : ticketGroups.values()) {
            if (group != null) {
                group.container.removeAllViews();
            }
        }
    }

    /**
     * The deletion of a custom ticket was requested by the user, remove it from the db and table
     */
    public void removeCustomTicketFromTable(final TicketLayout layout, final Runnable runOnAnimationFinished) {
        new Animator(layout).hide(new AnimatorListenerAdapter() {

            @Override
            public void onAnimationEnd(com.nineoldandroids.animation.Animator animation) {
                UserSettings.removeCustomTicket(layout.getValue());
                runOnAnimationFinished.run();
            }
        });
    }
}
