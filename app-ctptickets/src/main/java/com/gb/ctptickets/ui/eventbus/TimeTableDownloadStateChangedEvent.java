package com.gb.ctptickets.ui.eventbus;


/**
 * Event signifying either the finish or failure of a timetable download
 *
 * Created by GABOR on 2014-dec.-01.
 */
public class TimeTableDownloadStateChangedEvent {

	public static final int	STATE_FINISHED	= 1;
	public static final int	STATE_ERROR		= 2;

	@SuppressWarnings({"WeakerAccess", "unused"})
	public final String		busLine;
	public final int		state;
	public final String		reasonStr;


	public TimeTableDownloadStateChangedEvent(String busLine, int state, String reasonStr) {
		this.busLine = busLine;
		this.state = state;
		this.reasonStr = reasonStr;
	}
}
