package com.gb.ctptickets.ui.preference.helper;


import android.preference.Preference;
import android.widget.Toast;

import com.gb.ctptickets.R;
import com.gb.ctptickets.provider.timetable.TimeTableHandler;
import com.gb.ctptickets.ui.preference.helper.PreferenceHelper;


public class ClearTimetablesPreferenceHelper extends PreferenceHelper<Preference> {

	public ClearTimetablesPreferenceHelper(Preference preference) {
		super(preference);
	}


	public void setup() {
		preference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

			@Override
			public boolean onPreferenceClick(Preference preference) {
				int count = TimeTableHandler.deleteAllFromDevice();
				Toast.makeText(preference.getContext(),
						preference.getContext().getResources().getQuantityString(R.plurals.files_deleted, count, count), Toast.LENGTH_SHORT).show();
				return false;
			}
		});
	}
}
