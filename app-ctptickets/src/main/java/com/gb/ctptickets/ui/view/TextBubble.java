package com.gb.ctptickets.ui.view;


import android.content.Context;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.gb.ctptickets.R;


/**
 * Bubble with pointy end and a piece of text
 *
 * Created by gbiro on 11/11/2014.
 */
public class TextBubble extends Bubble {

	private final String	text;


	public TextBubble(Context context, String text) {
		super(context);
		this.text = text;
	}


	@Override
	public View getContent() {
		TextView content = new TextView(getContext());
		content.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
		int px = getContext().getResources().getDimensionPixelSize(R.dimen.spacing);
		content.setPadding(px, px, px, px);
		content.setText(text);
		content.setGravity(Gravity.CENTER);
		return content;
	}
}
