package com.gb.ctptickets.ui.view;


import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupWindow;

import com.gb.ctptickets.Constants;
import com.gb.ctptickets.R;


/**
 * Custom text popup window.
 *
 * Created by GABOR on 2014-okt.-11.
 */
public abstract class Bubble {

	private static final int	LEFT_MARGIN		= 10;
	private static final int	RIGHT_MARGIN	= 10;

	private final Context		mContext;
	private PopupWindow			mWindow;
	private ViewGroup			mRootView;
	private WindowManager		mWindowManager;
	private ImageView			mArrowUp;
	private ImageView			mArrowDown;
	private FrameLayout			mContent;


	Bubble(Context context) {
		this.mContext = context;
		initUI();
	}


	private void initUI() {
		mWindow = new PopupWindow(mContext);
		mWindow.setTouchInterceptor(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
					mWindow.dismiss();
					return true;
				}
				return false;
			}
		});
		mWindowManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mRootView = (ViewGroup) inflater.inflate(R.layout.bubble, null);
		mContent = (FrameLayout) mRootView.findViewById(R.id.content);
		mArrowDown = (ImageView) mRootView.findViewById(R.id.arrow_down);
		mArrowUp = (ImageView) mRootView.findViewById(R.id.arrow_up);
		ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
				ViewGroup.LayoutParams.WRAP_CONTENT);
		mRootView.setLayoutParams(layoutParams);
		mWindow.setContentView(mRootView);
	}


	private void setupWindow() {
		mWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		mWindow.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
		mWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
		mWindow.setTouchable(true);
		mWindow.setFocusable(true);
		mWindow.setOutsideTouchable(true);
		mWindow.setContentView(mRootView);

		View content = getContent();
		mContent.removeAllViews();
		mContent.addView(content);
	}


	public void show(View anchor) {
		setupWindow();
		int[] location = new int[2];
		anchor.getLocationOnScreen(location);

		Rect anchorRect = new Rect(location[0], location[1], location[0] + anchor.getWidth(), location[1]
				+ anchor.getHeight());

		mRootView.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		int popupWidth = mRootView.getMeasuredWidth();
		int popupHeight = mRootView.getMeasuredHeight();
		int screenWidth;
		int screenHeight;
		int anchorHCenter = anchorRect.centerX();

		if (Build.VERSION.SDK_INT >= Constants.HONEYCOMB_MR2) {
			Point screenSize = new Point();
			mWindowManager.getDefaultDisplay().getSize(screenSize);
			screenWidth = screenSize.x;
			screenHeight = screenSize.y;
		} else {
			//noinspection deprecation
			screenWidth = mWindowManager.getDefaultDisplay().getWidth();
			//noinspection deprecation
			screenHeight = mWindowManager.getDefaultDisplay().getHeight();
		}

		// anchorRect.centerX() is the horizontal center of the view to which this popup will be anchored
		// popupLeft is the left most side of the popup
		int popupLeft;
		int popupTop;

		if (anchorHCenter + popupWidth / 2 > screenWidth) {
			// we're too far right
			popupLeft = screenWidth - popupWidth - RIGHT_MARGIN;
		} else {
			popupLeft = anchorHCenter - popupWidth / 2;
		}

		if (popupLeft < 0) {
			// we're too far left
			popupLeft = LEFT_MARGIN;
		}

		boolean arrowOnTop;

		if (anchorRect.bottom + popupHeight > screenHeight) {
			// we're too far down; let's move on top of the anchor
			popupTop = anchorRect.top - popupHeight;
			arrowOnTop = false;
		} else {
			popupTop = anchorRect.bottom;
			arrowOnTop = true;
		}

		View arrowToShow = arrowOnTop ? mArrowUp : mArrowDown;

		int arrowLeftOffset = anchorHCenter - popupLeft - arrowToShow.getMeasuredWidth() / 2;

		if (arrowLeftOffset < 0) {
			arrowLeftOffset = 0;
		} else if (arrowLeftOffset + arrowToShow.getWidth() > screenWidth) {
			arrowLeftOffset -= arrowLeftOffset + arrowToShow.getMeasuredWidth() - screenWidth;
		}

		mWindow.showAtLocation(anchor, Gravity.NO_GRAVITY, popupLeft, popupTop);
		showArrow(arrowOnTop ? R.id.arrow_up : R.id.arrow_down, arrowLeftOffset);
	}


	/**
	 * Show arrow
	 *
	 * @param arrowToShow
	 *            arrow type resource id
	 * @param arrowLeftOffset
	 *            distance of the left side of the arrow from left side of the popup
	 */
	private void showArrow(int arrowToShow, int arrowLeftOffset) {
		View showArrow = (arrowToShow == R.id.arrow_up) ? mArrowUp : mArrowDown;
		showArrow.setVisibility(View.VISIBLE);
		View hideArrow = (arrowToShow == R.id.arrow_up) ? mArrowDown : mArrowUp;
		hideArrow.setVisibility(View.INVISIBLE);

		ViewGroup.MarginLayoutParams param = (ViewGroup.MarginLayoutParams) showArrow.getLayoutParams();
		param.leftMargin = arrowLeftOffset;
	}


	public void dismiss() {
		mWindow.dismiss();
	}


	Context getContext() {
		return mContext;
	}


	protected abstract View getContent();
}
