package com.gb.ctptickets.ui.activity;


import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Telephony;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gb.ctptickets.App;
import com.gb.ctptickets.AppSettings;
import com.gb.ctptickets.Constants;
import com.gb.ctptickets.R;
import com.gb.ctptickets.UserSettings;
import com.gb.ctptickets.provider.carrier.NetworkOperatorProvider;
import com.gb.ctptickets.provider.sms.ActiveTicketDiscoverer;
import com.gb.ctptickets.provider.sms.model.TicketSmsInfo;
import com.gb.ctptickets.provider.ticket.TicketHandlerFactory;
import com.gb.ctptickets.provider.ticket.TicketHistory;
import com.gb.ctptickets.provider.ticket.TicketRequestStatusRegistry;
import com.gb.ctptickets.provider.timetable.TimeTableAsyncTask;
import com.gb.ctptickets.receiver.SmsReceivedBroadcastReceiver;
import com.gb.ctptickets.ui.dialog.AddCustomBusLineDialog;
import com.gb.ctptickets.ui.dialog.BusLineSelectorDialogActivity;
import com.gb.ctptickets.ui.dialog.BuyTicketConfirmationDialogActivity;
import com.gb.ctptickets.ui.dialog.InvalidNetworkOperatorDialogActivity;
import com.gb.ctptickets.ui.view.Bubble;
import com.gb.ctptickets.ui.view.TextBubble;
import com.gb.ctptickets.ui.view.TicketLayout;
import com.gb.ctptickets.ui.view.TicketLayoutAdd;
import com.gb.ctptickets.ui.view.TicketLayoutQuick;
import com.gb.ctptickets.util.Animator;
import com.gb.ctptickets.util.GoogleAnalyticsHelper;
import com.gb.ctptickets.util.HumanTime;
import com.google.android.gms.analytics.GoogleAnalytics;

import net.i2p.android.ext.floatingactionbutton.AddFloatingActionButton;
import net.i2p.android.ext.floatingactionbutton.FloatingActionButton;
import net.i2p.android.ext.floatingactionbutton.FloatingActionsMenu;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener,
        SmsReceivedBroadcastReceiver.SmsReceivedListener {

    private MainActivityTicketManager ticketLayoutManager;

    private SmsReceivedBroadcastReceiver smsReceiver;

    private Bubble currentBubble;

    private ActionMode contextualActionMode;
    private ContextualActionModeCallback contextualActionModeCallback;

    private final String TITLE_CUSTOM = App.getContext().getString(
            R.string.header_custom);
    private final String TITLE_METRO_URBAN = App.getContext().getString(
            R.string.header_metro_urban);
    private final String TITLE_URBAN = App.getContext().getString(
            R.string.header_urban);

    private TicketLayoutAdd ticketLayoutAdd;
    private TicketLayoutQuick ticketLayoutQuick;

    private FloatingActionsMenu floatingTicketsMenu;

    private final View.OnClickListener statusIconClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            TicketLayout ticketLayout = (TicketLayout) v.getParent().getParent();

            if (!TextUtils.isEmpty(ticketLayout.getComment())) {
                if (currentBubble != null) {
                    currentBubble.dismiss();
                }
                currentBubble = new TextBubble(MainActivity.this, ticketLayout.getComment());
                currentBubble.show(v);
            }
        }
    };

    private final View.OnLongClickListener longClickListener = new View.OnLongClickListener() {

        @Override
        public boolean onLongClick(View v) {
            if (contextualActionMode != null) {
                return false;
            }

            // start the CAB using the ActionMode.Callback defined above
            contextualActionModeCallback = new ContextualActionModeCallback();
            contextualActionMode = MainActivity.this.startSupportActionMode(contextualActionModeCallback);
            v.setSelected(true);
            contextualActionModeCallback.addSelection((TicketLayout) v);
            return true;
        }
    };

    private final View.OnClickListener floatingAddTicketClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            floatingTicketsMenu.collapse();
            GoogleAnalyticsHelper.onAddButtonPressed();
            BusLineSelectorDialogActivity.showBusLineSelectorAndBuyDialog(MainActivity.this);
        }
    };

    private final View.OnClickListener addTicketClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            GoogleAnalyticsHelper.onAddButtonPressed();
            BusLineSelectorDialogActivity.showCustomTicketSelectorDialog(MainActivity.this);
        }
    };

    private final View.OnClickListener ticketClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            TicketLayout ticketLayout = (TicketLayout) v;

            if (contextualActionMode == null) {
                BuyTicketConfirmationDialogActivity.show(ticketLayout.getValue());
            } else {
                boolean isSelected = !ticketLayout.isSelected();
                ticketLayout.setSelected(isSelected);

                if (isSelected) {
                    contextualActionModeCallback.addSelection(ticketLayout);
                } else {
                    contextualActionModeCallback.removeSelection(ticketLayout);
                }
            }
        }
    };

    private final View.OnClickListener floatingTicketClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            floatingTicketsMenu.collapse();
            String ticket = (String) v.getTag();
            BuyTicketConfirmationDialogActivity.show(ticket);
        }
    };

    private final View.OnClickListener quickTicketSpinnerClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            BusLineSelectorDialogActivity.showBusLineSelectorDialog(MainActivity.this);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ticketLayoutManager = new MainActivityTicketManager((LinearLayout) findViewById(R.id.ticket_group_container));

        floatingTicketsMenu = (FloatingActionsMenu) findViewById(R.id.floating_tickets_button);

        setupTickets();
        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);

        long lastSyncDate = AppSettings.getLastSyncDate();
        boolean shouldSync = lastSyncDate == 0
                || lastSyncDate < (System.currentTimeMillis() - (long) UserSettings.getTimetableRefreshDays()
                * HumanTime.Time.DAY.valueInMilisecs);
        if (shouldSync) {
//            TimeTableAsyncTask.run();
        }

        checkNetworkOperator();

        //		String message = "Biletul pentru linia 48 a fost activat. Valabil pana la 12:40 in 08/01/2015. Cost total:0.50 EUR+Tva. Cod confirmare:PFQDLXPT.";
        //		try {
        //			TicketSmsInfo validTicket = new TicketSmsParser(message, System.currentTimeMillis()).parse();
        //			onTicketSmsAvailable(new TicketSmsInfo[] {
        //				validTicket
        //			});
        //		} catch (InvalidTicketSmsException e) {
        //			e.printStackTrace();
        //		}
        //
        //		message = "Biletul pentru linia 48 a fost activat. Valabil pana la 12:45 in 08/01/2015. Cost total:0.50 EUR+Tva. Cod confirmare:PFQDLXPT.";
        //		try {
        //			TicketSmsInfo validTicket = new TicketSmsParser(message, System.currentTimeMillis()).parse();
        //			onTicketSmsAvailable(new TicketSmsInfo[] {
        //				validTicket
        //			});
        //		} catch (InvalidTicketSmsException e) {
        //			e.printStackTrace();
        //		}
    }


    @Override
    protected void onStart() {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }


    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
    }


    @Override
    protected void onResume() {
        super.onResume();
//        TicketSmsInfo[] validTickets = ActiveTicketDiscoverer.get().getActiveTickets();
//        onTicketSmsAvailable(validTickets);

        IntentFilter filter = new IntentFilter(Telephony.Sms.Intents.SMS_RECEIVED_ACTION);
        smsReceiver = new SmsReceivedBroadcastReceiver();
        smsReceiver.setSmsReceivedListener(this);
        registerReceiver(smsReceiver, filter);
    }


    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(smsReceiver);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        PreferenceManager.getDefaultSharedPreferences(this).unregisterOnSharedPreferenceChangeListener(this);
    }


    private void checkNetworkOperator() {
        String currentNetworkOperatorName = NetworkOperatorProvider.getNetworkOperator();

        if (!TextUtils.isEmpty(currentNetworkOperatorName)) {
            String lastValidatedNetworkOperator = AppSettings.getLastValidatedNetworkOperator();

            if (lastValidatedNetworkOperator == null || // no warning was yet shown
                    !currentNetworkOperatorName.equals(lastValidatedNetworkOperator)) {// operator changed since last warning
                if (!NetworkOperatorProvider.validateNetworkOperator(currentNetworkOperatorName)) {
                    InvalidNetworkOperatorDialogActivity.show(currentNetworkOperatorName);
                    AppSettings.setLastValidatedNetworkOperator(currentNetworkOperatorName);
                }
            }
        }
    }


    private void setupTickets() {
        ticketLayoutAdd = new TicketLayoutAdd(this, App.getContext().getString(R.string.add_new_line));
        ticketLayoutAdd.setOnClickListener(addTicketClickListener);
        ticketLayoutManager.addTicketLayout(TITLE_CUSTOM, ticketLayoutAdd, this);

        ticketLayoutQuick = new TicketLayoutQuick(this, App.getContext().getString(R.string.custom_bus_line_add_text));
        ticketLayoutQuick.setOnClickListener(ticketClickListener);
        ticketLayoutQuick.setSpinnerOnClickListener(quickTicketSpinnerClickListener);
        ticketLayoutQuick.setStatusIconClickListener(statusIconClickListener);
        ticketLayoutManager.addTicketLayout(TITLE_CUSTOM, ticketLayoutQuick, MainActivity.this);

        removeFloatingTicketButtons();
        addFloatingAddTicketButton();

        String[] floatingTickets = TicketHistory.getTopTickets(3);

        for (String ticket : floatingTickets) {
            addCustomFloatingTicket(ticket);
        }

        String[] customTickets = UserSettings.getCustomTickets();

        for (String ticket : customTickets) {
            addCustomTicket(ticket);
        }

        int[] defaultTicketIds = Constants.DEFAULT_METRO_URBAN_TICKETS;

        for (int ticketId : defaultTicketIds) {
            addMetroUrbanTicket(ticketId);
        }

        defaultTicketIds = Constants.DEFAULT_URBAN_TICKETS;

        for (int ticketId : defaultTicketIds) {
            addUrbanTicket(ticketId);
        }

        ticketLayoutManager.setTicketGroupVisibility(TITLE_METRO_URBAN, !UserSettings.getHideDefaultTickets());
        ticketLayoutManager.setTicketGroupVisibility(TITLE_URBAN, !UserSettings.getHideDefaultTickets());
    }


    private TicketLayout addCustomTicket(String ticket) {
        TicketLayout ticketLayout = new TicketLayout(this, ticket, getString(R.string.line_name, ticket), null);
        ticketLayout.setOnClickListener(ticketClickListener);
        ticketLayout.setOnLongClickListener(longClickListener);
        ticketLayout.setStatusIconClickListener(statusIconClickListener);
        ticketLayoutManager.addTicketLayout(App.getContext().getString(R.string.header_custom), ticketLayout, this);
//        addCustomFloatingTicket(ticket);
        return ticketLayout;
    }


    private void addMetroUrbanTicket(int ticketId) {
        TicketLayout ticketLayout = new TicketLayout(this, App.getContext().getString(ticketId));
        ticketLayout.setOnClickListener(ticketClickListener);
        ticketLayout.setStatusIconClickListener(statusIconClickListener);
        ticketLayoutManager.addTicketLayout(TITLE_METRO_URBAN, ticketLayout, this);
    }


    private void addUrbanTicket(int ticketId) {
        TicketLayout ticketLayout = new TicketLayout(this, App.getContext().getString(ticketId));
        ticketLayout.setOnClickListener(ticketClickListener);
        ticketLayout.setStatusIconClickListener(statusIconClickListener);
        ticketLayoutManager.addTicketLayout(TITLE_URBAN, ticketLayout, this);
    }


    private void addCustomFloatingTicket(String ticket) {
        FloatingActionButton ticketActionButton = new FloatingActionButton(getBaseContext());
        ticketActionButton.setTag(ticket);
        ticketActionButton.setOnClickListener(floatingTicketClickListener);
        floatingTicketsMenu.addButton(ticketActionButton);

        TextView textView = (TextView) getLayoutInflater().inflate(R.layout.floating_ticket_label, null);
        textView.setText(ticket);
        textView.setDrawingCacheEnabled(true);
        int fabSizeNormal = (int) (getResources().getDimensionPixelSize(R.dimen.fab_size_normal) * 0.9);
        textView.measure(View.MeasureSpec.makeMeasureSpec(fabSizeNormal, View.MeasureSpec.EXACTLY),
                View.MeasureSpec.makeMeasureSpec(fabSizeNormal, View.MeasureSpec.EXACTLY));
        textView.layout(0, 0, textView.getMeasuredWidth(), textView.getMeasuredHeight());
        textView.buildDrawingCache(true);
        Bitmap bitmap = Bitmap.createBitmap(textView.getDrawingCache());
        textView.setDrawingCacheEnabled(false);
        BitmapDrawable drawable = new BitmapDrawable(getResources(), bitmap);
        ticketActionButton.setIconDrawable(drawable);
    }


    private void removeFloatingTicketButtons() {
        List<FloatingActionButton> toRemove = new ArrayList<>();

        for (int i = 0; i < floatingTicketsMenu.getChildCount() - 1; i++) {
            if (floatingTicketsMenu.getChildAt(i) instanceof FloatingActionButton) {
                toRemove.add((FloatingActionButton) floatingTicketsMenu.getChildAt(i));
            }
        }
        for (FloatingActionButton button : toRemove) {
            floatingTicketsMenu.removeButton(button);
        }
    }


    private void addFloatingAddTicketButton() {
        AddFloatingActionButton addNewTicket = new AddFloatingActionButton(getBaseContext());
        addNewTicket.setColorNormal(getResources().getColor(R.color.ticket_yellow));
        addNewTicket.setColorPressed(getResources().getColor(R.color.ticket_yellow_pressed));
        addNewTicket.setOnClickListener(floatingAddTicketClickListener);
        floatingTicketsMenu.addButton(addNewTicket);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                break;
            default:
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        if (contextualActionMode != null) {
            contextualActionMode.finish();
        } else if (floatingTicketsMenu.isExpanded()) {
            floatingTicketsMenu.collapse();
        } else {
            super.onBackPressed();
        }
    }


    @Override
    protected void onActivityResult(final int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case BusLineSelectorDialogActivity.REQUEST_SELECT_CUSTOM_TICKET:
                case BusLineSelectorDialogActivity.REQUEST_SELECT_QUICK_TICKET:
                case BusLineSelectorDialogActivity.REQUEST_BUY_TICKET:
                    String line = data.getStringExtra(BusLineSelectorDialogActivity.EXTRA_BUS_LINE);

                    if (line.equals(getString(R.string.new_))) {
                        // if the "new..." option was selected:
                        AddCustomBusLineDialog.show(this,
                                new AddCustomBusLineDialog.OnCustomBusLineAddedCallback() {

                                    @Override
                                    public void onCustomBusLineEntered(String line) {
                                        if (!UserSettings.addCustomBusLine(line)) {
                                            Toast.makeText(App.getContext(), R.string.line_already_exists,
                                                    Toast.LENGTH_SHORT).show();
                                        }

                                        switch (requestCode) {
                                            case BusLineSelectorDialogActivity.REQUEST_SELECT_CUSTOM_TICKET:
                                                addNewCustomTicket(line);
                                                break;
                                            case BusLineSelectorDialogActivity.REQUEST_SELECT_QUICK_TICKET:
                                                UserSettings.setQuickBusLineSelection(line);
                                                break;
                                            case BusLineSelectorDialogActivity.REQUEST_BUY_TICKET:
                                                addNewCustomTicket(line);
                                                BuyTicketConfirmationDialogActivity.show(line);
                                                break;
                                        }
                                    }
                                });
                    } else {
                        switch (requestCode) {
                            case BusLineSelectorDialogActivity.REQUEST_SELECT_CUSTOM_TICKET:
                                addNewCustomTicket(line);
                                break;
                            case BusLineSelectorDialogActivity.REQUEST_SELECT_QUICK_TICKET:
                                UserSettings.setQuickBusLineSelection(line);
                                break;
                            case BusLineSelectorDialogActivity.REQUEST_BUY_TICKET:
                                addNewCustomTicket(line);
                                BuyTicketConfirmationDialogActivity.show(line);
                                break;
                        }
                    }
                    break;
            }
        }
    }


    /**
     * Add a new custom ticket (if the specified line isn't already added) to the screen and wiggle it
     */
    private void addNewCustomTicket(String line) {
        if (!UserSettings.addCustomTicket(line)) {
            Toast.makeText(App.getContext(), R.string.duplicat_ticket_error, Toast.LENGTH_SHORT).show();
        } else {
            GoogleAnalyticsHelper.onCustomTicketAdded(line);
            TicketLayout ticketLayout = addCustomTicket(line);
            new Animator(ticketLayout).shake();
        }
    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(getString(R.string.pref_hide_default_tickets))) {
            ticketLayoutManager.setTicketGroupVisibility(TITLE_METRO_URBAN, !UserSettings.getHideDefaultTickets());
            ticketLayoutManager.setTicketGroupVisibility(TITLE_URBAN, !UserSettings.getHideDefaultTickets());
            GoogleAnalyticsHelper.onSettingChanged(key, Boolean.toString(sharedPreferences.getBoolean(key, false)));
        }
    }


    @Override
    /**
     * This is invoked on every onResume with all the tickets in the phone's sms inbox, that haven't yet expired.
     * Make sure the implementation can handle repeated invocations with the same parameters.
     */
    public void onTicketSmsAvailable(TicketSmsInfo[] tickets) {
        for (TicketSmsInfo ticket : tickets) {
            TicketRequestStatusRegistry registry = TicketHandlerFactory.getRequestStatusRegistry(ticket.getTicket());
            registry.processIncomingTicket(ticket.getTicket(), ticket.getValidUntil().getTime(), ticket.getCost(),
                    ticket.getConfirmationCode());
        }
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (currentBubble != null) {
            currentBubble.dismiss();
        }
    }

    private class ContextualActionModeCallback implements ActionMode.Callback {

        private final List<TicketLayout> selection;


        private ContextualActionModeCallback() {
            selection = new ArrayList<>();
        }


        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.main_contextual, menu);
            ticketLayoutAdd.setVisibility(View.GONE);
            ticketLayoutQuick.setVisibility(View.GONE);
            ticketLayoutManager.setTicketGroupVisibility(TITLE_METRO_URBAN, false);
            ticketLayoutManager.setTicketGroupVisibility(TITLE_URBAN, false);
            return true;
        }


        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }


        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_delete:
                    for (TicketLayout ticketLayout : selection) {
                        ticketLayoutManager.removeCustomTicketFromTable(ticketLayout, new Runnable() {

                            @Override
                            public void run() {
                                ticketLayoutManager.clearTickets();
                                setupTickets();
                            }
                        });
                    }
                    mode.finish();
                    return true;
                default:
                    return false;
            }
        }


        public void onDestroyActionMode(ActionMode mode) {
            ticketLayoutAdd.setVisibility(View.VISIBLE);
            ticketLayoutQuick.setVisibility(View.VISIBLE);
            ticketLayoutManager.setTicketGroupVisibility(TITLE_METRO_URBAN, !UserSettings.getHideDefaultTickets());
            ticketLayoutManager.setTicketGroupVisibility(TITLE_URBAN, !UserSettings.getHideDefaultTickets());

            for (TicketLayout ticketLayout : selection) {
                ticketLayout.setSelected(false);
            }
            contextualActionMode = null;
        }


        public void addSelection(TicketLayout ticketLayout) {
            if (!selection.contains(ticketLayout)) {
                selection.add(ticketLayout);
            }
            updateUI();
        }


        public void removeSelection(TicketLayout ticketLayout) {
            selection.remove(ticketLayout);
            updateUI();
        }


        private void updateUI() {
            contextualActionMode.setTitle(Integer.toString(selection.size()));

            if (selection.isEmpty()) {
                contextualActionMode.finish();
            }
        }
    }
}
