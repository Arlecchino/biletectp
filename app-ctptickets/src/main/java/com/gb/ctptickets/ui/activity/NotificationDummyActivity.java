package com.gb.ctptickets.ui.activity;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;


/**
 * Used by {@link com.gb.ctptickets.util.NotificationBuilder}. Prevents the app being started again when the user taps
 * on the ticket-late notification
 * 
 * Created by gbiro on 10/8/2014.
 */
public class NotificationDummyActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// If this activity is the root activity of the task, the app is not running
		if (isTaskRoot()) {
			// Start the app if not running
			Intent startAppIntent = new Intent(getApplicationContext(), MainActivity.class);
			startAppIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(startAppIntent);
		}
		finish();
	}
}
