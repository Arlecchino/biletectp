package com.gb.ctptickets.ui.util;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.text.TextUtils;


/**
 * Utilities for dialogs
 * 
 * Created by gbiro on 10/1/2014.
 */
public class DialogUtils {

	public static ProgressDialog getProgressDialog(Context context, @SuppressWarnings("SameParameterValue") String title, String message) {
		final ProgressDialog dialog = new ProgressDialog(context);
		if (!TextUtils.isEmpty(title)) {
			dialog.setTitle(title);
		}
		if (!TextUtils.isEmpty(message)) {
			dialog.setMessage(message);
		}
		dialog.setIndeterminate(true);
		dialog.setCancelable(true);
		return dialog;
	}
}
