package com.gb.ctptickets.ui.dialog;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.CompoundButton;

import com.gb.ctptickets.App;
import com.gb.ctptickets.R;
import com.gb.ctptickets.UserSettings;
import com.gb.ctptickets.ui.activity.DialogActivity;


/**
 * Shown where there are no pdf viewers installed on the device. Tries to take the user to google play.
 *
 * Created by gbiro on 11/13/2014.
 */
public class NoMatchingActivityDialogActivity extends DialogActivity {

	private static final String	EXTRA_FILENAME	= "filename";


	public static void show(String filename) {
		Intent i = new Intent(App.getContext(), NoMatchingActivityDialogActivity.class);
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		i.putExtra(EXTRA_FILENAME, filename);
		App.getContext().startActivity(i);
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		NoMatchingActivityDialog dialog = new NoMatchingActivityDialog();
		dialog.setArguments(getIntent().getExtras());
		dialog.show(getSupportFragmentManager(), null);
	}

	public static class NoMatchingActivityDialog extends DialogFragment implements
			CompoundButton.OnCheckedChangeListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

			String filename = getArguments().getString(EXTRA_FILENAME);

			alertDialogBuilder.setMessage(getString(R.string.no_matching_activity, filename));
			alertDialogBuilder.setNegativeButton(android.R.string.cancel, null);
			alertDialogBuilder.setPositiveButton(R.string.google_play, new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					Intent i = new Intent(Intent.ACTION_VIEW);
					i.setData(Uri.parse("market://search?q=pdf viewer&c=apps"));
					startActivity(i);
				}
			});
			return alertDialogBuilder.create();
		}


		@Override
		public void onDetach() {
			super.onDetach();
			getActivity().finish();
		}


		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
			UserSettings.setTicketLateWarningDialogNewerShowAgain(isChecked);
		}
	}
}
