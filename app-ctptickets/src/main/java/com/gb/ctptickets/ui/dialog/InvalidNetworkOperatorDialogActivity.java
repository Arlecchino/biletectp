package com.gb.ctptickets.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.os.Bundle;

import com.gb.ctptickets.App;
import com.gb.ctptickets.R;
import com.gb.ctptickets.ui.activity.DialogActivity;

/**
 * Created by gbiro on 5/27/2015.
 */
public class InvalidNetworkOperatorDialogActivity extends DialogActivity {

    public static void show(String currentNetworkOperatorName) {
        App.getContext().startActivity(prepareIntent(currentNetworkOperatorName));
    }

    @SuppressWarnings("WeakerAccess")
    public static Intent prepareIntent(String currentNetworkOperatorName) {
        Intent i = new Intent(App.getContext(), InvalidNetworkOperatorDialogActivity.class);
        i.putExtra(InvalidNetworkOperatorDialog.EXTRA_NETWORK_OPERATOR, currentNetworkOperatorName);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return i;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        InvalidNetworkOperatorDialog dialog = new InvalidNetworkOperatorDialog();
        dialog.setArguments(getIntent().getExtras());
        dialog.show(getSupportFragmentManager(), null);
    }

    public static class InvalidNetworkOperatorDialog extends DialogFragment {

        static final String EXTRA_NETWORK_OPERATOR = "EXTRA_NETWORK_OPERATOR";

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            String networkOperator = getArguments().getString(EXTRA_NETWORK_OPERATOR);

            return new AlertDialog.Builder(getActivity()).setMessage(getString(R.string.invalid_network_operator_body, networkOperator))
                    .setPositiveButton(android.R.string.ok, null).create();
        }
    }
}
