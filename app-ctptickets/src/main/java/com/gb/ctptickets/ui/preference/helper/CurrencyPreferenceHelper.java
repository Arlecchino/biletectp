package com.gb.ctptickets.ui.preference.helper;


import android.app.ProgressDialog;
import android.preference.ListPreference;
import android.preference.Preference;
import android.widget.Toast;

import com.gb.ctptickets.App;
import com.gb.ctptickets.Constants;
import com.gb.ctptickets.R;
import com.gb.ctptickets.UserSettings;
import com.gb.ctptickets.provider.currency.CurrencyProvider;
import com.gb.ctptickets.provider.currency.eventbus.CurrencyFetchingTaskEvent;
import com.gb.ctptickets.provider.currency.model.Currency;
import com.gb.ctptickets.provider.currency.model.Rate;
import com.gb.ctptickets.provider.ticket.model.TicketRate;
import com.gb.ctptickets.ui.eventbus.CurrencyRateChangedEvent;
import com.gb.ctptickets.ui.util.DialogUtils;
import com.gb.ctptickets.util.GoogleAnalyticsHelper;

import de.greenrobot.event.EventBus;


/**
 * Because PreferenceActivity is not backward-compatible, I needed to create two Activities (one with PreferenceActivity
 * and another with ActionBarActivity and PreferenceFragment) and I put in this class the common operations related to
 * the currency preference
 *
 * Created by GABOR on 12/15/2014.
 */
public class CurrencyPreferenceHelper extends SimpleListPreferenceHelper {

	private static ProgressDialog	currencyLoadingProgressDialog;

	private Currency				currency;


	public CurrencyPreferenceHelper(ListPreference preference) {
		super(preference);
	}


	@Override
	public void setup() {
		preference.setEntries(Currency.toStringArray());
		preference.setEntryValues(Currency.toNameArray());
		int index = preference.findIndexOfValue(preference.getValue());
		preference.setSummary(index >= 0 ? preference.getEntries()[index] : null);
		super.setup();
	}


	@Override
	public boolean onPreferenceChange(Preference preference, Object newValue) {
		if (preference.getKey().equals(App.getContext().getString(R.string.pref_currency))
				&& !newValue.equals(((ListPreference) preference).getValue())) {
			Currency selectedCurrency = Currency.valueOf(newValue.toString());
			setCurrency(selectedCurrency);
		}
		return false;
	}


	private void setCurrency(final Currency currency) {
		this.currency = currency;

		if (currency != Constants.BASE_CURRENCY) {
			CurrencyProvider.get().fetchRate(Constants.BASE_CURRENCY, currency);
		} else {
			UserSettings.clearCurrentCurrencyRate();
			setCurrencySelectionPreference(currency, preference);
		}
		GoogleAnalyticsHelper.onSettingChanged(preference.getKey(), currency.toString());
	}


	@SuppressWarnings("unused")
	public void onEventMainThread(CurrencyFetchingTaskEvent event) {
		switch (event.getType()) {
		case START:
			if (currencyLoadingProgressDialog != null) {
				currencyLoadingProgressDialog.dismiss();
			}
			currencyLoadingProgressDialog = DialogUtils.getProgressDialog(preference.getContext(), null, App
					.getContext().getString(R.string.loading_currency));
			currencyLoadingProgressDialog.show();
			break;
		case SUCCESS:
			onCurrencyRateReceived(event.getResult());
			break;
		case FAIL:
			onCurrencyRateError(event.getErrorMessage());
			break;
		case FINISH:
			if (currencyLoadingProgressDialog != null) {
				currencyLoadingProgressDialog.dismiss();
				currencyLoadingProgressDialog = null;
			}
			break;
		}
	}


	private void onCurrencyRateReceived(Rate rate) {
		TicketRate ticketRate = new TicketRate(Constants.BASE_CURRENCY, currency, rate);
		UserSettings.setCurrentCurrencyRate(ticketRate);
		setCurrencySelectionPreference(currency, preference);
		Toast.makeText(App.getContext(), rate.name + ": " + rate.rate, Toast.LENGTH_LONG).show();
	}


	private void onCurrencyRateError(String cause) {
		Toast.makeText(App.getContext(), App.getContext().getString(R.string.error_fetching_currency, cause),
				Toast.LENGTH_LONG).show();
		GoogleAnalyticsHelper.onSettingChanged(preference.getKey(), cause);
	}


	private void setCurrencySelectionPreference(Currency currency, ListPreference currencyPreference) {
		currencyPreference.setValue(currency.name());
		currencyPreference.setSummary(currency.toString());
		EventBus.getDefault().post(new CurrencyRateChangedEvent());
	}
}
