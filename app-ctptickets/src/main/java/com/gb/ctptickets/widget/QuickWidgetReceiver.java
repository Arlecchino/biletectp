package com.gb.ctptickets.widget;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.gb.ctptickets.Constants;
import com.gb.ctptickets.provider.ticket.TicketHandlerFactory;
import com.gb.ctptickets.provider.ticket.TicketRequestStatusRegistry;
import com.gb.ctptickets.ui.dialog.BuyTicketConfirmationDialogActivity;
import com.gb.ctptickets.util.GoogleAnalyticsHelper;
import com.gb.ctptickets.util.IntentUtils;


/**
 * Receiver that handles the user tapping on a quick ticket widget
 * 
 * Created by GABOR on 2014-aug.-03.
 */
public class QuickWidgetReceiver extends BroadcastReceiver {

	public static final String	WIDGET_SEND_SMS	= "com.gb.ctptickets.intent.action.WIDGET_SEND_SMS";
	public static final String	EXTRA_TICKET_ID	= "com.gb.ctptickets.QuickWidgetReceiver.EXTRA_TICKET_ID";


	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent.getAction().equals(WIDGET_SEND_SMS)) {
			String ticket = intent.getStringExtra(EXTRA_TICKET_ID);
			GoogleAnalyticsHelper.onWidgetClicked(ticket);

			TicketRequestStatusRegistry registry = TicketHandlerFactory.getRequestStatusRegistry(ticket);
			long id = registry.getIdForTicketWithClosestExpiration();

			if (id >= 0 && registry.isValid(id)) {
				IntentUtils.startSmsApplication(Constants.TICKET_BUY_PHONE_NUMBER);
			} else {
				BuyTicketConfirmationDialogActivity.show(ticket);
			}
		}
	}
}
