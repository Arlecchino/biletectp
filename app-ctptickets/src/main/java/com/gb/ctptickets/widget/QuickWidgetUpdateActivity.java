package com.gb.ctptickets.widget;


import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.os.Bundle;

import com.gb.ctptickets.AppSettings;
import com.gb.ctptickets.ui.dialog.BusLineSelectorDialog;
import com.gb.ctptickets.ui.dialog.BusLineSelectorDialogActivity;
import com.gb.ctptickets.util.GoogleAnalyticsHelper;


public class QuickWidgetUpdateActivity extends BusLineSelectorDialogActivity {

	private int	appWidgetId;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle extras = getIntent().getExtras();

		if (extras != null) {
			appWidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
		}

		if (appWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
			finish();
		}
	}


	protected void initUI() {
		BusLineSelectorDialog.prepare(false, false).show(getSupportFragmentManager(), null);
	}


	@Override
	public void onBusLineSelected(String line) {
		AppSettings.setAppWidgetTicket(appWidgetId, line);
		WidgetItem widgetItem = QuickWidgetProvider.getWidgetItemForTicket(line);
		QuickWidgetProvider.updateWidget(appWidgetId, widgetItem);
		Intent resultValue = new Intent();
		resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
		setResult(RESULT_OK, resultValue);
		finish();
		GoogleAnalyticsHelper.onWidgetCreated(line);
	}
}
