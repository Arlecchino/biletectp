package com.gb.ctptickets.widget;


class WidgetItem {

	private final String	id;
	private final String	line;


	public WidgetItem(String id, String line) {
		this.id = id;
		this.line = line;
	}


	public String getId() {
		return id;
	}


	public String getLine() {
		return line;
	}
}
