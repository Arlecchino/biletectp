package com.gb.ctptickets.widget;


import java.lang.ref.WeakReference;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RemoteViews;

import com.gb.ctptickets.App;
import com.gb.ctptickets.AppSettings;
import com.gb.ctptickets.R;
import com.gb.ctptickets.provider.ticket.TicketHandlerFactory;
import com.gb.ctptickets.provider.ticket.TicketRequestStatusRegistry;
import com.gb.ctptickets.ui.view.TicketLayout;
import com.gb.ctptickets.util.GoogleAnalyticsHelper;
import com.gb.ctptickets.util.HumanTime;


/**
 * Provider class for the quick-ticket widget
 * 
 * Created by GABOR on 2014-aug.-03.
 */
public class QuickWidgetProvider extends AppWidgetProvider {

	private static final Map<String, WeakReference<WidgetItem>>	widgetItemCache	= new HashMap<String, WeakReference<WidgetItem>>();


	public static void updateWidget(int appWidgetId, WidgetItem widgetItem) {
		Context context = App.getContext();
		AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
		RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_ticket_quick);
		remoteViews.setOnClickPendingIntent(R.id.widget_item,
				buildSendSMSPendingIntent(appWidgetId, context, widgetItem.getId()));

		remoteViews.setTextViewText(R.id.ticket_line, widgetItem.getLine());

		TicketRequestStatusRegistry registry = TicketHandlerFactory.getRequestStatusRegistry(widgetItem.getLine());

		TicketLayout.UIState state = TicketLayout.getStateForShortestExpiryTicket(registry);

		if (state == TicketLayout.UIState.UNKNOWN || state == TicketLayout.UIState.RECEIVED) {
			remoteViews.setViewVisibility(R.id.ticket_request_status, View.GONE);
		} else {
			remoteViews.setViewVisibility(R.id.ticket_request_status, View.VISIBLE);
			remoteViews.setImageViewResource(R.id.ticket_request_status, state.getImageRes());
		}

		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		Intent updateIntent = buildUpdateIntent(new int[] {
			appWidgetId
		});
		PendingIntent broadcast = PendingIntent.getBroadcast(context, widgetItem.getId().hashCode(), updateIntent,
				PendingIntent.FLAG_CANCEL_CURRENT);

		if (state == TicketLayout.UIState.RECEIVED) {
            long id = registry.getIdForTicketWithClosestExpiration();
			long delta = registry.getExpiryDate(id) - System.currentTimeMillis();

			if (delta <= HumanTime.Time.SECOND.valueInMilisecs) {
				delta = 0;
			}

			if (delta > 0) {
				remoteViews.setViewVisibility(R.id.ticket_countdown, View.VISIBLE);
				String deltaStr;

				if (delta > HumanTime.Time.MINUTE.valueInMilisecs) {
					HumanTime ht = new HumanTime(delta);
					deltaStr = ht.setCompact(true).getApproximately(1);
				} else {
					deltaStr = "<1" + App.getContext().getString(R.string.minute_abbr);
				}
				remoteViews.setTextViewText(R.id.ticket_countdown, deltaStr);
				Calendar c = Calendar.getInstance();
				c.set(Calendar.SECOND, 0);
				c.set(Calendar.MILLISECOND, 0);
				c.add(Calendar.MINUTE, 1);
				alarmManager.setRepeating(AlarmManager.RTC, c.getTimeInMillis(), 1000 * 60, broadcast);
			} else {
				remoteViews.setViewVisibility(R.id.ticket_countdown, View.GONE);
				alarmManager.cancel(broadcast);
			}
		} else {
			remoteViews.setViewVisibility(R.id.ticket_countdown, View.GONE);
			alarmManager.cancel(broadcast);
		}

		appWidgetManager.updateAppWidget(appWidgetId, remoteViews);
	}


	private static PendingIntent buildSendSMSPendingIntent(int appWidgetId, Context context, String ticketId) {
		Intent intent = new Intent(context, QuickWidgetReceiver.class);
		intent.setAction(QuickWidgetReceiver.WIDGET_SEND_SMS);
		intent.putExtra(QuickWidgetReceiver.EXTRA_TICKET_ID, ticketId);
		return PendingIntent.getBroadcast(context, appWidgetId, intent, PendingIntent.FLAG_UPDATE_CURRENT);
	}


	public static void updateWidgets(String ticket) {
		int[] ids = AppSettings.getAppWidgetsForTicket(ticket);
		App.getContext().sendBroadcast(buildUpdateIntent(ids));
	}


	private static Intent buildUpdateIntent(int[] ids) {
		Intent updateIntent = new Intent(App.getContext(), QuickWidgetProvider.class);
		updateIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
		updateIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);
		return updateIntent;
	}


	@Override
	public void onDeleted(Context context, int[] appWidgetIds) {
		for (int id : appWidgetIds) {
			GoogleAnalyticsHelper.onWidgetDeleted(AppSettings.getAppWidgetTicket(id));
			AppSettings.removeAppWidgetTicket(id);
		}
	}


	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
		for (int id : appWidgetIds) {
			String ticketId = AppSettings.getAppWidgetTicket(id);

			if (ticketId != null) {
				WidgetItem widgetItem = getWidgetItemForTicket(ticketId);
				updateWidget(id, widgetItem);
			}
		}
	}


	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
		if (AppWidgetManager.ACTION_APPWIDGET_UPDATE.equals(action)) {
			Bundle extras = intent.getExtras();
			if (extras != null) {
				int[] appWidgetIds = extras.getIntArray(AppWidgetManager.EXTRA_APPWIDGET_IDS);
				if (appWidgetIds != null && appWidgetIds.length > 0) {
					this.onUpdate(context, AppWidgetManager.getInstance(context), appWidgetIds);
				}
			}
		}
	}


	static WidgetItem getWidgetItemForTicket(String ticket) {
		Context context = App.getContext();
		WidgetItem widgetItem = widgetItemCache.containsKey(ticket) ? widgetItemCache.get(ticket).get() : null;

		if (widgetItem == null) {
			widgetItem = new WidgetItem(ticket, context.getString(R.string.line, ticket));
			widgetItemCache.put(ticket, new WeakReference<WidgetItem>(widgetItem));
		}
		return widgetItem;
	}
}
