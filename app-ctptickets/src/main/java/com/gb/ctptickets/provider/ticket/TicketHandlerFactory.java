package com.gb.ctptickets.provider.ticket;


import java.util.HashMap;
import java.util.Map;


/**
 * Created by gbiro on 12/10/2014.
 */
public class TicketHandlerFactory {

	private static TicketHandlerFactory	INSTANCE;

	private static class TicketHandler {

		private TicketBuyer					ticketBuyer;
		private TicketRequestStatusRegistry	ticketRequestStatusRegistry;
		private TicketUIProvider			ticketUIProvider;
	}

	private final Map<String, TicketHandler>	ticketHandlerMap;


	private TicketHandlerFactory() {
		ticketHandlerMap = new HashMap<String, TicketHandler>();
	}


	private static TicketHandlerFactory get() {
		if (INSTANCE == null) {
			INSTANCE = new TicketHandlerFactory();
		}
		return INSTANCE;
	}


	private static TicketHandler getHandler(String ticket) {
		TicketHandler handler = get().ticketHandlerMap.get(ticket);

		if (handler == null) {
			handler = new TicketHandler();
			handler.ticketBuyer = new TicketBuyer(ticket);
			handler.ticketRequestStatusRegistry = new DefaultTicketRequestStatusRegistry(ticket);
			handler.ticketUIProvider = new DefaultTicketUIProvider(ticket);
			get().ticketHandlerMap.put(ticket, handler);
		}
		return handler;
	}


	/**
	 * Created a new {@link com.gb.ctptickets.provider.ticket.TicketBuyer} if needed
	 */
	public static TicketBuyer getBuyer(String ticket) {
		return getHandler(ticket).ticketBuyer;
	}


	/**
	 * Creates a new {@link com.gb.ctptickets.provider.ticket.TicketRequestStatusRegistry} if needed
	 */
	public static TicketRequestStatusRegistry getRequestStatusRegistry(String ticket) {
		return getHandler(ticket).ticketRequestStatusRegistry;
	}


	/**
	 * Creates a new {@link com.gb.ctptickets.provider.ticket.TicketUIProvider} if needed
	 */
	public static TicketUIProvider getUIProvider(String ticket) {
		return getHandler(ticket).ticketUIProvider;
	}
}
