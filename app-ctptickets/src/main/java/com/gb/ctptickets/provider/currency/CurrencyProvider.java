package com.gb.ctptickets.provider.currency;


import retrofit.RestAdapter;

import com.gb.ctptickets.provider.currency.eventbus.CurrencyFetchingTaskEvent;
import com.gb.ctptickets.provider.currency.model.Currency;
import com.gb.ctptickets.provider.eventbus.DefaultRetrofitTaskCallback;


/**
 * Interface for fetching currency rates
 * 
 * Created by gbiro on 7/21/2014.
 */
public class CurrencyProvider {

	private static CurrencyProvider	INSTANCE;


	private CurrencyProvider() {
		// hiding constructor
	}


	public static CurrencyProvider get() {
		if (INSTANCE == null) {
			INSTANCE = new CurrencyProvider();
		}
		return INSTANCE;
	}


	public void fetchRate(@SuppressWarnings("SameParameterValue") Currency from, Currency to) {
		DefaultRetrofitTaskCallback callback = new DefaultRetrofitTaskCallback(new CurrencyFetchingTaskEvent());
		callback.sendStart();
		RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint("http://query.yahooapis.com/v1/public")
				.setLogLevel(RestAdapter.LogLevel.FULL).build();
		YahooAPIsService service = restAdapter.create(YahooAPIsService.class);
		service.query("select * from yahoo.finance.xchange where pair = \"" + from.name() + to.name() + "\"", callback);
	}
}
