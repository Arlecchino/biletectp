package com.gb.ctptickets.provider.sms.model;


import java.util.Date;


/**
 * Created by GABOR on 2014-szept.-11.
 */
public class TicketSmsInfo {

	private String	ticket;
	private Date	validUntil;
	private double	cost;
	private String	confirmationCode;


	public String getTicket() {
		return ticket;
	}


	public void setTicket(String ticket) {
		this.ticket = ticket;
	}


	public Date getValidUntil() {
		return validUntil;
	}


	public void setValidUntil(Date validUntil) {
		this.validUntil = validUntil;
	}


	public double getCost() {
		return cost;
	}


	public void setCost(double cost) {
		this.cost = cost;
	}


	public String getConfirmationCode() {
		return confirmationCode;
	}


	public void setConfirmationCode(String confirmationCode) {
		this.confirmationCode = confirmationCode;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		TicketSmsInfo that = (TicketSmsInfo) o;

		if (Double.compare(that.cost, cost) != 0)
			return false;
		if (confirmationCode != null ? !confirmationCode.equals(that.confirmationCode) : that.confirmationCode != null)
			return false;
		if (!ticket.equals(that.ticket))
			return false;
		//noinspection RedundantIfStatement
		if (!validUntil.equals(that.validUntil))
			return false;

		return true;
	}


	@Override
	public int hashCode() {
		int result;
		long temp;
		result = ticket.hashCode();
		result = 31 * result + validUntil.hashCode();
		temp = Double.doubleToLongBits(cost);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		result = 31 * result + (confirmationCode != null ? confirmationCode.hashCode() : 0);
		return result;
	}


	@Override
	public String toString() {
		return "TicketSmsInfo{" + "ticket='" + ticket + '\'' + ", expiryDate=" + validUntil + ", cost=" + cost
				+ ", confirmationCode='" + confirmationCode + '\'' + '}';
	}
}
