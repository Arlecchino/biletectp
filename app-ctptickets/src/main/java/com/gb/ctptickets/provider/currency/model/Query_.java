package com.gb.ctptickets.provider.currency.model;


import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


@SuppressWarnings({"WeakerAccess", "unused"})
public class Query_ {

	@SerializedName("count")
	public int		count;

	@SerializedName("created")
	public String	created;

	@SerializedName("lang")
	public String	lang;

	@SerializedName("results")
	public Results	results;


	public Date getCreated() {
		try {
			return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").parse(created);
		} catch (ParseException e) {
			return null;
		}
	}
}
