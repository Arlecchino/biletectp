package com.gb.ctptickets.provider.currency;


import retrofit.Callback;
import retrofit.http.GET;

import com.gb.ctptickets.provider.currency.model.Query;


/**
 * Created by gbiro on 8/17/2014.
 */
interface YahooAPIsService {

	// http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.xchange%20where%20pair%20=%20%22EURRON%22&format=json&env=store://datatables.org/alltableswithkeys&callback=
	@GET("/yql?format=json&env=store://datatables.org/alltableswithkeys")
	void query(@retrofit.http.Query("q") String query, Callback<Query> callback);
}
