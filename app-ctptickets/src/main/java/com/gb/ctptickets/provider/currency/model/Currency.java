package com.gb.ctptickets.provider.currency.model;

/**
 * Enum with ... many currencies
 *
 * Created by gbiro on 8/17/2014.
 */
public enum Currency {

    EUR("Euro"),
    RON("Romanian New Leu"),
    USD("US Dollar"),
    GBP("British Pound"),
    INR("Indian Rupee"),
    AUD("Australian Dollar"),
    CAD("Canadian Dollar"),
    AED("Emirati Dirham"),
    MYR("Malaysian Ringgit"),
    CHF("Swiss Franc"),
    CNY("Chinese Yuan Renminbi"),
    THB("Thai Baht"),
    SAR("Saudi Arabian Riyal"),
    NZD("New Zealand Dollar"),
    JPY("Japanese Yen"),
    SGD("Singapore Dollar"),
    PHP("Philippine Peso"),
    TRY("Turkish Lira"),
    HKD("Hong Kong Dollar"),
    IDR("Indonesian Rupiah"),
    ZAR("South African Rand"),
    MXN("Mexican Peso"),
    SEK("Swedish Krona"),
    BRL("Brazilian Real"),
    HUF("Hungarian Forint"),
    PKR("Pakistani Rupee"),
    QAR("Qatari Riyal"),
    OMR("Omani Rial"),
    KWD("Kuwaiti Dinar"),
    DKK("Danish Krone"),
    NOK("Norwegian Krone"),
    RUB("Russian Ruble"),
    EGP("Egyptian Pound"),
    KRW("South Korean Won"),
    PLN("Polish Zloty"),
    COP("Colombian Peso"),
    CZK("Czech Koruna"),
    ILS("Israeli Shekel"),
    IQD("Iraqi Dinar"),
    NGN("Nigerian Naira"),
    MAD("Moroccan Dirham"),
    ARS("Argentine Peso"),
    LKR("Sri Lankan Rupee"),
    TWD("Taiwan New Dollar"),
    BDT("Bangladeshi Taka"),
    BHD("Bahraini Dinar"),
    VND("Vietnamese Dong"),
    CLP("Chilean Peso"),
    KES("Kenyan Shilling"),
    TND("Tunisian Dinar"),
    XOF("CFA Franc"),
    JOD("Jordanian Dinar"),
    GHS("Ghanaian Cedi"),
    HRK("Croatian Kuna"),
    BGN("Bulgarian Lev"),
    PEN("Peruvian Nuevo Sol"),
    DZD("Algerian Dinar"),
    NPR("Nepalese Rupee"),
    XAF("Central African CFA Franc BEAC"),
    ISK("Icelandic Krona"),
    UAH("Ukrainian Hryvnia"),
    FJD("Fijian Dollar"),
    DOP("Dominican Peso"),
    XPF("CFP Franc"),
    MUR("Mauritian Rupee"),
    AZN("Azerbaijani New Manat"),
    BAM("Bosnian Convertible Marka"),
    XAU("Gold Ounce"),
    IRR("Iranian Rial"),
    RSD("Serbian Dinar"),
    LTL("Lithuanian Litas"),
    BND("Bruneian Dollar"),
    ETB("Ethiopian Birr"),
    CRC("Costa Rican Colon"),
    VEF("Venezuelan Bolivar"),
    AFN("Afghan Afghani"),
    TZS("Tanzanian Shilling"),
    UGX("Ugandan Shilling"),
    JMD("Jamaican Dollar"),
    GEL("Georgian Lari"),
    ZWD("Zimbabwean Dollar"),
    BWP("Botswana Pula"),
    CUC("Cuban Convertible Peso"),
    ZMW("Zambian Kwacha"),
    MMK("Burmese Kyat"),
    GTQ("Guatemalan Quetzal"),
    XCD("East Caribbean Dollar"),
    LYD("Libyan Dinar"),
    MKD("Macedonian Denar"),
    TTD("Trinidadian Dollar"),
    MZN("Mozambican Metical"),
    ALL("Albanian Lek"),
    BOB("Bolivian Boliviano"),
    KZT("Kazakhstani Tenge"),
    BBD("Barbadian or Bajan Dollar"),
    AOA("Angolan Kwanza"),
    KHR("Cambodian Riel"),
    XAG("Silver Ounce"),
    AMD("Armenian Dram"),
    UYU("Uruguayan Peso"),
    MOP("Macau Pataca"),
    NAD("Namibian Dollar"),
    LBP("Lebanese Pound"),
    LAK("Lao or Laotian Kip"),
    BYR("Belarusian Ruble"),
    MGA("Malagasy Ariary"),
    SYP("Syrian Pound"),
    VUV("Ni-Vanuatu Vatu"),
    PGK("Papua New Guinean Kina"),
    MNT("Mongolian Tughrik"),
    SDG("Sudanese Pound"),
    ANG("Dutch Guilder"),
    MWK("Malawian Kwacha"),
    GMD("Gambian Dalasi"),
    CUP("Cuban Peso"),
    RWF("Rwandan Franc"),
    MVR("Maldivian Rufiyaa"),
    BTN("Bhutanese Ngultrum"),
    SCR("Seychellois Rupee"),
    HNL("Honduran Lempira"),
    KPW("North Korean Won"),
    PYG("Paraguayan Guarani"),
    DJF("Djiboutian Franc"),
    XBT("Bitcoin"),
    YER("Yemeni Rial"),
    CDF("Congolese Franc"),
    WST("Samoan Tala"),
    GYD("Guyanese Dollar"),
    AWG("Aruban or Dutch Guilder"),
    MDL("Moldovan Leu"),
    BZD("Belizean Dollar"),
    HTG("Haitian Gourde"),
    KGS("Kyrgyzstani Som"),
    NIO("Nicaraguan Cordoba"),
    CVE("Cape Verdean Escudo"),
    KYD("Caymanian Dollar"),
    GNF("Guinean Franc"),
    BSD("Bahamian Dollar"),
    BIF("Burundian Franc"),
    SLL("Sierra Leonean Leone"),
    MRO("Mauritanian Ouguiya"),
    TOP("Tongan Pa'anga"),
    BMD("Bermudian Dollar"),
    SBD("Solomon Islander Dollar"),
    UZS("Uzbekistani Som"),
    SOS("Somali Shilling"),
    PAB("Panamanian Balboa"),
    SRD("Surinamese Dollar"),
    XDR("IMF Special Drawing Rights"),
    SZL("Swazi Lilangeni"),
    ERN("Eritrean Nakfa"),
    LRD("Liberian Dollar"),
    TJS("Tajikistani Somoni"),
    TMT("Turkmenistani Manat"),
    GIP("Gibraltar Pound"),
    LSL("Basotho Loti"),
    KMF("Comoran Franc"),
    SVC("Salvadoran Colon"),
    GGP("Guernsey Pound"),
    XPT("Platinum Ounce"),
    STD("Sao Tomean Dobra"),
    IMP("Isle of Man Pound"),
    FKP("Falkland Island Pound"),
    XPD("Palladium Ounce"),
    JEP("Jersey Pound"),
    SHP("Saint Helenian Pound"),
    SPL("Seborgan Luigino"),
    TVD("Tuvaluan Dollar");

	private final String	description;


	Currency(String description) {
		this.description = description;
	}


	@Override
	public String toString() {
		return name() + " - " + description;
	}


	public static String[] toStringArray() {
		Currency[] currencies = values();
		String[] result = new String[currencies.length];

		for (int i = 0; i < currencies.length; i++) {
			result[i] = currencies[i].toString();
		}
		return result;
	}


	public static String[] toNameArray() {
		Currency[] currencies = values();
		String[] result = new String[currencies.length];

		for (int i = 0; i < currencies.length; i++) {
			result[i] = currencies[i].name();
		}
		return result;
	}
}
