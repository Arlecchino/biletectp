package com.gb.ctptickets.provider.timetable;


import java.io.File;
import java.util.Map;

import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;

import com.gb.ctptickets.App;
import com.gb.ctptickets.AppSettings;
import com.gb.ctptickets.Constants;
import com.gb.ctptickets.R;
import com.gb.ctptickets.UserSettings;
import com.gb.ctptickets.util.HumanTime;
import com.gb.ctptickets.util.IntentUtils;


/**
 * Create an instance of this class with a bus-line and use all the operations available for that line.
 * <p/>
 * Created by gbiro on 12/9/2014.
 */
public class TimeTableHandler {

	private final String		busLine;
	private Map<String, String>	pdfUriMap;


	public TimeTableHandler(String busLine) {
		this.busLine = busLine;
	}


	/**
	 * Fetch url from RATUC for the specified <code>busLine</code>, null if the ratuc metadata wasn't synced or the bus
	 * line doesn't exist. Note: this method doesn't involve any syncing, it extract the data from AppSettings.
	 *
	 * @return uri from RATUC, null if the timetable uri has not yet been received from RATUC
	 * @see {@link TimeTableAsyncTask}
	 */
	private Uri getRatucUri() {
		if (pdfUriMap == null) {
			pdfUriMap = AppSettings.getPdfUriMap();

			if (pdfUriMap == null) {
				return null;
			}
		}
		String uri = pdfUriMap.get(busLine);
		return TextUtils.isEmpty(uri) ? null : Uri.parse(uri);
	}


	/**
	 * Generate a unique file-path in the /sdcard/Android/data/com.gb.ctptickets/files folder based on the
	 * <code>uri</code>
	 *
	 * @return file path to where the file can be saved, null if the timetable uri has not yet been received from RATUC
	 */
	public File getFile(Uri uri) {
		return new File(App.getContext().getExternalFilesDir("") + "/" + sanitizeFileName(uri.getLastPathSegment()));
	}


	/**
	 * @return the name of the pdf file as it is saved, or null if it doesn't exists
	 */
	public String getFilename() {
		Uri uri = getRatucUri();

		if (uri == null) {
			// metadata was not yet synced
			return null;
		}
		return sanitizeFileName(uri.getLastPathSegment());
	}


	/**
	 * Replaces everything that's not letter, digit, underscore, dash and non-leading period with underscore.
	 */
	private static String sanitizeFileName(String filename) {
		filename = filename.replaceAll("[^a-zA-Z0-9_\\-\\.]", "_");
		// replacing leading periods
		filename = filename.replaceAll("^[\\.]+", "_");
		return filename;
	}


	public boolean isDownloaded() {
		Uri uri = getRatucUri();
		return uri != null && getFile(uri).exists();
	}


	@SuppressWarnings("BooleanMethodIsAlwaysInverted")
	public boolean isExpired() {
		Uri uri = getRatucUri();

		if (uri == null) {
			// metadata was not yet synced
			return false;
		}
		File file = getFile(uri);
		long maxAge = System.currentTimeMillis() - UserSettings.getTimetableRefreshDays()
				* HumanTime.Time.DAY.valueInMilisecs;
		return file.exists() && file.lastModified() < maxAge;
	}


	public static int deleteAllFromDevice() {
		return clearFolder(App.getContext().getExternalFilesDir(""), 0);
	}


	private static int clearFolder(File fileOrDirectory, int index) {
		if (fileOrDirectory.exists() && fileOrDirectory.isDirectory()) {
			for (File file : fileOrDirectory.listFiles()) {
				clearFolder(file, index);
				file.delete();
				index++;
			}
		}
		return index;
	}


	public void deleteFromDevice() {
		Uri uri = getRatucUri();

		if (uri == null) {
			// metadata was not yet synced
			return;
		}
		getFile(uri).delete();
	}


	/**
	 * @throws android.content.ActivityNotFoundException
	 *             if no application capable of opening the file is installed on the device
	 * @throws TimeTableUnavailableException
	 *             if the timetables were not synced from RATUC or RATUC does not not provide a timetable for this
	 *             bus-line (which most probably means the bus-line itself does not exist at RATUC)
	 * @see {@link TimeTableAsyncTask}
	 */
	public void open() throws ActivityNotFoundException, TimeTableUnavailableException {
		Uri uri = getRatucUri();

		if (uri == null) {
			// metadata was not yet synced or invalid bus-line
			throw new TimeTableUnavailableException();
		}
		File file = getFile(uri);
		IntentUtils.openFile(file);
	}


	public void download() throws TimeTableUnavailableException {
		Uri uri = getRatucUri();

		if (uri == null) {
			throw new TimeTableUnavailableException();
		}
		DownloadManager.Request request = new DownloadManager.Request(uri);
		request.setTitle(App.getContext().getString(R.string.download_label, busLine));
		request.setVisibleInDownloadsUi(true);

		if (Build.VERSION.SDK_INT >= Constants.HONEYCOMB) {
			request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE);
		}
		request.setDestinationInExternalFilesDir(App.getContext(), "", uri.getLastPathSegment());

		// The result of the download is handled in DownloadStateBroadcastReceiver
		DownloadManager downloadManager = (android.app.DownloadManager) App.getContext().getSystemService(
				Context.DOWNLOAD_SERVICE);
		downloadManager.enqueue(request);
	}
}
