package com.gb.ctptickets.provider.calendar;

import android.annotation.TargetApi;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.BaseColumns;
import android.provider.CalendarContract;
import android.text.format.DateUtils;

import com.gb.ctptickets.App;
import com.gb.ctptickets.Constants;

import java.util.Date;

/**
 * Created by gbiro on 5/26/2015.
 */
public class HolidayProvider {

    private static final String HOLIDAY_CALENDAR_TITLE = "S\u0103rb\u0103tori \u00een Rom\u00e2nia";

    /**
     * Fetch all the holidays for today from the <code>{@value #HOLIDAY_CALENDAR_TITLE}</code> calendar through the native Calendar content provider
     *
     * @return titles of the holidays
     */
    public static String[] getHolidays() {
        Context context = App.getContext();
        Cursor cursor;
        Uri calendarUri;
        Uri eventUri;
        String[] calendarFields;
        String[] eventFields;
        String calendarSelect;
        String eventCalendarId;

        if (Build.VERSION.SDK_INT >= Constants.ICE_CREAM_SANDWICH) {
            calendarFields = new String[]{BaseColumns._ID, CalendarContract.Calendars.NAME,
                    CalendarContract.Calendars.CALENDAR_DISPLAY_NAME};
            calendarUri = CalendarContract.Calendars.CONTENT_URI;
            calendarSelect = CalendarContract.Calendars.NAME + "=?";

            eventUri = CalendarContract.Instances.CONTENT_URI;
            eventFields = new String[]{CalendarContract.Instances.TITLE};
            eventCalendarId = CalendarContract.Instances.CALENDAR_ID;
        } else {
            calendarFields = new String[]{BaseColumns._ID, "name", "calendar_displayName"};
            calendarUri = Uri.parse("content://com.android.calendar/calendars");
            calendarSelect = "name=?";

            eventUri = Uri.parse("content://com.android.calendar/instances/when");
            eventFields = new String[]{"title"};
            eventCalendarId = "calendar_id";
        }
        cursor = context.getContentResolver().query(calendarUri, calendarFields, calendarSelect, new String[]{HOLIDAY_CALENDAR_TITLE}, null);

        if (cursor.moveToNext()) {
            String[] holidays;
            Uri.Builder builder = eventUri.buildUpon();
            long now = new Date().getTime();
            ContentUris.appendId(builder, now);
            ContentUris.appendId(builder, now);
            Cursor eventCursor = context.getContentResolver().query(builder.build(),
                    eventFields, eventCalendarId + "=" + cursor.getLong(cursor.getColumnIndex(BaseColumns._ID)), null, null);
            holidays = new String[eventCursor.getCount()];

            for (int i = 0; i < eventCursor.getCount() && eventCursor.moveToNext(); i++) {
                holidays[i] = eventCursor.getString(eventCursor.getColumnIndex("title"));
            }
            return holidays;
        } else {
            return null;
        }
    }
}
