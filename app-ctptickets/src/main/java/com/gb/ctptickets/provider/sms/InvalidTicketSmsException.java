package com.gb.ctptickets.provider.sms;


/**
 * Used by {@link TicketSmsParser}, thrown when something is wrong with the sms format (can happen
 * if in future CTP changes the ticket sms's wording).
 *
 * Created by GABOR on 2014-nov.-08.
 */
class InvalidTicketSmsException extends Exception {

	public InvalidTicketSmsException(String detailMessage) {
		super(detailMessage);
	}
}
