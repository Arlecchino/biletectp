package com.gb.ctptickets.provider.currency.model;


import com.google.gson.annotations.SerializedName;

public class Query {

    @SuppressWarnings("unused")
    @SerializedName("query")
    public com.gb.ctptickets.provider.currency.model.Query_ query;
}
