package com.gb.ctptickets.provider.ticket;


/**
 * Handles the existing ticket requests. Receivers should set the statuses here and objects related to expired requests
 * are automatically evicted. A request is considered expired if the expiration time passes or if the ticket was never
 * received and the waiting period expired.<br>
 * An instance of this class refers to one bus line. There may be more than one request for a bus-line, identified by an
 * id.
 * 
 * Created by gbiro on 12/10/2014.
 */
public interface TicketRequestStatusRegistry {

	/**
	 * @return true if there is at least one not expired ticket available
	 */
	@SuppressWarnings("BooleanMethodIsAlwaysInverted")
	boolean hasValidTicket();


	/**
	 * Create an internal request
	 * 
	 * @param requestTimeoutDate
	 *            time at which the request is automatically evicted from the registry
	 */
	void createRequest(long requestTimeoutDate);


	/**
	 * Checks all the existing requests that were made for this bus-line and returns the id of the one that expires the
	 * soonest.
	 * 
	 * @return unique request id or -1 if there is no request for the bus-line
	 */
	long getIdForTicketWithClosestExpiration();


	/**
	 * Checks all the existing requests that were made for this bus-line and returns one that's still haven't been sent.<br>
	 *
	 * @return unique request id or -1 if there is no unsent request for the bus-line or there are no more unsent
	 *         requests
	 */
	long getNextUnsentRequestId();


	/**
	 * Checks all the existing requests that were made for this bus-line and returns one that's still haven't been
	 * delivered.<br>
	 *
	 * @return unique request id or -1 if there is no delivered request for the bus-line or there are no more delivered
	 *         requests
	 */
	long getNextUndeliveredRequestId();


	/**
	 * @param id
	 *            Unique id of the request. Generated by the app. Use {@link #getIdForTicketWithClosestExpiration()}
	 * 
	 * @return true if the either the request sending or request delivery has failed
	 * @see com.gb.ctptickets.receiver.SmsSentBroadcastReceiver
	 * @see com.gb.ctptickets.receiver.SmsDeliveredBroadcastReceiver
	 */
	boolean isRequestFailed(long id);


	/**
	 * @param id
	 *            Unique id of the request. Generated by the app. Use {@link #getIdForTicketWithClosestExpiration()}
	 * 
	 * @return true if the request sms has been successfully sent to the current carrier (doesn't mean it actually
	 *         arrived)
	 * @see com.gb.ctptickets.receiver.SmsSentBroadcastReceiver
	 */
	boolean isRequestSent(long id);


	/**
	 * @param id
	 *            Unique id of the request. Generated by the app. Use {@link #getIdForTicketWithClosestExpiration()}
	 * 
	 * @return true if the request sms has been successfully delivered by the current carrier (doesn't mean it actually
	 *         arrived)
	 * @see com.gb.ctptickets.receiver.SmsDeliveredBroadcastReceiver
	 */
	boolean isRequestDelivered(long id);


	/**
	 * @param id
	 *            Unique id of the request. Generated by the app. Use {@link #getIdForTicketWithClosestExpiration()}
	 * 
	 * @return true if there is an sms that contains a valid (not-expired) ticket corresponding to the current bus line
	 */
	boolean isValid(long id);


	/**
	 * @param id
	 *            Unique id of the request. Generated by the app. Use {@link #getIdForTicketWithClosestExpiration()}
	 * 
	 * @return the expiration date of a ticket as specified in the sms
	 */
	long getExpiryDate(long id);


	/**
	 * @param id
	 *            Unique id of the request. Generated by the app. Use {@link #getIdForTicketWithClosestExpiration()}
	 * 
	 * @return error string corresponding to the sms sent/delivered status or "Ticket is available"
	 */
	String getStatusDescription(long id);


	/**
	 * @param smsSentStatus
	 *            One of:<br>
	 *            {@link android.app.Activity#RESULT_OK}<br>
	 *            {@link android.telephony.SmsManager#RESULT_ERROR_GENERIC_FAILURE}<br>
	 *            {@link android.telephony.SmsManager#RESULT_ERROR_NO_SERVICE}<br>
	 *            {@link android.telephony.SmsManager#RESULT_ERROR_NULL_PDU}<br>
	 *            {@link android.telephony.SmsManager#RESULT_ERROR_RADIO_OFF}<br>
	 *            or 0 - unknown
	 * @param id
	 *            Unique id of the request. Generated by the app. Use {@link #getIdForTicketWithClosestExpiration()}
	 */
	void setSmsSentStatus(int smsSentStatus, long id);


	/**
	 * @param smsDeliveredStatus
	 *            One of:<br>
	 *            {@link android.app.Activity#RESULT_OK}<br>
	 *            {@link android.app.Activity#RESULT_CANCELED}<br>
	 *            or -2 - unknown
	 * @param id
	 *            Unique id of the request. Generated by the app. Use {@link #getIdForTicketWithClosestExpiration()}
	 */
	void setSmsDeliveredStatus(int smsDeliveredStatus, long id);


	@SuppressWarnings("UnusedParameters")
	void processIncomingTicket(String ticket, long expirationDate, double cost, String confirmationCode);
}
