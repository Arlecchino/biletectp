package com.gb.ctptickets.provider.ticket;


import android.content.Context;

import com.gb.ctptickets.App;
import com.gb.ctptickets.Constants;
import com.gb.ctptickets.R;
import com.gb.ctptickets.provider.currency.model.Currency;
import com.gb.ctptickets.provider.ticket.model.TicketRate;


/**
 * Provides internationalized titles and description for a ticket
 *
 * Created by gbiro on 12/10/2014.
 */
public class DefaultTicketUIProvider implements TicketUIProvider {

	private final String	ticket;


	public DefaultTicketUIProvider(String ticket) {
		this.ticket = ticket;
	}


	public String getTitle() {
		Context context = App.getContext();
		String result;

		if (context.getString(R.string.m40_met_urb_label).equals(ticket)) {
			result = context.getString(R.string.m40_met_urb);
		} else if (context.getString(R.string.m60_urb_label).equals(ticket)) {
			result = context.getString(R.string.m60_urb);
		} else if (context.getString(R.string.m60_met_urb_label).equals(ticket)) {
			result = context.getString(R.string.m60_met_urb);
		} else if (context.getString(R.string.h24_urb_label).equals(ticket)) {
			result = context.getString(R.string.h24_urb);
		} else if (context.getString(R.string.m80_met_urb_label).equals(ticket)) {
			result = context.getString(R.string.m80_met_urb);
		} else if (context.getString(R.string.h24_met_urb_label).equals(ticket)) {
			result = context.getString(R.string.h24_met_urb);
		} else {
			result = context.getString(R.string.custom_ticket, ticket);
		}
		return result;
	}


	public String getShortTitle() {
		Context context = App.getContext();
		String result;

		if (context.getString(R.string.m40_met_urb_label).equals(ticket)) {
			result = context.getString(R.string.m40_met_urb_short);
		} else if (context.getString(R.string.m60_urb_label).equals(ticket)) {
			result = context.getString(R.string.m60_urb_short);
		} else if (context.getString(R.string.m60_met_urb_label).equals(ticket)) {
			result = context.getString(R.string.m60_met_urb_short);
		} else if (context.getString(R.string.h24_urb_label).equals(ticket)) {
			result = context.getString(R.string.h24_urb_short);
		} else if (context.getString(R.string.m80_met_urb_label).equals(ticket)) {
			result = context.getString(R.string.m80_met_urb_short);
		} else if (context.getString(R.string.h24_met_urb_label).equals(ticket)) {
			result = context.getString(R.string.h24_met_urb_short);
		} else {
			result = context.getString(R.string.custom_ticket_short, ticket);
		}
		return result;
	}


	public String getPrice(TicketRate rate) {
		Context context = App.getContext();
		float basePrice = getTicketBaseEuroPrice();
		double price = rate != null ? basePrice * rate.getRate().rate : basePrice;
		Currency currency = rate != null ? rate.getTo() : Constants.BASE_CURRENCY;
		return context.getString(R.string.price, price, currency.name());
	}


	private float getTicketBaseEuroPrice() {
		Context context = App.getContext();
		float price;

		if (context.getString(R.string.m40_met_urb_label).equals(ticket)) {
			price = Constants.PRICE_M40_MET_URB;
		} else if (context.getString(R.string.m60_urb_label).equals(ticket)) {
			price = Constants.PRICE_M60_URB;
		} else if (context.getString(R.string.m60_met_urb_label).equals(ticket)) {
			price = Constants.PRICE_M60_MET_URB;
		} else if (context.getString(R.string.h24_urb_label).equals(ticket)) {
			price = Constants.PRICE_H24_URB;
		} else if (context.getString(R.string.m80_met_urb_label).equals(ticket)) {
			price = Constants.PRICE_M80_MET_URB;
		} else if (context.getString(R.string.h24_met_urb_label).equals(ticket)) {
			price = Constants.PRICE_H24_MET_URB;
		} else {
			price = Constants.PRICE_SINGLE_TRAVEL;
		}
		return price;
	}
}
