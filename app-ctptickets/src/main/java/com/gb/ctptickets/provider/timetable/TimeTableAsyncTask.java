package com.gb.ctptickets.provider.timetable;


import android.os.AsyncTask;
import android.util.Log;

import com.gb.ctptickets.AppSettings;
import com.gb.ctptickets.Constants;
import com.gb.ctptickets.provider.ticket.AvailableBusLineRegistry;
import com.gb.ctptickets.util.GoogleAnalyticsHelper;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;


/**
 * Load the bus schedules from http://www.ratuc.ro/programe1.php.<br>
 * It also fetches the latest list of bus lines and (indirectly) updates the bus line selectors in the UI.
 */
public class TimeTableAsyncTask extends AsyncTask<Void, Void, Void> {

    private static final String TAG = TimeTableAsyncTask.class.getSimpleName();

    private static TimeTableAsyncTask INSTANCE;


    private TimeTableAsyncTask() {
        // hiding constructor
    }


    /**
     * Fetch the table from RATUC with the bus lines and pdf url's. If a sync is currently running invoking this method
     * has no effect. It also updates the list of available bus-lines.
     */
    public static void run() {
        if (INSTANCE == null) {
            INSTANCE = new TimeTableAsyncTask();
            INSTANCE.execute();
        }
    }


    @Override
    protected Void doInBackground(Void... params) {
        Log.d(TAG, "Sync started (" + Constants.RATUC_URL_TIMETABLE + ")");
        updateTimetableURLsAndBusLines();
        INSTANCE = null;
        Log.d(TAG, "Sync finished");
        return null;
    }


    private static void updateTimetableURLsAndBusLines() {
        try {
            Document doc = Jsoup.connect(Constants.RATUC_URL_TIMETABLE).get();
            Element table = doc.select("table#rightTbl").get(0);
            Map<String, String> pdfUrlMap = new HashMap<>();
            TreeSet<String> busLines = new TreeSet<>();

            for (Element row : table.select("tr")) {
                Elements columns = row.select("td");

                if (columns.size() == 2) {
                    String pdfUrl = columns.get(1).child(0).attr("href");

                    String line = columns.get(0).child(0).text().toUpperCase();
                    String busLine = line.substring(6, line.length());

                    pdfUrlMap.put(busLine, Constants.RATUC_URL_BASE + "/" + pdfUrl);
                    pdfUrlMap.put(Constants.RATUC_URL_BASE + "/" + pdfUrl, busLine);
                    busLines.add(busLine);
                }
            }
            AppSettings.setPdfUriMap(pdfUrlMap);
            AppSettings.setLastSyncDate(System.currentTimeMillis());
            updateBusLines(busLines.toArray(new String[busLines.size()]));
        } catch (IOException e) {
            Log.e(TAG, "", e);
            GoogleAnalyticsHelper.onSyncError("updateTimetableURLsAndBusLines");
        }
    }


    private static void updateBusLines(String[] newBusLines) {
        if (newBusLines == null || newBusLines.length < 1) {
            return;
        }

        String[] currentBusLines = AvailableBusLineRegistry.getAllBusLines(false, false);

        // determine bus lines that should be removed
        Set<String> setNew = new HashSet<String>(Arrays.asList(newBusLines));
        Set<String> toBeDeleted = new HashSet<String>(Arrays.asList(currentBusLines));
        toBeDeleted.removeAll(setNew);
        AppSettings.setHiddenBusLines(toBeDeleted.toArray(new String[toBeDeleted.size()]));

        // determine bus lines that should be added
        Set<String> toBeAdded = new HashSet<>(Arrays.asList(newBusLines));
        Set<String> setOld = new HashSet<>(Arrays.asList(currentBusLines));
        toBeAdded.removeAll(setOld);
        AppSettings.setNewBusLines(toBeAdded.toArray(new String[toBeAdded.size()]));
    }
}