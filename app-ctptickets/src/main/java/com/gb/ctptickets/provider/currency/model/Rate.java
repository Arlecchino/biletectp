package com.gb.ctptickets.provider.currency.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings({"CanBeFinal", "WeakerAccess"})
public class Rate implements Parcelable {

	public static final Parcelable.Creator<Rate>	CREATOR	= new Parcelable.Creator<Rate>() {

																@Override
																public Rate createFromParcel(Parcel in) {
																	return new Rate(in);
																}


																@Override
																public Rate[] newArray(int size) {
																	return new Rate[size];
																}
															};
	@SerializedName("id")
	public String									id;
	@SerializedName("Name")
	public String									name;
	@SerializedName("Rate")
	public double									rate;
	@SerializedName("Date")
	public String									date;
	@SerializedName("Time")
	public String									time;
	@SerializedName("Ask")
	public double									ask;
	@SerializedName("Bid")
	public double									bid;


	public Rate(Parcel in) {
		id = in.readString();
		name = in.readString();
		rate = in.readDouble();
		date = in.readString();
		time = in.readString();
		ask = in.readDouble();
		bid = in.readDouble();
	}


	@Override
	public int describeContents() {
		return 0;
	}


	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(id);
		dest.writeString(name);
		dest.writeDouble(rate);
		dest.writeString(date);
		dest.writeString(time);
		dest.writeDouble(ask);
		dest.writeDouble(bid);
	}
}
