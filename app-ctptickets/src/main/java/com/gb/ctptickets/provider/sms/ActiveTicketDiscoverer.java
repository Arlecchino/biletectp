package com.gb.ctptickets.provider.sms;


import java.util.ArrayList;
import java.util.List;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.Telephony;

import com.gb.ctptickets.App;
import com.gb.ctptickets.Constants;
import com.gb.ctptickets.provider.sms.model.TicketSmsInfo;
import com.gb.ctptickets.util.HumanTime;


/**
 * Reads the phone's SMS content provider to see if there are any non-expired tickets available
 * 
 * Created by GABOR on 2014-szept.-11.
 */
public class ActiveTicketDiscoverer {

	private static final long	OLDEST_SMS_TO_SCAN_FOR	= HumanTime.Time.DAY.valueInMilisecs;
	private static final int	KITKAT					= 19;

	private static ActiveTicketDiscoverer INSTANCE;


	private ActiveTicketDiscoverer() {
		// hiding constructor
	}


	public static ActiveTicketDiscoverer get() {
		if (INSTANCE == null) {
			INSTANCE = new ActiveTicketDiscoverer();
		}
		return INSTANCE;
	}


	/**
	 * Scan all incoming sms's from the number {@value com.gb.ctptickets.Constants#TICKET_BUY_PHONE_NUMBER} and return
	 * all that are still active
	 */
	public TicketSmsInfo[] getActiveTickets() {
		List<TicketSmsInfo> result = new ArrayList<TicketSmsInfo>();
		Uri inboxURI;
		String idColumnName;
		String addressColumnName;
		String bodyColumnName;
		String dateColumnName;
		String[] columns;
		String selection;
		String[] selectionArgs;
		String sortBy;

		if (Build.VERSION.SDK_INT < KITKAT) {
			inboxURI = Uri.parse("content://sms/inbox");
			idColumnName = "_id";
			addressColumnName = "address";
			bodyColumnName = "body";
			dateColumnName = "date";
		} else {
			inboxURI = Telephony.Sms.CONTENT_URI;
			idColumnName = Telephony.Sms._ID;
			addressColumnName = Telephony.Sms.ADDRESS;
			bodyColumnName = Telephony.Sms.BODY;
			dateColumnName = Telephony.Sms.DATE;
		}
		columns = new String[] {
				idColumnName, addressColumnName, bodyColumnName, dateColumnName
		};
		selection = addressColumnName + " = ? AND " + dateColumnName + " >= ?";
		sortBy = dateColumnName + " ASC";

		selectionArgs = new String[] {
				Constants.TICKET_BUY_PHONE_NUMBER, Long.toString(OLDEST_SMS_TO_SCAN_FOR)
		};
		ContentResolver cr = App.getContext().getContentResolver();
		Cursor cursor = cr.query(inboxURI, columns, selection, selectionArgs, sortBy);

		while (cursor.moveToNext()) {
			long date = cursor.getLong(cursor.getColumnIndex(dateColumnName));
			String body = cursor.getString(cursor.getColumnIndex(bodyColumnName));

			try {
				TicketSmsInfo ticketSmsInfo = new TicketSmsParser(body, date).parse();

				if (ticketSmsInfo != null && ticketSmsInfo.getValidUntil() != null
						&& ticketSmsInfo.getValidUntil().getTime() > System.currentTimeMillis()) {
					result.add(ticketSmsInfo);
				}
			} catch (InvalidTicketSmsException e) {
				// it is acceptable that some sms's are not parsable (like the outbound sms's with bus line numbers)
			}
		}
		cursor.close();
		return result.toArray(new TicketSmsInfo[result.size()]);
	}
}
