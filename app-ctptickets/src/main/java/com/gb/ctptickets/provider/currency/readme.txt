How to use:

Callback<Query> callback = new Callback<Query>() {

                    				@Override
                    				public void success(Query query, Response response) {
                    				    Rate rate = query.getQuery().getResults().getRate();
                    				    //...
                    				}


                    				@Override
                    				public void failure(RetrofitError error) {
                    				    //...
                    				}
                    			};
CurrencyProvider.get().getRate(Currency.EUR, Currency.RON, callback);






Needs Retrofit! (compile 'com.squareup.retrofit:retrofit:1.6.1+')