package com.gb.ctptickets.provider.eventbus;


import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import com.gb.ctptickets.provider.currency.model.Query;

import de.greenrobot.event.EventBus;


/**
 * Callback used by async retrofit background processes to notify the ui. It sends {@link RetrofitTaskEvent}s through
 * EventBus.<br>
 * If you want to use it, extend {@link RetrofitTaskEvent} and tell it how to deal with retrofit success or error and
 * pass it to a callback instance. Then pass the callback instance to the retorfit invocation.
 * 
 * Created by gbiro on 12/15/2014.
 */
public class DefaultRetrofitTaskCallback implements Callback<Query> {

	private final RetrofitTaskEvent	event;


	public DefaultRetrofitTaskCallback(RetrofitTaskEvent event) {
		this.event = event;
	}


	public void sendStart() {
		event.populateOnStart();
		EventBus.getDefault().post(event);
	}


	@Override
	public void success(Query query, Response response) {
		event.populateOnSuccess(query);
		EventBus.getDefault().post(event);
		sendFinish();
	}


	@Override
	public void failure(RetrofitError error) {
		event.populateOnError(error);
		EventBus.getDefault().post(event);
		sendFinish();
	}


	private void sendFinish() {
		event.populateOnFinish();
		EventBus.getDefault().post(event);
	}
}
