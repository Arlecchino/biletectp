package com.gb.ctptickets.provider.eventbus;


import retrofit.RetrofitError;

import com.gb.ctptickets.provider.currency.model.Query;


/**
 * Used to track and publish some retrofit requests
 *
 * Created by gbiro on 12/15/2014.
 */
public class RetrofitTaskEvent {

	public enum Type {
		START, SUCCESS, FAIL, FINISH
	}

	private Type	type;


	public Type getType() {
		return type;
	}


	/**
	 * Don't forget to invoke super. when overriding this method.
	 */
	public void populateOnStart() {
		type = Type.START;
	}


	/**
	 * Don't forget to invoke super. when overriding this method.
	 */
	public void populateOnSuccess(Query query) {
		type = Type.SUCCESS;
	}


	/**
	 * Don't forget to invoke super. when overriding this method.
	 */
	public void populateOnError(RetrofitError error) {
		type = Type.FAIL;
	}


	/**
	 * Don't forget to invoke super. when overriding this method.
	 */
	public void populateOnFinish() {
		type = Type.FINISH;
	}
}
