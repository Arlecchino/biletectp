package com.gb.ctptickets.provider.ticket;


import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;

import com.gb.ctptickets.Constants;
import com.gb.ctptickets.UserSettings;
import com.gb.ctptickets.receiver.SmsDeliveredBroadcastReceiver;
import com.gb.ctptickets.receiver.SmsSentBroadcastReceiver;
import com.gb.ctptickets.ui.dialog.TicketLateDialog;
import com.gb.ctptickets.ui.eventbus.TicketStatusChangedEvent;
import com.gb.ctptickets.util.GoogleAnalyticsHelper;
import com.gb.ctptickets.util.HumanTime;
import com.gb.ctptickets.util.NotificationBuilder;
import com.gb.ctptickets.util.SmsSender;

import de.greenrobot.event.EventBus;


/**
 * This class is responsible for buying a ticket. Uses {@link android.provider.Telephony.Sms}.
 *
 * Created by GABOR on 2014-szept.-09.
 */
public class TicketBuyer {

    public static final String EXTRA_TICKET = "EXTRA_TICKET";
    private static final String EXTRA_REQUEST_ID = "EXTRA_REQUEST_ID";

    private Handler handler;
    private final String ticket;


    TicketBuyer(String ticket) {
        this.ticket = ticket;
        new Thread() {

            @Override
            public void run() {
                try {
                    Looper.prepare();
                    handler = new Handler();
                    Looper.loop();
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        }.start();
    }


    public void buy() {
        TicketRequestStatusRegistry registry = TicketHandlerFactory.getRequestStatusRegistry(ticket);

        if (!UserSettings.getMockTicketsEnabled()) {
            Intent sentCallback = new Intent(SmsSentBroadcastReceiver.INTENT_SENT);
            sentCallback.putExtra(EXTRA_TICKET, ticket);
            // doesn't really matter what we put here as long as it's unique
            sentCallback.putExtra(EXTRA_REQUEST_ID, System.currentTimeMillis());
            Intent deliveredCallback = new Intent(SmsDeliveredBroadcastReceiver.INTENT_DELIVERED);
            deliveredCallback.putExtra(EXTRA_TICKET, ticket);
            new SmsSender(Constants.TICKET_BUY_PHONE_NUMBER, ticket, sentCallback, deliveredCallback).send();
            GoogleAnalyticsHelper.onSmsSent(ticket);
        }
        final int waitingTime = UserSettings.getTicketWaitingTimeoutMilisecs();
        registry.createRequest(System.currentTimeMillis() + waitingTime);

        if (UserSettings.getMockTicketsEnabled()) {
            long id = registry.getNextUnsentRequestId();
            registry.setSmsSentStatus(Activity.RESULT_OK, id);
        }
        TicketHistory.reportTicketBought(ticket);
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                TicketRequestStatusRegistry registry = TicketHandlerFactory.getRequestStatusRegistry(ticket);

                if (!UserSettings.getMockTicketsEnabled() || UserSettings.getMockBuyFailedEnabled()) {
                    // we show the notification for any pending request
                    if (!registry.hasValidTicket()) {
                        GoogleAnalyticsHelper.onTicketLate(ticket, waitingTime);

                        // show the notification if not disabled
                        if (UserSettings.isNotificationsEnabled()) {
                            new NotificationBuilder(ticket).show();
                        }

                        // show the dialog if not disabled
                        if (!UserSettings.getTicketLateWarningNewerShowAgain()) {
                            TicketLateDialog.show(ticket);
                        }
                    }
                } else if (!UserSettings.getMockBuyFailedEnabled()) {
                    // insert dummy ticket
                    registry.processIncomingTicket(ticket, System.currentTimeMillis()
                            + HumanTime.Time.MINUTE.valueInMilisecs * 3, 0, "");
                }
                EventBus.getDefault().post(new TicketStatusChangedEvent(ticket));
            }
        }, UserSettings.getTicketWaitingTimeoutMilisecs());
    }
}
