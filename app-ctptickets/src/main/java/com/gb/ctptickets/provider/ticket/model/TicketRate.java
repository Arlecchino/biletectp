package com.gb.ctptickets.provider.ticket.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.gb.ctptickets.provider.currency.model.Currency;
import com.gb.ctptickets.provider.currency.model.Rate;


/**
 * Created by GABOR on 2014-szept.-09.
 */
public class TicketRate implements Parcelable {

	public static final Parcelable.Creator<TicketRate>	CREATOR	= new Parcelable.Creator<TicketRate>() {

																	@Override
																	public TicketRate createFromParcel(Parcel in) {
																		return new TicketRate(in);
																	}


																	@Override
																	public TicketRate[] newArray(int size) {
																		return new TicketRate[size];
																	}
																};

	private final Currency								from;
	private final Currency								to;
	private final Rate									rate;


	public TicketRate(@SuppressWarnings("SameParameterValue") Currency from, Currency to, Rate rate) {
		this.from = from;
		this.to = to;
		this.rate = rate;
	}


	private TicketRate(Parcel in) {
		from = Currency.values()[in.readInt()];
		to = Currency.values()[in.readInt()];
		rate = in.readParcelable(Rate.class.getClassLoader());
	}


	@SuppressWarnings("unused")
	public Currency getFrom() {
		return from;
	}


	public Currency getTo() {
		return to;
	}


	public Rate getRate() {
		return rate;
	}


	@Override
	public int describeContents() {
		return 0;
	}


	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(from.ordinal());
		dest.writeInt(to.ordinal());
		dest.writeParcelable(rate, 0);
	}
}
