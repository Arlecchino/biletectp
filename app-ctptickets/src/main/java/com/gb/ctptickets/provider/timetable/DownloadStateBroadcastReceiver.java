package com.gb.ctptickets.provider.timetable;


import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.widget.Toast;

import com.gb.ctptickets.App;
import com.gb.ctptickets.AppSettings;
import com.gb.ctptickets.Constants;
import com.gb.ctptickets.ui.eventbus.TimeTableDownloadStateChangedEvent;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.greenrobot.event.EventBus;


/**
 * Reacts to changes in the downloading of a bus timetable, most importantly it notifies the
 * {@link com.gb.ctptickets.ui.dialog.BuyTicketConfirmationDialog} to stop the progress indicator and show the downloaded pdf
 *
 * Created by GABOR on 2014-nov.-29.
 */
public class DownloadStateBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        DownloadManager downloadManager = (android.app.DownloadManager) App.getContext().getSystemService(
                Context.DOWNLOAD_SERVICE);

        if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
            long downloadId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0);
            DownloadManager.Query q;
            q = new DownloadManager.Query();
            q.setFilterById(downloadId);
            Cursor cursor = downloadManager.query(q);

            if (cursor.moveToFirst()) {
                int status = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));
                int reason = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_REASON));
                String url = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_URI));

                Map<String, String> pdfUrlMap = AppSettings.getPdfUriMap();
                String busLine = pdfUrlMap != null ? pdfUrlMap.get(url) : null;

                // verify that this is our download
                if (busLine != null) {
                    // rename if file was overridden
                    String path = null;

                    if (Build.VERSION.SDK_INT >= Constants.HONEYCOMB) {
                        path = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_FILENAME));
                    } else {
                        path = Uri.parse(cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI))).getEncodedPath();
                    }
                    TimeTableHandler timeTableHandler = new TimeTableHandler(busLine);
                    String expectedPath = timeTableHandler.getFile(Uri.parse(url)).getPath();
                    boolean dontNotify = false;

                    if (!expectedPath.equals(path)) {
                        if (!new File(path).renameTo(new File(expectedPath))) {
                            // rename failed, delete and download again
                            timeTableHandler.deleteFromDevice();
                            try {
                                timeTableHandler.download();
                            } catch (TimeTableUnavailableException e) {
                                e.printStackTrace();
                            }
                            dontNotify = true;
                        }
                    }

                    if (!dontNotify) {
                        // check download status
                        switch (status) {
                            case DownloadManager.STATUS_FAILED:
                            case DownloadManager.STATUS_PAUSED:
                                EventBus.getDefault()
                                        .post(new TimeTableDownloadStateChangedEvent(busLine,
                                                TimeTableDownloadStateChangedEvent.STATE_ERROR, getReasonString(status, reason)));
                                break;
                            case DownloadManager.STATUS_SUCCESSFUL:
                                EventBus.getDefault().post(
                                        new TimeTableDownloadStateChangedEvent(busLine,
                                                TimeTableDownloadStateChangedEvent.STATE_FINISHED, null));
                                break;
                        }
                    }
                }
            }
            cursor.close();
        } else if (DownloadManager.ACTION_NOTIFICATION_CLICKED.equals(action)) {
            DownloadManager.Query q;
            Cursor cursor = downloadManager.query(new DownloadManager.Query());
            List<Long> downloadIds = new ArrayList<>();

            while (cursor.moveToNext()) {
                downloadIds.add(cursor.getLong(cursor.getColumnIndex(DownloadManager.COLUMN_ID)));
            }
            cursor.close();

            for (long downloadId : downloadIds) {
                q = new DownloadManager.Query();
                q.setFilterById(downloadId);
                cursor = downloadManager.query(q);

                if (cursor.moveToFirst()) {
                    int status = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));
                    int reason = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_REASON));
                    String url = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_URI));
                    String statusStr;

                    Map<String, String> pdfUrlMap = AppSettings.getPdfUriMap();
                    String busLine = pdfUrlMap != null ? pdfUrlMap.get(url) : null;

                    // verify that this is our download
                    if (busLine != null) {
                        switch (status) {
                            case DownloadManager.STATUS_FAILED:
                                statusStr = "(" + busLine + ") Download failed (" + getReasonString(status, reason) + ")";
                                break;
                            case DownloadManager.STATUS_PAUSED:
                                statusStr = "(" + busLine + ") Download paused... (" + getReasonString(status, reason)
                                        + ")";
                                break;
                            case DownloadManager.STATUS_PENDING:
                                statusStr = "(" + busLine + ") Download pending...";
                                break;
                            case DownloadManager.STATUS_RUNNING:
                                statusStr = "(" + busLine + ") Download running...";
                                break;
                            case DownloadManager.STATUS_SUCCESSFUL:
                                statusStr = "(" + busLine + ") Download successful";
                                break;
                            default:
                                statusStr = "(" + busLine + ") Download state unknown";
                        }
                        Toast.makeText(context, statusStr, Toast.LENGTH_SHORT).show();
                    }
                }
                cursor.close();
            }
        }
    }


    private static String getReasonString(int statusId, int reasonId) {
        if (statusId == DownloadManager.STATUS_PENDING || statusId == DownloadManager.STATUS_RUNNING
                || statusId == DownloadManager.STATUS_SUCCESSFUL) {
            return null;
        }

        switch (reasonId) {
            case DownloadManager.ERROR_FILE_ERROR: // 1001 (0x000003e9)
                // a storage issue which doesn't fit under any other error code. Use the more specific ERROR_INSUFFICIENT_SPACE and ERROR_DEVICE_NOT_FOUND when appropriate
                return "ERROR_FILE_ERROR";
            case DownloadManager.ERROR_UNHANDLED_HTTP_CODE: // 1002 (0x000003ea)
                // an HTTP code was received that download manager can't handle
                return "ERROR_UNHANDLED_HTTP_CODE";
            case DownloadManager.ERROR_HTTP_DATA_ERROR: // 1004 (0x000003ec)
                //an error receiving or processing data occurred at the HTTP level
                return "ERROR_HTTP_DATA_ERROR";
            case DownloadManager.ERROR_TOO_MANY_REDIRECTS: // 1005 (0x000003ed)
                //Value of COLUMN_REASON when there were too many redirects.
                return "ERROR_TOO_MANY_REDIRECTS";
            case DownloadManager.ERROR_INSUFFICIENT_SPACE: // 1006 (0x000003ee)
                // There was insufficient storage space. Typically, this is because the SD card is full.
                return "ERROR_INSUFFICIENT_SPACE";
            case DownloadManager.ERROR_DEVICE_NOT_FOUND: // 1007 (0x000003ef)
                // No external storage device was found. Typically, this is because the SD card is not mounted.
                return "ERROR_DEVICE_NOT_FOUND";
            case DownloadManager.ERROR_CANNOT_RESUME: // 1008 (0x000003f0)
                // Some possibly transient error occurred but we can't resume the download.
                return "ERROR_CANNOT_RESUME";
            case DownloadManager.ERROR_FILE_ALREADY_EXISTS: // 1009 (0x000003f1)
                // The requested destination file already exists (the download manager will not overwrite an existing file).
                return "ERROR_FILE_ALREADY_EXISTS";

            case DownloadManager.PAUSED_WAITING_TO_RETRY: // 1 (0x00000001)
                // Value of COLUMN_REASON when the download is paused because some network error occurred and the download manager is waiting before retrying the request.
                return "PAUSED_WAITING_TO_RETRY";
            case DownloadManager.PAUSED_WAITING_FOR_NETWORK: // 2 (0x00000002)
                // Value of COLUMN_REASON when the download is waiting for network connectivity to proceed.
                return "PAUSED_WAITING_FOR_NETWORK";
            case DownloadManager.PAUSED_QUEUED_FOR_WIFI: // 3 (0x00000003)
                // Value of COLUMN_REASON when the download exceeds a size limit for downloads over the mobile network and the download manager is waiting for a Wi-Fi connection to proceed.
                return "PAUSED_QUEUED_FOR_WIFI";
            case DownloadManager.PAUSED_UNKNOWN: // 4 (0x00000004)
                // Value of COLUMN_REASON when the download is paused for some other reason.
                return "PAUSED_UNKNOWN";

            case 100:
                return "HTTP 100: Continue";
            case 101:
                return "HTTP 101: Switching Protocols";
            case 200:
                return "HTTP 200: OK";
            case 201:
                return "HTTP 201: Created";
            case 202:
                return "HTTP 202: Accepted";
            case 203:
                return "HTTP 203: Non-Authoritative Information";
            case 204:
                return "HTTP 204: No Content";
            case 205:
                return "HTTP 205: Reset Content";
            case 206:
                return "HTTP 206: Partial Content";
            case 300:
                return "HTTP 300: Multiple Choices";
            case 301:
                return "HTTP 301: Moved Permanently";
            case 302:
                return "HTTP 302: Found";
            case 303:
                return "HTTP 303: See Other";
            case 304:
                return "HTTP 304: Not Modified";
            case 305:
                return "HTTP 305: Use Proxy";
            case 307:
                return "HTTP 307: Temporary Redirect";
            case 400:
                return "HTTP 400: Bad Request";
            case 401:
                return "HTTP 401: Unauthorized";
            case 402:
                return "HTTP 402: Payment Required";
            case 403:
                return "HTTP 403: Forbidden";
            case 404:
                return "HTTP 404: Not Found";
            case 405:
                return "HTTP 405: Method Not Allowed";
            case 406:
                return "HTTP 406: Not Acceptable";
            case 407:
                return "HTTP 407: Proxy Authentication Required";
            case 408:
                return "HTTP 408: Request Time-out";
            case 409:
                return "HTTP 409: Conflict";
            case 410:
                return "HTTP 410: Gone";
            case 411:
                return "HTTP 411: Length Required";
            case 412:
                return "HTTP 412: Precondition Failed";
            case 413:
                return "HTTP 413: Request Entity Too Large";
            case 414:
                return "HTTP 414: Request-URI Too Large";
            case 415:
                return "HTTP 415: Unsupported Media Type";
            case 416:
                return "HTTP 416: Requested range not satisfiable";
            case 417:
                return "HTTP 417: Expectation Failed";
            case 500:
                return "HTTP 500: Internal Server Error";
            case 501:
                return "HTTP 501: Not Implemented";
            case 502:
                return "HTTP 502: Bad Gateway";
            case 503:
                return "HTTP 503: Service Unavailable";
            case 504:
                return "HTTP 504: Gateway Time-out";
            case 505:
                return "HTTP 505: HTTP Version not supported";
            case DownloadManager.ERROR_UNKNOWN: // 1000 (0x000003e8)
                // the download has completed with an error that doesn't fit under any other error code
            default:
                return "ERROR_UNKNOWN";
        }
    }
}
