package com.gb.ctptickets.provider.carrier;

import android.content.Context;
import android.telephony.TelephonyManager;

import com.gb.ctptickets.App;

/**
 * Created by gbiro on 5/26/2015.
 */
public class NetworkOperatorProvider {

    private static final String OPERATOR_NAME_ORANGE = "orange";
    private static final String OPERATOR_NAME_VODAFONE = "vodafone";
    private static final String OPERATOR_NAME_DIGI = "digi";

    /**
     * @return true if the current network operator is one of Orange, Vodafone or Digi.Mobil
     */
    public static boolean validateNetworkOperator(String networkOperatorName) {
        networkOperatorName = networkOperatorName.toLowerCase();
        return networkOperatorName != null && (networkOperatorName.contains(OPERATOR_NAME_ORANGE) ||
                networkOperatorName.contains(OPERATOR_NAME_VODAFONE) || networkOperatorName.contains(OPERATOR_NAME_DIGI));
    }

    public static String getNetworkOperator() {
        Context context = App.getContext();
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getNetworkOperatorName();
    }
}
