package com.gb.ctptickets.provider.sms;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.gb.ctptickets.App;
import com.gb.ctptickets.Constants;
import com.gb.ctptickets.provider.sms.model.TicketSmsInfo;
import com.gb.ctptickets.util.ArraySearcher;


/**
 * Created by gbiro on 12/10/2014.
 */
public class TicketSmsParser {

	private static final String	PATTERN_SINGLE_TRAVLE_TICKET	= "Biletul pentru linia ([\\w]+)";
	private static final String	PATTERN_VALID_UNTIL				= "Valabil pana la ([\\d]{1,2}:[\\d]{2} in [\\d]{1,2}/[\\d]{1,2}/[\\d]{4})";
	private static final String	PATTERN_COST					= "Cost total:([\\d,\\.]+) EUR\\+";
	private static final String	PATTERN_CONFIRMATION_CODE		= "Cod confirmare:[\\s]*([\\w]+)";
	private static final String	PATTERN_ALL_LINES_METRO_URBAN	= "Biletul pe toate liniile METROPOLITANE SI URBANE a fost activat";
	private static final String	PATTERN_ALL_LINES_URBAN			= "Biletul pe toate liniile URBANE a fost activat";

	private final String			smsBody;
	private final long			arrivalDate;


	public TicketSmsParser(String smsBody, long arrivalDate) {
		this.smsBody = smsBody;
		this.arrivalDate = arrivalDate;
	}


	/**
	 * Parse the sms message to extract information about the ticket
	 *
	 * @return null if the message doesn't have a certain expected structure, a
	 *         {@link com.gb.ctptickets.provider.sms.model.TicketSmsInfo} instance otherwise
	 */
	public TicketSmsInfo parse() throws InvalidTicketSmsException {
		TicketSmsInfo result = new TicketSmsInfo();
		Date validUntil;
		//        Biletul pentru linia 48 a fost activat. Valabil pana la 17:10 in 07/11/2014. Cost total:0.50 eur+Tva. Cod confirmare:PFQDLXPT.

		SimpleDateFormat format = new SimpleDateFormat("HH:mm 'in' dd/MM/yyyy");
		try {
			validUntil = format.parse(getFirstMatch(smsBody, PATTERN_VALID_UNTIL));
			result.setValidUntil(validUntil);
		} catch (ParseException e) {
			e.printStackTrace();
			throw new InvalidTicketSmsException(e.getMessage());
		}

		try {
			result.setTicket(getFirstMatch(smsBody, PATTERN_SINGLE_TRAVLE_TICKET));
		} catch (InvalidTicketSmsException e) {
			try {
				getFirstMatch(smsBody, PATTERN_ALL_LINES_METRO_URBAN);
				result.setTicket(getMetroUrbanSeasonTicket(arrivalDate, validUntil.getTime()));
			} catch (InvalidTicketSmsException e2) {
				getFirstMatch(smsBody, PATTERN_ALL_LINES_URBAN);
				result.setTicket(getUrbanSeasonTicket(arrivalDate, validUntil.getTime()));
			}
		}

		try {
			String cost = getFirstMatch(smsBody, PATTERN_COST);
			result.setCost(Double.valueOf(cost.replace(',', '.')));
		} catch (NumberFormatException e) {
			e.printStackTrace();
			throw new InvalidTicketSmsException(e.getMessage());
		}

		result.setConfirmationCode(getFirstMatch(smsBody, PATTERN_CONFIRMATION_CODE));

		return result;
	}


	private static String getFirstMatch(String text, String pattern) throws InvalidTicketSmsException {
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(text);

		if (m.find()) {
			if (m.groupCount() > 0) {
				return m.group(1);
			} else {
				return m.group();
			}
		} else {
			throw new InvalidTicketSmsException("Cannot match \"" + pattern + "\" in \"" + text + "\"");
		}
	}


	private static String getMetroUrbanSeasonTicket(long smsArrivalTimestamp, long validUntilTimestamp) {
		long duration = validUntilTimestamp - smsArrivalTimestamp;
		int index = new ArraySearcher(Constants.DEFAULT_METRO_URBAN_TICKETS_DURATION).closest(duration);
		return App.getContext().getString(Constants.DEFAULT_METRO_URBAN_TICKETS[index]);
	}


	private static String getUrbanSeasonTicket(long smsArrivalTimestamp, long validUntilTimestamp) {
		long duration = validUntilTimestamp - smsArrivalTimestamp;
		int index = new ArraySearcher(Constants.DEFAULT_URBAN_TICKETS_DURATION).closest(duration);
		return App.getContext().getString(Constants.DEFAULT_URBAN_TICKETS[index]);
	}
}
