package com.gb.ctptickets.provider.ticket;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.ArrayUtils;

import android.content.Context;
import android.text.TextUtils;

import com.gb.ctptickets.App;
import com.gb.ctptickets.AppSettings;
import com.gb.ctptickets.Constants;
import com.gb.ctptickets.R;
import com.gb.ctptickets.UserSettings;


/**
 * Keeps stock of all the available bus lines.
 * <p/>
 * Created by gbiro on 8/17/2014.
 */
public class AvailableBusLineRegistry {

    /**
     * Return all bus lines (default + custom) that haven't been used in a custom ticket.
     *
     * @param includeAddNewButton if set to true, the array will contain a special item for adding a custom bus line
     * @param unusedOnly          only include tickets that are not yet used as custom ticket
     * @return String array of bus line ids
     */
    public static String[] getAllBusLines(boolean includeAddNewButton, boolean unusedOnly) {
        Context context = App.getContext();
        String[] busLines = Constants.DEFAULT_BUS_LINES; // bus lines that have been hardcoded in the app
        busLines = ArrayUtils.addAll(busLines, AppSettings.getNewBusLines()); // everything ratuc.ro has that DEFAULT_BUS_LINES doesn't
        busLines = ArrayUtils.removeElements(busLines, AppSettings.getHiddenBusLines()); // everything that was removed on ratuc.ro
        busLines = ArrayUtils.addAll(busLines, UserSettings.getCustomBusLines()); // abonamente

        Arrays.sort(busLines, new BusLineComparator());

        if (unusedOnly) {
            busLines = ArrayUtils.removeElements(busLines, UserSettings.getCustomTickets()); // remove bus lines that were already favorited
        }

        if (includeAddNewButton) {
            busLines = ArrayUtils.add(busLines, context.getString(R.string.new_)); // add the "new" button
        }
        return busLines;
    }


    /**
     * @return true if the specified <code>ticket</code> is a seasonal ticket (abonament)
     */
    public static boolean isSeasonalTicketValue(String ticket) {
        List<String> defaultTickets = new ArrayList<>();

        for (int i = 0; i < Constants.DEFAULT_URBAN_TICKETS.length; i++) {
            defaultTickets.add(App.getContext().getString(Constants.DEFAULT_URBAN_TICKETS[i]));
        }
        for (int i = 0; i < Constants.DEFAULT_METRO_URBAN_TICKETS.length; i++) {
            defaultTickets.add(App.getContext().getString(Constants.DEFAULT_METRO_URBAN_TICKETS[i]));
        }
        return defaultTickets.indexOf(ticket) >= 0;
    }

    /**
     * Compares two expressions quazi-alphanumerically but not in a character-by-character way. It splits the strings up
     * into contiguous numeric or non-numeric spans and compares these spans. Numbers have priority<br>
     * Ex:<br>
     * 1 < 2<br>
     * 2A < 2B<br>
     * 2 < A<br>
     * M4 < M11 (!)<br>
     */
    private static class BusLineComparator implements Comparator<String> {

        /**
         * Splits a string into contiguous numeric or non-numeric spans
         *
         * @return array of Integers and Strings
         */
        private static Object[] getChunks(String str) {
            Matcher m = Pattern.compile("\\d+|\\D+").matcher(str);
            List<Object> chunks = new ArrayList<>();

            while (m.find()) {
                String group = m.group();

                if (!TextUtils.isEmpty(group)) {
                    try {
                        chunks.add(Integer.valueOf(group));
                    } catch (NumberFormatException e) {
                        chunks.add(group);
                    }
                }
            }
            return chunks.toArray(new Object[chunks.size()]);
        }


        @Override
        public int compare(String lhs, String rhs) {
            Object[] lChunks = getChunks(lhs);
            Object[] rChunks = getChunks(rhs);

            int index = 0;

            do {
                if (lChunks[index] instanceof Integer) {
                    if (rChunks[index] instanceof Integer) {
                        if (lChunks[index].equals(rChunks[index])) {
                            index++;
                        } else {
                            return (Integer) lChunks[index] - (Integer) rChunks[index];
                        }
                    } else {
                        // numbers have priority
                        return -1;
                    }
                } else {
                    if (rChunks[index] instanceof Integer) {
                        // numbers have priority
                        return 1;
                    } else {
                        if (lChunks[index].equals(rChunks[index])) {
                            index++;
                        } else {
                            return ((String) lChunks[index]).toLowerCase().compareTo(
                                    ((String) rChunks[index]).toLowerCase());
                        }
                    }
                }
            } while (index < lChunks.length && index < rChunks.length);
            return lChunks.length - rChunks.length;
        }
    }
}
