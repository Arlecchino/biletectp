package com.gb.ctptickets.provider.currency.eventbus;


import retrofit.RetrofitError;

import com.gb.ctptickets.provider.currency.model.Query;
import com.gb.ctptickets.provider.currency.model.Rate;
import com.gb.ctptickets.provider.eventbus.RetrofitTaskEvent;


/**
 * Retrofit callback to catch the results of a currency query to yahoo
 *
 * Created by gbiro on 12/15/2014.
 */
public class CurrencyFetchingTaskEvent extends RetrofitTaskEvent {

	private Rate	result;
	private String	errorMessage;


	@Override
	public void populateOnSuccess(Query query) {
        super.populateOnSuccess(query);
		result = query.query.results.rate;
	}


	@Override
	public void populateOnError(RetrofitError error) {
        super.populateOnError(error);
		errorMessage = error.getMessage();
	}


	public Rate getResult() {
		return result;
	}


	public String getErrorMessage() {
		return errorMessage;
	}
}
