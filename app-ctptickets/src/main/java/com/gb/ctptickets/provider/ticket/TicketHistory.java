package com.gb.ctptickets.provider.ticket;

import com.gb.ctptickets.AppSettings;
import com.gb.ctptickets.UserSettings;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/**
 * Keeps record of all the bought tickets (send sms's anyway)
 * <p/>
 * Created by gbiro on 6/5/2015.
 */
public class TicketHistory {

    public static void reportTicketBought(String ticket) {
        Map<String, Integer> ticketHistory = AppSettings.getTicketHistory();
        Integer count = ticketHistory.get(ticket);
        ticketHistory.put(ticket, count != null ? count + 1 : 1);
        AppSettings.setTicketHistory(ticketHistory);
    }

    public static String[] getTopTickets(int size) {
        final Map<String, Integer> ticketHistory = AppSettings.getTicketHistory();
        String[] tickets;

        if (ticketHistory != null) {
            tickets = ticketHistory.keySet().toArray(new String[ticketHistory.size()]);

            Arrays.sort(tickets, new Comparator<String>() {
                @Override
                public int compare(String lhs, String rhs) {
                    return ticketHistory.get(lhs).compareTo(ticketHistory.get(rhs));
                }
            });
        } else {
            tickets = UserSettings.getCustomTickets(); // this only happens on update from 1.1.1 to 1.1.2
        }
        String[] topTickets = new String[Math.min(size, tickets.length)];
        System.arraycopy(tickets, 0, topTickets, 0, Math.min(size, tickets.length));

        if (ticketHistory == null) {
            final Map<String, Integer> newTicketHistory = new HashMap<>();

            for (String ticket : topTickets) {
                newTicketHistory.put(ticket, 1);
            }
            AppSettings.setTicketHistory(newTicketHistory);
        }

        return topTickets;
    }
}
