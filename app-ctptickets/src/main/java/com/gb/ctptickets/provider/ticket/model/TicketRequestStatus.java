package com.gb.ctptickets.provider.ticket.model;


import java.io.Serializable;

import android.app.Activity;


/**
 * Status of a request for a certain ticket. Does not refer to a single sent sms. Sent-, Delivered- and Received-
 * broadcast receivers will concurrently update this object. In case of multiple sms's sent, the status may become
 * inconsistent (it may switch to SENT for example even though a delivery broadcast already arrived).
 */
public class TicketRequestStatus implements Serializable {

	private final String	ticket;

	private final long	id;

	/**
	 * One of:<br>
	 * {@link android.app.Activity#RESULT_OK}<br>
	 * {@link android.telephony.SmsManager#RESULT_ERROR_GENERIC_FAILURE}<br>
	 * {@link android.telephony.SmsManager#RESULT_ERROR_NO_SERVICE}<br>
	 * {@link android.telephony.SmsManager#RESULT_ERROR_NULL_PDU}<br>
	 * {@link android.telephony.SmsManager#RESULT_ERROR_RADIO_OFF}<br>
	 * or 0 - unknown
	 */
	private int		smsSentStatus		= 0;

	/**
	 * One of:<br>
	 * {@link android.app.Activity#RESULT_OK}<br>
	 * {@link android.app.Activity#RESULT_CANCELED}<br>
	 * or -2 - unknown
	 */
	private int		smsDeliveredStatus	= -2;

	/**
	 * Exact time when the request becomes too old and should be deleted
	 */
	private long	requestTimeoutDate	= 0;

	/**
	 * 0 - no confirmation available <br>
	 */
	private long	expiryDate			= 0;


	public TicketRequestStatus(String ticket, long id, long requestTimeoutDate) {
		this.ticket = ticket;
		this.id = id;
		this.requestTimeoutDate = requestTimeoutDate;
	}


	public String getTicket() {
		return ticket;
	}


	public long getId() {
		return id;
	}


	/**
	 * One of:<br>
	 * {@link android.app.Activity#RESULT_OK}<br>
	 * {@link android.telephony.SmsManager#RESULT_ERROR_GENERIC_FAILURE}<br>
	 * {@link android.telephony.SmsManager#RESULT_ERROR_NO_SERVICE}<br>
	 * {@link android.telephony.SmsManager#RESULT_ERROR_NULL_PDU}<br>
	 * {@link android.telephony.SmsManager#RESULT_ERROR_RADIO_OFF}<br>
	 * or 0 - unknown
	 */
	public void setSmsSentStatus(int smsSentStatus) {
		this.smsSentStatus = smsSentStatus;
	}


	/**
	 * One of:<br>
	 * {@link android.app.Activity#RESULT_OK}<br>
	 * {@link android.app.Activity#RESULT_CANCELED}<br>
	 * or -2 - unknown
	 */
	public void setSmsDeliveredStatus(int smsDeliveredStatus) {
		this.smsDeliveredStatus = smsDeliveredStatus;
	}


	public long getExpirationDate() {
		return expiryDate;
	}


	public int getSmsSentStatus() {
		return smsSentStatus;
	}


	public int getSmsDeliveredStatus() {
		return smsDeliveredStatus;
	}


	public void setExpiryDate(long expiryDate) {
		this.expiryDate = expiryDate;
	}


	@Override
	public String toString() {
		return "TicketRequestStatus{" + "ticket='" + ticket + '\'' + ", id=" + id + ", smsSentStatus=" + smsSentStatus
				+ ", smsDeliveredStatus=" + smsDeliveredStatus + ", requestTimeoutDate=" + requestTimeoutDate
				+ ", expiryDate=" + expiryDate + ", sent=" + isSent() + ", delivered=" + isDelivered() + ", failed="
				+ isFailed() + ", pending=" + isPending() + ", valid=" + isValid() + ", requestTimedOut="
				+ isRequestTimedOut() + ", ticketExpired=" + isTicketExpired() + '}';
	}


	public boolean isFailed() {
		return (smsSentStatus != 0 && smsSentStatus != Activity.RESULT_OK)
				|| (smsDeliveredStatus != -2 && smsDeliveredStatus != Activity.RESULT_OK);
	}


	public boolean isSent() {
		return smsSentStatus == Activity.RESULT_OK;
	}


	public boolean isDelivered() {
		return smsDeliveredStatus == Activity.RESULT_OK;
	}


	/**
	 * @return true if the confirmation sms has arrived, we have a valid ticket with some expiration date that's in the
	 *         future
	 */
	public boolean isValid() {
		return expiryDate > 0 && System.currentTimeMillis() <= expiryDate;
	}


	/**
	 * @return true if we don't have a valid ticket and the waiting time for the confirmation sms has expired.
	 */
	public boolean isRequestTimedOut() {
		return expiryDate == 0 && System.currentTimeMillis() >= requestTimeoutDate;
	}


	/**
	 * @return true if we have a ticket, but the expiration date has expired
	 */
	public boolean isTicketExpired() {
		return expiryDate > 0 && System.currentTimeMillis() >= expiryDate;
	}


	/**
	 * @return true if the request has been sent, it's not yet expired and no confirmation has yet arrived
	 */
	public boolean isPending() {
		return expiryDate == 0 && System.currentTimeMillis() < requestTimeoutDate;
	}
}
