package com.gb.ctptickets.provider.currency.model;


import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Results {

	@SerializedName("rate")
	public Rate	rate;
}
