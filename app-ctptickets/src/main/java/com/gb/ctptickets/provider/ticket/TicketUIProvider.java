package com.gb.ctptickets.provider.ticket;


import com.gb.ctptickets.provider.ticket.model.TicketRate;


/**
 * Created by gbiro on 12/10/2014.
 */
public interface TicketUIProvider {

	String getTitle();


	String getShortTitle();


	String getPrice(TicketRate rate);
}
