package com.gb.ctptickets.provider.ticket;


import android.app.Activity;
import android.content.Context;
import android.telephony.SmsManager;
import android.util.Log;

import com.gb.ctptickets.App;
import com.gb.ctptickets.Constants;
import com.gb.ctptickets.R;
import com.gb.ctptickets.UserSettings;
import com.gb.ctptickets.provider.ticket.model.TicketRequestStatus;
import com.gb.ctptickets.ui.eventbus.TicketStatusChangedEvent;

import org.apache.commons.lang3.ArrayUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.ListIterator;

import de.greenrobot.event.EventBus;


/**
 * Handles the status of one ticket request. Automatically evicted when expired.
 *
 * Created by gbiro on 12/10/2014.
 */
public class DefaultTicketRequestStatusRegistry implements TicketRequestStatusRegistry {

	private static final String	TAG	= DefaultTicketRequestStatusRegistry.class.getName();

	private final String				ticket;


	public DefaultTicketRequestStatusRegistry(String ticket) {
		this.ticket = ticket;
	}


	@Override
	public boolean hasValidTicket() {
		TicketRequestStatus[] statuses = loadStatuses();

		if (statuses != null) {
			for (TicketRequestStatus status : statuses) {
				if (status.isValid()) {
					return true;
				}
			}
		}

		return false;
	}


	@Override
	public void createRequest(long requestTimeoutDate) {
		TicketRequestStatus[] statuses = loadStatuses();
		TicketRequestStatus status = new TicketRequestStatus(ticket, generateNextId(), requestTimeoutDate);
		Log.d(TAG, "Ticket request timeout date for " + ticket + " is " + new Date(requestTimeoutDate));
		statuses = ArrayUtils.add(statuses, status);
		UserSettings.setTicketRequestStatuses(ticket, statuses);
	}


	private long generateNextId() {
		TicketRequestStatus[] statuses = loadStatuses();
		return statuses != null ? statuses.length : 0;
	}


	@Override
	public long getIdForTicketWithClosestExpiration() {
		TicketRequestStatus[] statuses = loadStatuses();
		TicketRequestStatus result = null;

		for (TicketRequestStatus status : statuses) {
			if (result == null || status.getExpirationDate() < result.getExpirationDate()) {
				result = status;
			}
		}
		return result != null ? result.getId() : -1;
	}


	/**
	 * Checks all the existing requests that were made for this bus-line and returns one that's still pending.<br>
	 * The point of this method is that when the user sends two request sms's for the same bus-line and receives two
	 * confirmation sms's, there is no way to know which confirmation corresponds to which request. Which is fine
	 * because we don't need to know this correspondence, we only need to know whether we received 0, 1 or 2
	 * confirmations. We then associate them (in the request status registry) with any of the pending requests.
	 *
	 * @return unique request id or -1 if there is no request for the bus-line or there are no more pending requests
	 */
	private long getNextPendingRequestId() {
		TicketRequestStatus[] statuses = loadStatuses();

		for (TicketRequestStatus status : statuses) {
			if (status.isPending()) {
				return status.getId();
			}
		}
		return -1;
	}


	@Override
	public long getNextUnsentRequestId() {
		TicketRequestStatus[] statuses = loadStatuses();

		for (TicketRequestStatus status : statuses) {
			if (!status.isSent()) {
				return status.getId();
			}
		}
		return -1;
	}


	@Override
	public long getNextUndeliveredRequestId() {
		TicketRequestStatus[] statuses = loadStatuses();

		for (TicketRequestStatus status : statuses) {
			if (!status.isDelivered()) {
				return status.getId();
			}
		}
		return -1;
	}


	/**
	 * Fetch the request with that has the specified <code>expirationDate</code>
	 *
	 * @param expirationDate
	 *            date at which the ticket expires
	 * @return unique request id or -1 if there is no request for the specified <code>expirationDate</code>
	 */
	private long searchByExpirationDate(long expirationDate) {
		TicketRequestStatus[] statuses = loadStatuses();

		for (TicketRequestStatus status : statuses) {
			if (status.getExpirationDate() == expirationDate) {
				return status.getId();
			}
		}
		return -1;
	}


	public boolean isRequestFailed(long id) {
		return getStatus(id).isFailed();
	}


	public boolean isRequestSent(long id) {
		return getStatus(id).isSent();
	}


	public boolean isRequestDelivered(long id) {
		return getStatus(id).isDelivered();
	}


	public boolean isValid(long id) {
		return getStatus(id).isValid();
	}


	public long getExpiryDate(long id) {
		return getStatus(id).getExpirationDate();
	}


	public String getStatusDescription(long id) {
		TicketRequestStatus status = getStatus(id);

		String resultStr = null;
		Context context = App.getContext();

		if (status.getExpirationDate() > 0) {
			context.getString(R.string.ticket_available);
		} else if (status.getSmsSentStatus() > -2) {
			switch (status.getSmsSentStatus()) {
			case Activity.RESULT_OK:
				resultStr = context.getString(R.string.sms_sent);
				break;
			case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
				resultStr = context.getString(R.string.generic_failure);
				break;
			case SmsManager.RESULT_ERROR_NO_SERVICE:
				resultStr = context.getString(R.string.no_service);
				break;
			case SmsManager.RESULT_ERROR_NULL_PDU:
				resultStr = "Null PDU";
				break;
			case SmsManager.RESULT_ERROR_RADIO_OFF:
				resultStr = context.getString(R.string.radio_off);
				break;
			}
		} else if (status.getSmsDeliveredStatus() > -2) {
			switch (status.getSmsDeliveredStatus()) {
			case Activity.RESULT_OK:
				resultStr = context.getString(R.string.sms_delivered);
				break;
			case Activity.RESULT_CANCELED:
				resultStr = context.getString(R.string.sms_not_delivered);
				break;
			}
		}
		return resultStr;
	}


	/**
	 * One of:<br>
	 * {@link android.app.Activity#RESULT_OK}<br>
	 * {@link android.telephony.SmsManager#RESULT_ERROR_GENERIC_FAILURE} <br>
	 * {@link android.telephony.SmsManager#RESULT_ERROR_NO_SERVICE} <br>
	 * {@link android.telephony.SmsManager#RESULT_ERROR_NULL_PDU} <br>
	 * {@link android.telephony.SmsManager#RESULT_ERROR_RADIO_OFF} <br>
	 * or 0 - unknown
	 */
	public void setSmsSentStatus(final int smsSentStatus, long id) {
		new StatusUpdater(id, "Sent status for " + ticket + " is " + smsSentStatus + " (-1 is ok)") {

			@Override
			public void updateStatus(TicketRequestStatus status) {
				status.setSmsSentStatus(smsSentStatus);
			}
		}.run();
	}


	/**
	 * One of:<br>
	 * {@link android.app.Activity#RESULT_OK}<br>
	 * {@link android.app.Activity#RESULT_CANCELED}<br>
	 * or -2 - unknown
	 */
	public void setSmsDeliveredStatus(final int smsDeliveredStatus, long id) {
		new StatusUpdater(id, "Delivery status for " + ticket + " is " + smsDeliveredStatus + " (-1 is ok)") {

			@Override
			public void updateStatus(TicketRequestStatus status) {
				status.setSmsDeliveredStatus(smsDeliveredStatus);
			}
		}.run();
	}


	/**
	 * Set the expiration date for the specified ticket (fetched from the incoming sms)
	 *
	 * @param expiryDate
	 * @param id
	 *            Unique id of the request. Generated by the app. Use {@link #getIdForTicketWithClosestExpiration()}
	 */
	private void setExpirationDate(final long expiryDate, long id) {
		new StatusUpdater(id, "Ticket expiry date for " + ticket + " is " + new Date(expiryDate)) {

			@Override
			public void updateStatus(TicketRequestStatus status) {
				status.setExpiryDate(expiryDate);
			}
		}.run();
	}


	@Override
	public void processIncomingTicket(String ticket, long expirationDate, double cost, String confirmationCode) {
		long id = getNextPendingRequestId();

		// Case 1: There are no pending requests or valid tickets with the same expiration date as the current sms - we need to create a new request
		// Case 2: There are pending requests - we take the first and give it an expiration date, turning it into a valid ticket

		if (id < 0) {
			id = searchByExpirationDate(expirationDate);

			if (id < 0) {
				// Case 1
				createRequest(Long.MAX_VALUE);
				id = getNextPendingRequestId();
			}
		}

		if (id >= 0) {
			setExpirationDate(expirationDate, id);
		}
	}

	private abstract class StatusUpdater implements Runnable {

		private final long	id;
		private final String	message;


		StatusUpdater(long id, String message) {
			this.id = id;
			this.message = message;
		}


		@Override
		public void run() {
			TicketRequestStatus status = getStatus(id);

			if (status != null) {
				updateStatus(status);
				UserSettings.setTicketRequestStatus(status);
				EventBus.getDefault().post(new TicketStatusChangedEvent(ticket));
			}
			Log.d(TAG, message);
		}


		public abstract void updateStatus(TicketRequestStatus status);
	}


	private TicketRequestStatus getStatus(long id) {
		TicketRequestStatus[] statuses = loadStatuses();
		TicketRequestStatus result = null;

		for (TicketRequestStatus status : statuses) {
			if (status.getId() == id) {
				result = status;
			}
		}
		return result;
	}


	private TicketRequestStatus[] loadStatuses() {
		TicketRequestStatus[] statuses = UserSettings.getTicketRequestStatus(ticket);
		boolean evicted = false;

		if (statuses != null) {
            ArrayList<TicketRequestStatus> statusList = new ArrayList<TicketRequestStatus>(Arrays.asList(statuses));

			for (ListIterator<TicketRequestStatus> i = statusList.listIterator(); i.hasNext();) {
				if (shouldStatusBeEvicted(i.next())) {
					i.remove();
					evicted = true;
					Log.d(TAG, "Request evicted: " + ticket + " (" + i + ")");
				}
			}
			if (evicted) {
				statuses = statusList.toArray(new TicketRequestStatus[statusList.size()]);
				// save the cleaned up list
				UserSettings.setTicketRequestStatuses(ticket, statuses);

				if (Constants.UPDATE_TICKET_STATUS_ICON) {
					EventBus.getDefault().post(new TicketStatusChangedEvent(ticket));
				}
			}
		} else {
			statuses = new TicketRequestStatus[0];
		}
		return statuses;
	}


	private boolean shouldStatusBeEvicted(TicketRequestStatus status) {
		return status.isTicketExpired() || status.isRequestTimedOut();
	}
}
