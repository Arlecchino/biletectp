package com.gb.ctptickets;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;

import com.gb.ctptickets.util.PrefsUtil;


/**
 * Used by the application rather than the user
 * <p/>
 * Created by gbiro on 11/11/2014.
 */
public class AppSettings {

    private static final String PREF_LAST_SYNC_DATE = "PREF_LAST_SYNC_DATE";
    private static final String PREF_PDF_URI_MAP = "PREF_PDF_URI_MAP";
    public static final String PREF_HIDDEN_BUS_LINES = "PREF_HIDDEN_BUS_LINES";
    public static final String PREF_NEW_BUS_LINES = "PREF_NEW_BUS_LINES";
    private static final String PREF_APPWIDGET_TICKET = "PREF_APPWIDGET_TICKET";
    private static final String PREF_LAST_VALIDATED_NETWORK_OPERATOR = "PREF_LAST_VALIDATED_NETWORK_OPERATOR";
    public static final String PREF_TICKET_HISTORY = "PREF_TICKET_HISTORY";


    public static long getLastSyncDate() {
        return PrefsUtil.get(PREF_LAST_SYNC_DATE, 0L);
    }


    public static void setLastSyncDate(long date) {
        PrefsUtil.put(PREF_LAST_SYNC_DATE, date);
    }


    public static Map<String, String> getPdfUriMap() {
        //noinspection unchecked
        return PrefsUtil.get(PREF_PDF_URI_MAP, (HashMap) null);
    }


    public static void setPdfUriMap(Map<String, String> map) {
        PrefsUtil.put(PREF_PDF_URI_MAP, map);
    }


    /**
     * Fetch bus lines that were removed by RATUC
     *
     * @see com.gb.ctptickets.Constants#DEFAULT_BUS_LINES
     */
    public static String[] getHiddenBusLines() {
        return PrefsUtil.get(PREF_HIDDEN_BUS_LINES, new String[0]);
    }


    /**
     * Save bus lines that were removed by RATUC
     *
     * @see com.gb.ctptickets.Constants#DEFAULT_BUS_LINES
     */
    public static void setHiddenBusLines(String[] hiddenBusLines) {
        PrefsUtil.put(PREF_HIDDEN_BUS_LINES, hiddenBusLines);
    }


    /**
     * Fetch bus lines that were added by RATUC
     *
     * @see com.gb.ctptickets.Constants#DEFAULT_BUS_LINES
     */
    public static String[] getNewBusLines() {
        return PrefsUtil.get(PREF_NEW_BUS_LINES, new String[0]);
    }


    /**
     * Save bus lines that were added by RATUC
     *
     * @see com.gb.ctptickets.Constants#DEFAULT_BUS_LINES
     */
    public static void setNewBusLines(String[] newBusLines) {
        PrefsUtil.put(PREF_NEW_BUS_LINES, newBusLines);
    }


    public static void setAppWidgetTicket(int appWidgetId, String ticket) {
        //noinspection unchecked
        Map<Integer, String> appWidgetTicketsMap = PrefsUtil.get(PREF_APPWIDGET_TICKET, (Map) null);

        if (appWidgetTicketsMap == null) {
            appWidgetTicketsMap = new HashMap<>();
        }
        appWidgetTicketsMap.put(appWidgetId, ticket);
        PrefsUtil.put(PREF_APPWIDGET_TICKET, appWidgetTicketsMap);
    }


    public static String getAppWidgetTicket(int appWidgetId) {
        //noinspection unchecked
        Map<Integer, String> appWidgetTicketMap = PrefsUtil.get(PREF_APPWIDGET_TICKET, (Map) null);
        return appWidgetTicketMap != null ? appWidgetTicketMap.get(appWidgetId) : null;
    }


    public static void removeAppWidgetTicket(int appWidgeTicket) {
        //noinspection unchecked
        Map<Integer, String> appWidgetTicketMap = PrefsUtil.get(PREF_APPWIDGET_TICKET, (Map) null);

        if (appWidgetTicketMap != null) {
            appWidgetTicketMap.remove(appWidgeTicket);
            PrefsUtil.put(PREF_APPWIDGET_TICKET, appWidgetTicketMap);
        }
    }


    public static int[] getAppWidgetsForTicket(String ticket) {
        //noinspection unchecked
        Map<Integer, String> appWidgetTicketMap = PrefsUtil.get(PREF_APPWIDGET_TICKET, (Map) null);
        ArrayList<Integer> ids = new ArrayList<>();

        if (appWidgetTicketMap != null) {
            for (Map.Entry<Integer, String> entry : appWidgetTicketMap.entrySet()) {
                if (entry.getValue().equals(ticket)) {
                    ids.add(entry.getKey());
                }
            }
        }
        return ArrayUtils.toPrimitive(ids.toArray(new Integer[ids.size()]));
    }


    /**
     * Fetch the network operator for which a warning was last shown. Null if no warning was yet shown.
     */
    public static String getLastValidatedNetworkOperator() {
        return PrefsUtil.get(PREF_LAST_VALIDATED_NETWORK_OPERATOR, (String) null);
    }

    /**
     * Set the network operator for which a warning was just shown
     */
    public static void setLastValidatedNetworkOperator(String networkOperatorName) {
        PrefsUtil.put(PREF_LAST_VALIDATED_NETWORK_OPERATOR, networkOperatorName);
    }

    public static Map<String, Integer> getTicketHistory() {
        //noinspection unchecked
        return PrefsUtil.get(PREF_TICKET_HISTORY, (HashMap) null);
    }


    public static void setTicketHistory(Map<String, Integer> map) {
        PrefsUtil.put(PREF_TICKET_HISTORY, map);
    }
}
