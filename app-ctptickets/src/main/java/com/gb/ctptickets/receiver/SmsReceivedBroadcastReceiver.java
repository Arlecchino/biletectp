package com.gb.ctptickets.receiver;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

import com.gb.ctptickets.Constants;
import com.gb.ctptickets.provider.sms.TicketSmsParser;
import com.gb.ctptickets.provider.sms.model.TicketSmsInfo;
import com.gb.ctptickets.util.GoogleAnalyticsHelper;
import com.gb.ctptickets.util.NotificationBuilder;


/**
 * Broadcast receiver for the {@link Telephony.Sms.Intents.SMS_RECEIVED_ACTION} intent
 * 
 * Created by GABOR on 2014-szept.-11.
 */
public class SmsReceivedBroadcastReceiver extends BroadcastReceiver {

	private SmsReceivedListener	smsReceivedListener;


	public void setSmsReceivedListener(SmsReceivedListener smsReceivedListener) {
		this.smsReceivedListener = smsReceivedListener;
	}


	@Override
	public void onReceive(Context context, Intent intent) {
		Bundle bundle = intent.getExtras();
		try {
			if (bundle != null && smsReceivedListener != null) {
				Object[] pdusObj = (Object[]) bundle.get("pdus");

				for (int i = 0; i < pdusObj.length; i++) {
					SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
					String phoneNumber = currentMessage.getDisplayOriginatingAddress();
					String message = currentMessage.getDisplayMessageBody();
					long date = currentMessage.getTimestampMillis();

					if (phoneNumber.endsWith(Constants.TICKET_BUY_PHONE_NUMBER)) {
						// message = "Biletul pentru 1 CALATORIE pe linia 100 a fost activat. Valabil pana la 12:52 in 15/09/2014. Cost total:0,55 eur+TVA. Cod confirmare: OYVHJ";
						TicketSmsInfo validTicket = new TicketSmsParser(message, date).parse();

						if (validTicket != null) {
							smsReceivedListener.onTicketSmsAvailable(new TicketSmsInfo[]{
                                    validTicket
                            });
							new NotificationBuilder(validTicket.getTicket()).hide();
							GoogleAnalyticsHelper.onTicketReceived(validTicket.getTicket());
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public interface SmsReceivedListener {

		void onTicketSmsAvailable(TicketSmsInfo[] tickets);
	}
}
