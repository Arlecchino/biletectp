package com.gb.ctptickets.receiver;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.gb.ctptickets.provider.ticket.TicketBuyer;
import com.gb.ctptickets.provider.ticket.TicketHandlerFactory;
import com.gb.ctptickets.provider.ticket.TicketRequestStatusRegistry;
import com.gb.ctptickets.util.GoogleAnalyticsHelper;


/**
 * Broadcast receiver for the com.gb.ctptickets.SMS_DELIVERED intent
 * 
 * Created by GABOR on 2014-jún.-22.
 */
public class SmsDeliveredBroadcastReceiver extends BroadcastReceiver {

	public static final String	INTENT_DELIVERED	= "com.gb.ctptickets.SMS_DELIVERED";


	@Override
	public void onReceive(Context context, Intent intent) {
		String ticket = intent.getStringExtra(TicketBuyer.EXTRA_TICKET);

		if (!TextUtils.isEmpty(ticket)) {
			TicketRequestStatusRegistry registry = TicketHandlerFactory.getRequestStatusRegistry(ticket);
			long id = registry.getNextUndeliveredRequestId();

			if (id >= 0) {
				registry.setSmsDeliveredStatus(getResultCode(), id);
				String status = registry.getStatusDescription(id);
				GoogleAnalyticsHelper.onOrderStatus(ticket, status);
			}
		}
	}
}
