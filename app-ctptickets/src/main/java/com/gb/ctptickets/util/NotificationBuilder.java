package com.gb.ctptickets.util;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.gb.ctptickets.App;
import com.gb.ctptickets.R;
import com.gb.ctptickets.UserSettings;
import com.gb.ctptickets.provider.ticket.TicketHandlerFactory;
import com.gb.ctptickets.provider.ticket.TicketUIProvider;
import com.gb.ctptickets.ui.activity.NotificationDummyActivity;


/**
 * Utilities for android notifications
 * 
 * Created by GABOR on 2014-szept.-13.
 */
public class NotificationBuilder {

	private final String				ticket;
	private final TicketUIProvider	uiProvider;


	public NotificationBuilder(String ticket) {
		this.ticket = ticket;
        this.uiProvider = TicketHandlerFactory.getUIProvider(ticket);
	}


	public void show() {
		Context context = App.getContext();
		String body = context.getString(R.string.ticket_late_notif_body, uiProvider.getTitle());
		Intent resultIntent = new Intent(context, NotificationDummyActivity.class);
		PendingIntent resultPendingIntent = PendingIntent.getActivity(context, 0, resultIntent,
				PendingIntent.FLAG_UPDATE_CURRENT);
		Uri alarmUri = UserSettings.getNotificationRingtoneUri();
		long[] alarmPattern = new long[] {
				1000, 400, 1000
		};
		NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
				.setSmallIcon(R.drawable.ic_notif_warning).setAutoCancel(true).setContentInfo(body)
				.setContentIntent(resultPendingIntent).setTicker(context.getString(R.string.ticket_late_notif_title))
				.setSound(alarmUri).setVibrate(alarmPattern).setLights(Color.RED, 500, 500)
				.setPriority(Integer.MAX_VALUE).setContentTitle(context.getString(R.string.ticket_late_notif_title))
				.setStyle(new NotificationCompat.BigTextStyle().bigText(body)).setContentText(uiProvider.getTitle());

		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(ticket.hashCode(), builder.build());
	}


	public void hide() {
		Context context = App.getContext();
		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.cancel(ticket.hashCode());
	}
}
