package com.gb.ctptickets.util;


import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import com.gb.ctptickets.App;
import com.gb.ctptickets.R;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;


/**
 * Sends usage statistics to google play console
 *
 * @see https://www.google.com/analytics/web/#report/app-overview/a55446447w88624787p92069503/
 * <p/>
 * Created by gbiro on 10/6/2014.
 */
public class GoogleAnalyticsHelper {

    private final static String KEY_TICKET = "ticket";
    private final static String KEY_TICKET_STATUS = "status";
    private final static String KEY_TICKET_TIMEOUT = "timeout";
    private final static String KEY_SETTINGS_KEY = "settings_key";
    private final static String KEY_SETTINGS_VALUE = "settings_value";
    private final static String KEY_VERSION = "version";

    private final static String CATEGORY_UI = "ui";
    private final static String CATEGORY_SMS = "sms";
    private final static String CATEGORY_SETTINGS = "settings";
    private final static String CATEGORY_WIDGET = "widget";
    private final static String CATEGORY_TICKET_UI = "ticket_ui";
    private final static String CATEGORY_SYNC = "sync";

    private static Tracker tracker;


    private static synchronized Tracker getTracker() {
        if (tracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(App.getContext());
            tracker = analytics.newTracker(R.xml.app_tracker);
            //			analytics.getLogger().setLogLevel(Logger.LogLevel.VERBOSE);
        }
        return tracker;
    }


    public static void onSettingChanged(String prefKey, String value) {
        Map<String, String> data = new HashMap<>();
        data.put(KEY_SETTINGS_KEY, prefKey);
        data.put(KEY_SETTINGS_VALUE, value);
        onEvent(CATEGORY_SETTINGS, "setting changed", data);
    }


    public static void onWidgetCreated(String ticket) {
        onWidgetEvent("widget created", ticket);
    }


    public static void onWidgetClicked(String ticket) {
        onWidgetEvent("widget clicked", ticket);
    }


    public static void onWidgetDeleted(String ticket) {
        onWidgetEvent("widget deleted", ticket);
    }


    private static void onWidgetEvent(String action, String ticket) {
        Map<String, String> data = new HashMap<>();
        data.put(KEY_TICKET, ticket);
        onEvent(CATEGORY_WIDGET, action, data);
    }


    public static void onSmsSent(String ticket) {
        onOrderStatus(ticket, "requested");
    }


    public static void onTicketReceived(String ticket) {
        onOrderStatus(ticket, "received");
    }


    public static void onTicketLate(String ticket, int timeout) {
        Map<String, String> data = new HashMap<>();
        data.put(KEY_TICKET_TIMEOUT, Integer.toString(timeout));
        onOrderStatus(ticket, "late", data);
    }


    public static void onOrderStatus(String ticket, String status) {
        onOrderStatus(ticket, status, null);
    }


    private static void onOrderStatus(String ticket, String status, Map<String, String> data) {
        if (data == null) {
            data = new HashMap<>();
        }
        data.put(KEY_TICKET, ticket);
        data.put(KEY_TICKET_STATUS, status);
        onEvent(CATEGORY_SMS, "order status changed", data);
    }


    public static void onCustomTicketAdded(String ticket) {
        onTicketUIEvent("custom ticket added", ticket);
    }


    public static void onCustomLineAdded(String line) {
        onTicketUIEvent("custom line added", line);
    }


    private static void onTicketUIEvent(String action, String ticket) {
        Map<String, String> data = new HashMap<>();
        data.put(KEY_TICKET, ticket);
        onEvent(CATEGORY_TICKET_UI, action, data);
    }


    public static void onAddButtonPressed() {
        onGenericUIEvent("add button pressed");
    }


    private static void onGenericUIEvent(@SuppressWarnings("SameParameterValue") String action) {
        onEvent(CATEGORY_UI, action, null);
    }

    public static void onSyncError(@SuppressWarnings("SameParameterValue") String action) {
        onEvent(CATEGORY_SYNC, action, null);
    }

    private static void onEvent(String category, String action, Map<String, String> data) {
        if (data == null) {
            data = new HashMap<>();
        }

        data.put(KEY_VERSION, App.getVersion());
        String label = new JSONObject(data).toString();
        Map<String, String> params = new HitBuilders.EventBuilder().setCategory(category).setAction(action)
                .setLabel(label).build();
        getTracker().send(params);
        GoogleAnalytics.getInstance(App.getContext()).dispatchLocalHits();
    }
}
