package com.gb.ctptickets.util;


import static com.nineoldandroids.view.ViewPropertyAnimator.animate;

import android.view.View;
import android.view.animation.Animation;

import com.gb.ctptickets.R;


/**
 * Utilities for animations
 * 
 * Created by gbiro on 7/10/2014.
 */
public class Animator {

	private final View	view;


	public Animator(View view) {
		this.view = view;
	}


	public void hide(com.nineoldandroids.animation.Animator.AnimatorListener listener) {
		animate(view).scaleXBy(-1.0f).scaleYBy(-1.0f).alpha(0).setDuration(400).setListener(listener);
	}


	public void shake() {
		Animation shakeAnimation = android.view.animation.AnimationUtils.loadAnimation(view.getContext(), R.anim.shake);
		view.startAnimation(shakeAnimation);
	}
}
