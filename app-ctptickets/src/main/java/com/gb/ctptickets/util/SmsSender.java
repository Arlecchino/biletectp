package com.gb.ctptickets.util;


import java.util.ArrayList;

import android.app.PendingIntent;
import android.content.Intent;
import android.telephony.SmsManager;

import com.gb.ctptickets.App;


/**
 * This class is responsible for sending sms's
 * <p/>
 * Created by gbiro on 11/18/2014.
 */
public class SmsSender {

    private final String number;
    private final String message;
    private final Intent intentSent;
    private final Intent deliveredIntent;

    public SmsSender(@SuppressWarnings("SameParameterValue") String number, String message, Intent intentSent, Intent deliveredIntent) {
        this.number = number;
        this.message = message;
        this.intentSent = intentSent;
        this.deliveredIntent = deliveredIntent;
    }

    public void send() {
        SmsManager smsManager = SmsManager.getDefault();

        SmsManager sms = SmsManager.getDefault();
        ArrayList<String> parts = sms.divideMessage(message);
        int messageCount = parts.size();

        PendingIntent sentPI = PendingIntent.getBroadcast(App.getContext(), 0, intentSent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent deliveredPI = PendingIntent.getBroadcast(App.getContext(), 0, deliveredIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        if (messageCount > 1) {
            ArrayList<PendingIntent> deliveryIntents = new ArrayList<PendingIntent>();
            ArrayList<PendingIntent> sentIntents = new ArrayList<PendingIntent>();

            for (int j = 0; j < messageCount; j++) {
                sentIntents.add(sentPI);
                deliveryIntents.add(deliveredPI);
            }
            sentIntents.trimToSize();
            deliveryIntents.trimToSize();
            smsManager.sendMultipartTextMessage(number, null, parts, sentIntents, deliveryIntents);
        } else {
            smsManager.sendTextMessage(number, null, message, sentPI, deliveredPI);
        }
    }
}
