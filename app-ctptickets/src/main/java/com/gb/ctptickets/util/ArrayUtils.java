package com.gb.ctptickets.util;

import java.lang.reflect.Array;

/**
 * Arrays related utility class
 *
 * Created by gbiro on 4/29/2015.
 */
public class ArrayUtils {

    /**
     * Clean an array from null values
     *
     * @param clazz array item type
     * @param array the array to clean
     * @param <T>   array item type
     * @return array of same type, but without the null's
     */
    public static <T> T[] deleteNulls(Class<T> clazz, T[] array) {
        int nonNullCount = 0;

        for (int i = 0; i < array.length; i++) {
            if (array[i] != null) {
                nonNullCount++;
            }
        }
        //noinspection unchecked
        T[] cleanCopy = (T[]) Array.newInstance(clazz, nonNullCount);
        int index = 0;

        for (int i = 0; i < nonNullCount; i++) {
            while (array[index] == null) index++;
            cleanCopy[i] = array[index];
        }
        return cleanCopy;
    }
}
