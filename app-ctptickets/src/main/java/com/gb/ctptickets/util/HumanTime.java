/*
 * HumanTime.java
 * 
 * Created on 06.10.2008
 * 
 * Copyright (c) 2008 Johann Burkard (<mailto:jb@eaio.com>) <http://eaio.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
 * Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 */
package com.gb.ctptickets.util;


import android.content.res.Resources;

import com.gb.ctptickets.App;
import com.gb.ctptickets.R;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;


@SuppressWarnings({"SameParameterValue", "WeakerAccess", "unused"})
public class HumanTime implements Externalizable, Comparable<HumanTime>, Cloneable {

	public enum Time {

		/**
		 * One milisecond.
		 */
		MILISEC(1, R.plurals.milisec, R.string.milisec_abbr),

		/**
		 * One second.
		 */
		SECOND(MILISEC.valueInMilisecs * 1000, R.plurals.second, R.string.second_abbr),

		/**
		 * One minute.
		 */
		MINUTE(SECOND.valueInMilisecs * 60, R.plurals.minute, R.string.minute_abbr),

		/**
		 * One hour.
		 */
		HOUR(MINUTE.valueInMilisecs * 60, R.plurals.hour, R.string.hour_abbr),

		/**
		 * One day.
		 */
		DAY(HOUR.valueInMilisecs * 24, R.plurals.day, R.string.day_abbr),

		/**
		 * One year.
		 */
		YEAR(DAY.valueInMilisecs * 365, R.plurals.year, R.string.year_abbr);

		Time(int valueInMilisecs, int compactStrRes, int strRes) {
			this.valueInMilisecs = valueInMilisecs;
			this.compactStrRes = compactStrRes;
			this.strRes = strRes;
		}

		public final int	valueInMilisecs;
		private final int	compactStrRes;
		private final int	strRes;


		public String toString(boolean compact, int value) {
			Resources res = App.getContext().getResources();
			return compact ? res.getString(strRes) : res.getQuantityString(compactStrRes, value);
		}
	}

	/**
	 * Percentage of what is round up or down.
	 */
	private static final int	CEILING_PERCENTAGE	= 1;

	private boolean				compact;

	/**
	 * The time delta.
	 */
	private long				delta;


	/**
	 * No-argument Constructor for HumanTime.
	 * <p/>
	 * Equivalent to calling <code>new HumanTime(0L)</code>.
	 */
	public HumanTime() {
		this(0L);
	}


	/**
	 * Constructor for HumanTime.
	 *
	 * @param delta
	 *            the initial time delta, interpreted as a positive number
	 */
	public HumanTime(long delta) {
		this.delta = Math.abs(delta);
	}


	/**
	 * setting this true will omit the space after the number and before the unit. For example, "1 m 2 s 3 ms" will be
	 * shown as "1m 2s 3ms"
	 */
	public HumanTime setCompact(boolean compact) {
		this.compact = compact;
		return this;
	}


	private long upperCeiling(long x) {
		return (x / 100) * (100 - CEILING_PERCENTAGE);
	}


	private long lowerCeiling(long x) {
		return (x / 100) * CEILING_PERCENTAGE;
	}


	private int ceil(long d, long n) {
		return (int) Math.ceil((double) d / n);
	}


	private int floor(long d, long n) {
		return (int) Math.floor((double) d / n);
	}


	/**
	 * Adds one year to the time delta.
	 *
	 * @return this HumanTime object
	 */
	public HumanTime y() {
		return y(1);
	}


	/**
	 * Adds n years to the time delta.
	 *
	 * @param n
	 *            n
	 * @return this HumanTime object
	 */
	HumanTime y(int n) {
		delta += Time.YEAR.valueInMilisecs * Math.abs(n);
		return this;
	}


	/**
	 * Adds one day to the time delta.
	 *
	 * @return this HumanTime object
	 */
	public HumanTime d() {
		return d(1);
	}


	/**
	 * Adds n days to the time delta.
	 *
	 * @param n
	 *            n
	 * @return this HumanTime object
	 */
	HumanTime d(int n) {
		delta += Time.DAY.valueInMilisecs * Math.abs(n);
		return this;
	}


	/**
	 * Adds one hour to the time delta.
	 *
	 * @return this HumanTime object
	 */
	public HumanTime h() {
		return h(1);
	}


	/**
	 * Adds n hours to the time delta.
	 *
	 * @param n
	 *            n
	 * @return this HumanTime object
	 */
	HumanTime h(int n) {
		delta += Time.HOUR.valueInMilisecs * Math.abs(n);
		return this;
	}


	/**
	 * Adds one month to the time delta.
	 *
	 * @return this HumanTime object
	 */
	public HumanTime m() {
		return m(1);
	}


	/**
	 * Adds n months to the time delta.
	 *
	 * @param n
	 *            n
	 * @return this HumanTime object
	 */
	HumanTime m(int n) {
		delta += Time.MINUTE.valueInMilisecs * Math.abs(n);
		return this;
	}


	/**
	 * Adds one second to the time delta.
	 *
	 * @return this HumanTime object
	 */
	public HumanTime s() {
		return s(1);
	}


	/**
	 * Adds n seconds to the time delta.
	 *
	 * @param n
	 *            seconds
	 * @return this HumanTime object
	 */
	HumanTime s(int n) {
		delta += Time.SECOND.valueInMilisecs * Math.abs(n);
		return this;
	}


	/**
	 * Adds one millisecond to the time delta.
	 *
	 * @return this HumanTime object
	 */
	public HumanTime ms() {
		return ms(1);
	}


	/**
	 * Adds n milliseconds to the time delta.
	 *
	 * @param n
	 *            n
	 * @return this HumanTime object
	 */
	HumanTime ms(int n) {
		delta += Math.abs(n);
		return this;
	}


	/**
	 * Returns a human-formatted representation of the time delta.
	 *
	 * @return a formatted representation of the time delta, never <code>null</code>
	 */
	String getExactly() {
		return getExactly(new StringBuilder()).toString();
	}


	/**
	 * Appends a human-formatted representation of the time delta to the given {@link Appendable} object.
	 *
	 * @param a
	 *            the StringBuilder object, may not be <code>null</code>
	 * @return the given StringBuilder object, never <code>null</code>
	 */
	StringBuilder getExactly(StringBuilder a) {
		boolean prependBlank = false;
		long d = delta;
		if (d >= Time.YEAR.valueInMilisecs) {
			int year = floor(d, Time.YEAR.valueInMilisecs);
			append(a, Time.YEAR, year);
			prependBlank = true;
		}
		d %= Time.YEAR.valueInMilisecs;
		if (d >= Time.DAY.valueInMilisecs) {
			if (prependBlank) {
				a.append(' ');
			}
			int day = floor(d, Time.DAY.valueInMilisecs);
			append(a, Time.DAY, day);
			prependBlank = true;
		}
		d %= Time.DAY.valueInMilisecs;
		if (d >= Time.HOUR.valueInMilisecs) {
			if (prependBlank) {
				a.append(' ');
			}
			int hour = floor(d, Time.HOUR.valueInMilisecs);
			append(a, Time.HOUR, hour);
			prependBlank = true;
		}
		d %= Time.HOUR.valueInMilisecs;
		if (d >= Time.MINUTE.valueInMilisecs) {
			if (prependBlank) {
				a.append(' ');
			}
			int minute = floor(d, Time.MINUTE.valueInMilisecs);
			a.append(minute);
			append(a, Time.MINUTE, minute);
			prependBlank = true;
		}
		d %= Time.MINUTE.valueInMilisecs;
		if (d >= Time.SECOND.valueInMilisecs) {
			if (prependBlank) {
				a.append(' ');
			}
			int second = floor(d, Time.SECOND.valueInMilisecs);
			append(a, Time.SECOND, second);
			prependBlank = true;
		}
		d %= Time.SECOND.valueInMilisecs;
		if (d > 0) {
			if (prependBlank) {
				a.append(' ');
			}
			int milisec = (int) d;
			append(a, Time.MILISEC, milisec);
		}
		return a;
	}


	/**
	 * Returns an approximate, human-formatted representation of the time delta.
	 *
	 * @return a formatted representation of the time delta, never <code>null</code>
	 */
	public String getApproximately(int elementCount) {
		return getApproximately(new StringBuilder(), elementCount).toString();
	}


	/**
	 * Appends an approximate, human-formatted representation of the time delta to the given {@link Appendable} object.
	 *
	 * @param a
	 *            the StringBuilder object, may not be <code>null</code>
	 * @return the given StringBuilder object, never <code>null</code>
	 */
	StringBuilder getApproximately(StringBuilder a, int elementCount) {
		int parts = 0;
		boolean rounded = false;
		boolean prependBlank = false;
		long d = delta;
		long mod = d % Time.YEAR.valueInMilisecs;

		int year = Integer.MIN_VALUE;

		if (mod >= upperCeiling(Time.YEAR.valueInMilisecs)) {
			year = ceil(d, Time.YEAR.valueInMilisecs);
			rounded = true;
		} else if (d >= Time.YEAR.valueInMilisecs) {
			year = floor(d, Time.YEAR.valueInMilisecs);
			rounded = mod <= lowerCeiling(Time.YEAR.valueInMilisecs);
		}
		if (year > Integer.MIN_VALUE) {
			append(a, Time.YEAR, year);
			++parts;
			prependBlank = true;
		}

		if (!rounded && parts < elementCount) {
			d %= Time.YEAR.valueInMilisecs;
			mod = d % Time.DAY.valueInMilisecs;
			int day = Integer.MIN_VALUE;

			if (mod >= upperCeiling(Time.DAY.valueInMilisecs)) {
				day = ceil(d, Time.DAY.valueInMilisecs);
				rounded = true;
			} else if (d >= Time.DAY.valueInMilisecs) {
				day = floor(d, Time.DAY.valueInMilisecs);
				rounded = mod <= lowerCeiling(Time.DAY.valueInMilisecs);
			}
			if (day > Integer.MIN_VALUE) {
				if (prependBlank) {
					a.append(' ');
				}
				append(a, Time.DAY, day);
				++parts;
				prependBlank = true;
			}

			if (parts < elementCount) {
				d %= Time.DAY.valueInMilisecs;
				mod = d % Time.HOUR.valueInMilisecs;
				int hour = Integer.MIN_VALUE;

				if (mod >= upperCeiling(Time.HOUR.valueInMilisecs)) {
					hour = ceil(d, Time.HOUR.valueInMilisecs);
					rounded = true;
				} else if (d >= Time.HOUR.valueInMilisecs && !rounded) {
					hour = floor(d, Time.HOUR.valueInMilisecs);
					rounded = mod <= lowerCeiling(Time.HOUR.valueInMilisecs);
				}
				if (hour > Integer.MIN_VALUE) {
					if (prependBlank) {
						a.append(' ');
					}
					append(a, Time.HOUR, hour);
					++parts;
					prependBlank = true;
				}

				if (parts < elementCount) {
					d %= Time.HOUR.valueInMilisecs;
					mod = d % Time.MINUTE.valueInMilisecs;
					int minute = Integer.MIN_VALUE;

					if (mod >= upperCeiling(Time.MINUTE.valueInMilisecs)) {
						minute = ceil(d, Time.MINUTE.valueInMilisecs);
						rounded = true;
					} else if (d >= Time.MINUTE.valueInMilisecs && !rounded) {
						minute = floor(d, Time.MINUTE.valueInMilisecs);
						rounded = mod <= lowerCeiling(Time.MINUTE.valueInMilisecs);
					}
					if (minute > Integer.MIN_VALUE) {
						if (prependBlank) {
							a.append(' ');
						}
						append(a, Time.MINUTE, minute);
						++parts;
						prependBlank = true;
					}

					if (parts < elementCount) {
						d %= Time.MINUTE.valueInMilisecs;
						mod = d % Time.SECOND.valueInMilisecs;
						int seconds = Integer.MIN_VALUE;

						if (mod >= upperCeiling(Time.SECOND.valueInMilisecs)) {
							seconds = ceil(d, Time.SECOND.valueInMilisecs);
							rounded = true;
						} else if (d >= Time.SECOND.valueInMilisecs && !rounded) {
							seconds = floor(d, Time.SECOND.valueInMilisecs);
							rounded = mod <= lowerCeiling(Time.SECOND.valueInMilisecs);
						}
						if (seconds > Integer.MIN_VALUE) {
							if (prependBlank) {
								a.append(' ');
							}
							append(a, Time.SECOND, seconds);
							++parts;
							prependBlank = true;
						}

						if (parts < elementCount) {
							d %= Time.SECOND.valueInMilisecs;
							int milisecs = Integer.MIN_VALUE;

							if (d > 0 && !rounded) {
								milisecs = (int) d;
							}
							if (milisecs > Integer.MIN_VALUE) {
								if (prependBlank) {
									a.append(' ');
								}
								append(a, Time.MILISEC, milisecs);
							}
						}
					}
				}
			}
		}
		return a;
	}


	private void append(StringBuilder a, Time time, int value) {
		a.append(value);
		if (!compact) {
			a.append(' ');
		}
		String append = time.toString(compact, value);
		a.append(append);
	}


	// Comparable

	@Override
	public int compareTo(HumanTime t) {
		return delta == t.delta ? 0 : (delta < t.delta ? -1 : 1);
	}


	// Externalizable

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeLong(delta);
	}


	@Override
	public void readExternal(ObjectInput in) throws IOException {
		delta = in.readLong();
	}


	@Override
	public int hashCode() {
		return (int) (delta ^ (delta >> 32));
	}


	@Override
	public boolean equals(Object obj) {
		return this == obj || obj instanceof HumanTime && delta == ((HumanTime) obj).delta;
	}


	@Override
	public String toString() {
		return getExactly();
	}
}
