package com.gb.ctptickets.util;


import java.io.File;
import java.util.List;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.webkit.MimeTypeMap;

import com.gb.ctptickets.App;


/**
 * Android intent related utilities
 *
 * Created by gbiro on 5/22/2015.
 */
public class IntentUtils {

	public static Intent getSendMailIntent(String to, String subject, String appChooserTitle) {
		Intent i = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", to, null));
		i.putExtra(Intent.EXTRA_SUBJECT, subject);
		return Intent.createChooser(i, appChooserTitle);
	}


	public static void startSmsApplication(@SuppressWarnings("SameParameterValue") String phoneNumber) {
		Intent i = new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", phoneNumber, null));
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		i.addCategory(Intent.CATEGORY_DEFAULT);
		App.getContext().startActivity(i);
	}


	public static void openFile(File file) throws ActivityNotFoundException {
		Intent intent = new Intent(Intent.ACTION_VIEW);
		String extension = MimeTypeMap.getFileExtensionFromUrl(file.getPath());

		if (extension != null) {
			MimeTypeMap mime = MimeTypeMap.getSingleton();
			String type = mime.getMimeTypeFromExtension(extension);
			intent.setDataAndType(Uri.fromFile(file), type);
		} else {
			intent.setData(Uri.fromFile(file));
		}
		//noinspection deprecation
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET | Intent.FLAG_ACTIVITY_NEW_TASK);

		if (isIntentAvailable(intent)) {
			App.getContext().startActivity(intent);
		} else {
			throw new ActivityNotFoundException();
		}
	}


	private static boolean isIntentAvailable(Intent intent) {
		final PackageManager manager = App.getContext().getPackageManager();
		List<ResolveInfo> list = manager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
		return list.size() > 0;
	}


	public static void launchGooglePlay(Context activityContext) {
		String pack = App.getContext().getPackageName();
		Uri uri = Uri.parse("market://details?id=" + pack);
		Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
		//noinspection deprecation
		goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
		try {
			activityContext.startActivity(goToMarket);
		} catch (ActivityNotFoundException e) {
			goToMarket = new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id="
					+ pack));
			//noinspection deprecation
			goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
			activityContext.startActivity(goToMarket);
		}
	}
}
