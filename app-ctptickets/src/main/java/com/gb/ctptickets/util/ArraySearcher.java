package com.gb.ctptickets.util;


/**
 * Utility to walk a non-sorted array and find the item that's closest to a certain value
 *
 * Created by gbiro on 10/3/2014.
 */
public class ArraySearcher {

	private final long[]	array;


	public ArraySearcher(long[] array) {
		this.array = array;
	}


	/**
	 * The specified <code>array</code> must be sorted in increasing order
	 *
	 * @return the index of the element to which the specified <code>value</code> is nearest or -1 if the array is null
	 *         or empty; in case of multiple results it returns the first index
	 */
	public int closest(long value) {
		int index = -1;
		long minDelta = Long.MAX_VALUE;

		if (array == null || array.length < 1) {
			return -1;
		}

		if (value <= array[0]) {
			return 0;
		}

		for (int i = array.length - 1; i >= 0; i--) {
			if (Math.abs(array[i] - value) <= minDelta) {
				index = i;
				minDelta = Math.abs(array[i] - value);
			}
		}
		return index;
	}
}
