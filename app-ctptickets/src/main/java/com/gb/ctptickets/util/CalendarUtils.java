package com.gb.ctptickets.util;

import java.util.Calendar;

/**
 * Created by gbiro on 5/25/2015.
 */
public class CalendarUtils {

    /**
     * @return true if today is Saturday or Sunday, false otherwise
     */
    public static boolean isWeekend() {
        int dayOfWeek = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
        return dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY;
    }
}