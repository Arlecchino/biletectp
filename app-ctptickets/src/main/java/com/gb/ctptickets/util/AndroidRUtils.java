package com.gb.ctptickets.util;


import java.lang.reflect.Field;

import android.net.Uri;

import com.gb.ctptickets.App;
import com.gb.ctptickets.R;


/**
 * Utilities for android resources
 * 
 * Created by gbiro on 10/3/2014.
 */
@SuppressWarnings("unused")
public class AndroidRUtils {

	public static final String	ANIM		= "anim";
	public static final String	ARRAY		= "array";
	public static final String	ATTR		= "attr";
	public static final String	BOOL		= "bool";
	public static final String	COLOR		= "color";
	public static final String	DIMEN		= "dimen";
	public static final String	DRAWABLE	= "drawable";
	public static final String	ID			= "id";
	public static final String	INTEGER		= "integer";
	public static final String	LAYOUT		= "layout";
	public static final String	MENU		= "menu";
	public static final String	RAW			= "raw";
	public static final String	STRING		= "string";
	public static final String	STYLE		= "style";
	public static final String	XML			= "xml";
	public static final String	STYLABLE	= "styleable";


	/**
	 * 
	 * @param name
	 *            one of the constant fields from this class
	 * @return all id's that were found under R.name
	 */
	public static Uri[] getResourceUrisFromRSubClass(@SuppressWarnings("SameParameterValue") String name) {
		Class[] classes = R.class.getDeclaredClasses();
		Class searchedClazz = null;

		for (Class clazz : classes) {
			if (clazz.getSimpleName().equals(name)) {
				searchedClazz = clazz;
				break;
			}
		}

		if (searchedClazz != null) {
			Field[] fields = searchedClazz.getDeclaredFields();
			Uri[] result = new Uri[fields.length];

			for (int i = 0; i < fields.length; i++) {
				result[i] = Uri.parse("android.resource://" + App.getContext().getPackageName() + "/raw/"
						+ fields[i].getName());
			}
			return result;
		} else {
			return new Uri[0];
		}
	}
}
