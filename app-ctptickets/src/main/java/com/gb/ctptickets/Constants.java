package com.gb.ctptickets;


import com.gb.ctptickets.provider.currency.model.Currency;
import com.gb.ctptickets.util.HumanTime;


/**
 * Application constants
 * <p/>
 * Created by GABOR on 2014-jún.-22.
 */
public class Constants {

    /**
     * February 2011: Android 3.0.
     */
    public static final int HONEYCOMB = 11;

    /**
     * June 2011: Android 3.2.
     */
    public static final int HONEYCOMB_MR2 = 13;

    /**
     * October 2011: Android 4.0.
     */
    public static final int ICE_CREAM_SANDWICH = 14;

    public static final boolean UPDATE_TICKET_STATUS_ICON = true;

    public static final String TICKET_BUY_PHONE_NUMBER = "7479";

    public static final Currency BASE_CURRENCY = Currency.EUR;

    public static final String[] DEFAULT_BUS_LINES = new String[]{
            "1", "3", "4", "5", "6", "7", "8", "8L", "9", "19", "20", "21", "22", "23", "24", "24B", "25", "25N", "26",
            "27", "28", "28B", "29", "30", "31", "32", "32B", "33", "34", "35", "36", "36B", "37", "38", "39", "40",
            "41", "42", "43", "43B", "43P", "46B", "46", "47", "47B", "48", "50", "52", "100", "101", "102", "M11",
            "M12", "M13", "M16", "M21", "M22", "M23", "M24", "M25", "M26", "M31", "M32", "M33", "M34", "M37", "M38",
            "M39", "M41", "M42", "M43", "M44", "M45"
    };

    public static final int[] DEFAULT_URBAN_TICKETS = new int[]{
            R.string.m60_urb_label, R.string.h24_urb_label
    };
    public static final long[] DEFAULT_URBAN_TICKETS_DURATION = new long[]{
            HumanTime.Time.HOUR.valueInMilisecs, HumanTime.Time.DAY.valueInMilisecs
    };

    public static final int[] DEFAULT_METRO_URBAN_TICKETS = new int[]{
            R.string.m40_met_urb_label, R.string.m60_met_urb_label, R.string.m80_met_urb_label,
            R.string.h24_met_urb_label
    };
    public static final long[] DEFAULT_METRO_URBAN_TICKETS_DURATION = new long[]{
            40 * HumanTime.Time.MINUTE.valueInMilisecs, HumanTime.Time.HOUR.valueInMilisecs,
            80 * HumanTime.Time.MINUTE.valueInMilisecs, HumanTime.Time.DAY.valueInMilisecs
    };

    public static final float PRICE_SINGLE_TRAVEL = 0.5f;
    public static final float PRICE_M40_MET_URB = 0.9f;
    public static final float PRICE_M60_URB = 0.9f;
    public static final float PRICE_M60_MET_URB = 1.35f;
    public static final float PRICE_H24_URB = 2.6f;
    public static final float PRICE_M80_MET_URB = 1.8f;
    public static final float PRICE_H24_MET_URB = 4.0f;

    public static final String RATUC_URL_BASE = "http://www.ratuc.ro";
    public static final String RATUC_URL_TIMETABLE = RATUC_URL_BASE + "/programe1.php";
}
