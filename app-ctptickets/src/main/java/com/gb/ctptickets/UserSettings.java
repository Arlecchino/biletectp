package com.gb.ctptickets;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;

import android.content.Context;
import android.net.Uri;
import android.preference.PreferenceManager;

import com.gb.ctptickets.provider.ticket.AvailableBusLineRegistry;
import com.gb.ctptickets.provider.ticket.model.TicketRate;
import com.gb.ctptickets.provider.ticket.model.TicketRequestStatus;
import com.gb.ctptickets.util.PrefsUtil;


/**
 * Created by GABOR on 2014-szept.-11.
 */
public class UserSettings {

	private static final String							PREF_CURRENT_CURRENCY_RATE			= "PREF_CURRENT_CURRENCY_RATE";
	public static final String							PREF_QUICK_BUS_LINE_SELECTION		= "PREF_CUSTOM_BUS_LINE_SELECTION";
	public static final String							PREF_CUSTOM_BUS_LINES				= "PREF_CUSTOM_BUS_LINES";
	private static final String							PREF_CUSTOM_TICKETS					= "PREF_CUSTOM_TICKETS";
	private static final String							PREF_TICKET_REQUEST_STATUS			= "PREF_TICKET_REQUEST_STATUS";
	private static final String							PREF_TICKET_LATE_NEWER_SHOW_AGAIN	= "PREF_TICKET_LATE_NEWER_SHOW_AGAIN";

	private static TicketRate							currencyCache;
	private static Map<String, TicketRequestStatus[]>	ticketRequestStatusCache;


	public static TicketRate getCurrentTicketRate() {
		if (currencyCache == null) {
			currencyCache = PrefsUtil.get(PREF_CURRENT_CURRENCY_RATE, TicketRate.CREATOR);
		}
		return currencyCache;
	}


	public static void setCurrentCurrencyRate(TicketRate rate) {
		PrefsUtil.put(PREF_CURRENT_CURRENCY_RATE, rate);
		currencyCache = rate;
	}


	public static void clearCurrentCurrencyRate() {
		PrefsUtil.remove(PREF_CURRENT_CURRENCY_RATE);
		currencyCache = null;
	}


	public static String getQuickBusLineSelection() {
		return PrefsUtil.get(PREF_QUICK_BUS_LINE_SELECTION, (String) null);
	}


	public static void setQuickBusLineSelection(String line) {
		PrefsUtil.put(PREF_QUICK_BUS_LINE_SELECTION, line);
	}


	public static String[] getCustomBusLines() {
		return PrefsUtil.get(PREF_CUSTOM_BUS_LINES, (String[]) null);
	}


	private static void setCustomBusLines(String[] lines) {
		PrefsUtil.put(PREF_CUSTOM_BUS_LINES, lines);
	}


	/**
	 * @return true if the custom bus line does not yet exists, false otherwise
	 */
	@SuppressWarnings("BooleanMethodIsAlwaysInverted")
	public static boolean addCustomBusLine(String line) {
		if (!ArrayUtils.contains(AvailableBusLineRegistry.getAllBusLines(true, false), line)) {
			String[] customBusLines = getCustomBusLines();
			customBusLines = ArrayUtils.add(customBusLines, line);
			setCustomBusLines(customBusLines);
			return true;
		}
		return false;
	}


	public static String[] getCustomTickets() {
		return PrefsUtil.get(PREF_CUSTOM_TICKETS, (String[]) null);
	}


	private static void setCustomTickets(String[] tickets) {
		PrefsUtil.put(PREF_CUSTOM_TICKETS, tickets);
	}


	public static boolean addCustomTicket(String ticket) {
		List<String> customTickets = new ArrayList<>(Arrays.asList(getCustomTickets()));

		if (!customTickets.contains(ticket)) {
			customTickets.add(ticket);
			setCustomTickets(customTickets.toArray(new String[customTickets.size()]));
			return true;
		}
		return false;
	}


	public static void removeCustomTicket(String ticket) {
		List<String> customTickets = new ArrayList<>(Arrays.asList(getCustomTickets()));

		if (customTickets.contains(ticket)) {
			customTickets.remove(ticket);
			setCustomTickets(customTickets.toArray(new String[customTickets.size()]));
		}
	}


	public static void setTicketRequestStatus(TicketRequestStatus status) {
		//noinspection unchecked
		ticketRequestStatusCache = PrefsUtil.get(PREF_TICKET_REQUEST_STATUS, (Map) null);

		if (ticketRequestStatusCache == null) {
			ticketRequestStatusCache = new HashMap<>();
		}
		TicketRequestStatus[] statuses = ticketRequestStatusCache.get(status.getTicket());
		boolean found = false;

		for (int i = 0; i < statuses.length; i++) {
			if (statuses[i].getId() == status.getId()) {
				statuses[i] = status;
				found = true;
			}
		}

		if (!found) {
			ArrayUtils.add(statuses, status);
		}
		PrefsUtil.put(PREF_TICKET_REQUEST_STATUS, ticketRequestStatusCache);
	}


	public static void setTicketRequestStatuses(String ticket, TicketRequestStatus[] status) {
		//noinspection unchecked
		ticketRequestStatusCache = PrefsUtil.get(PREF_TICKET_REQUEST_STATUS, (Map) null);

		if (ticketRequestStatusCache == null) {
			ticketRequestStatusCache = new HashMap<>();
		}
		ticketRequestStatusCache.put(ticket, status);
		PrefsUtil.put(PREF_TICKET_REQUEST_STATUS, ticketRequestStatusCache);
	}


	public static TicketRequestStatus[] getTicketRequestStatus(String ticket) {
		if (ticketRequestStatusCache != null && ticketRequestStatusCache.containsKey(ticket)) {
			return ticketRequestStatusCache.get(ticket);
		} else {
			//noinspection unchecked
			ticketRequestStatusCache = PrefsUtil.get(PREF_TICKET_REQUEST_STATUS, (Map) null);
			return ticketRequestStatusCache != null ? ticketRequestStatusCache.get(ticket) : null;
		}
	}


	public static int getTicketWaitingTimeoutMilisecs() {
		Context context = App.getContext();
		String timeoutStr = PreferenceManager.getDefaultSharedPreferences(context).getString(
				context.getString(R.string.pref_ticket_waiting_timeout),
				Integer.toString(context.getResources().getInteger(R.integer.pref_ticket_waiting_timeout_default)));
		return Integer.valueOf(timeoutStr) * 1000;
	}


	public static int getTimetableRefreshDays() {
		Context context = App.getContext();
		String refreshDaysStr = PreferenceManager.getDefaultSharedPreferences(context).getString(
				context.getString(R.string.pref_timetable_refresh),
				Integer.toString(context.getResources().getInteger(R.integer.pref_timetable_refresh_default)));
		return Integer.valueOf(refreshDaysStr);
	}


	@SuppressWarnings("BooleanMethodIsAlwaysInverted")
	public static boolean getHideDefaultTickets() {
		Context context = App.getContext();
		return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(
				context.getString(R.string.pref_hide_default_tickets), false);
	}


	public static void setTicketLateWarningDialogNewerShowAgain(boolean neverShowAgain) {
		PrefsUtil.put(PREF_TICKET_LATE_NEWER_SHOW_AGAIN, neverShowAgain);
	}


	public static boolean getTicketLateWarningNewerShowAgain() {
		return PrefsUtil.get(PREF_TICKET_LATE_NEWER_SHOW_AGAIN, false);
	}


	public static boolean isNotificationsEnabled() {
		Context context = App.getContext();
		return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(
				context.getString(R.string.pref_notification_enabled), true);
	}


	public static Uri getNotificationRingtoneUri() {
		Context context = App.getContext();
		return Uri.parse(PreferenceManager.getDefaultSharedPreferences(context).getString(
				context.getString(R.string.pref_notification_sound),
				"android.resource://com.gb.ctptickets/raw/woop_woop"));
	}


	public static boolean getMockTicketsEnabled() {
		Context context = App.getContext();
		return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(
				context.getString(R.string.pref_debug_mock_tickets), false);
	}

	public static boolean getMockBuyFailedEnabled() {
		Context context = App.getContext();
		return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(
				context.getString(R.string.pref_debug_mock_buy_fail), false);
	}

	public static boolean isWeekendWarningEnabled() {
		Context context = App.getContext();
		return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(
				context.getString(R.string.pref_weekend_warning), false);
	}
}
