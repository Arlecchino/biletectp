package com.gb.ctptickets;


import java.io.UnsupportedEncodingException;
import java.util.UUID;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.gb.ctptickets.provider.timetable.TimeTableAsyncTask;
import com.gb.ctptickets.ui.eventbus.TicketStatusChangedEvent;
import com.gb.ctptickets.util.HumanTime;
import com.gb.ctptickets.widget.QuickWidgetProvider;

import de.greenrobot.event.EventBus;


/**
 * Main Application class
 * 
 * Created by gbiro on 2014-Jun-20.
 */
public class App extends Application {

	private static Context	applicationContext;


	public static Context getContext() {
		return applicationContext;
	}


	/**
	 * Generate a unique id for the device. Changes with every factory reset. If the device doesn't have a proper
	 * android_id and deviceId, it falls back to a randomly generated id, that is persisted in SharedPreferences.
	 */
	public static String generateUDID(Context context) {
		String deviceId = null;
		String androidId = null;
		UUID deviceUuid = null;

		// androidId changes with every factory reset (which is useful in our case)
		androidId = ""
				+ android.provider.Settings.Secure.getString(context.getContentResolver(),
						android.provider.Settings.Secure.ANDROID_ID);

		try {
			if (!"9774d56d682e549c".equals(androidId)) {
				deviceUuid = UUID.nameUUIDFromBytes(androidId.getBytes("utf8"));
			} else {
				// On some 2.2 devices androidId is always 9774d56d682e549c,
				// which is unsafe
				TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

				if (tm != null) {
					// Tablets may not have imei and/or imsi.
					// Does not change on factory reset.
					deviceId = tm.getDeviceId();
				}

				if (TextUtils.isEmpty(deviceId)) {
					// worst case scenario as this id is lost when the
					// application stops
					deviceUuid = UUID.randomUUID();
				} else {
					deviceUuid = UUID.nameUUIDFromBytes(deviceId.getBytes("utf8"));
				}
			}
		} catch (UnsupportedEncodingException e) {
			// Change it back to "utf8" right now!!
		}
		return deviceUuid.toString();
	}


	@Override
	public void onCreate() {
		super.onCreate();
		EventBus.getDefault().register(this);
		applicationContext = this;
		PreferenceManager.setDefaultValues(this, R.xml.pref_general, false);
	}


	@SuppressWarnings("unused")
	public void onEvent(TicketStatusChangedEvent event) {
		QuickWidgetProvider.updateWidgets(event.ticket);
	}


	public static String getVersion() {
		PackageManager manager = getContext().getPackageManager();
		try {
			PackageInfo info = manager.getPackageInfo(getContext().getPackageName(), 0);
			return Integer.toString(info.versionCode);
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}
		return "Unknown";
	}

	public static String getVersionName() {
		PackageManager manager = getContext().getPackageManager();
		try {
			PackageInfo info = manager.getPackageInfo(getContext().getPackageName(), 0);
			return info.versionName;
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}
		return "Unknown";
	}
}
